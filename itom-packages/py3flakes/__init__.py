# -*- coding: utf-8 -*-

# Copyright (c) 2010 - 2012 Detlev Offenbach <detlev@die-offenbachs.de>
#

"""
Package containg the pyflakes Python3 port adapted for Qt.
"""

__version__ = '0.5.0'
