<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../plot/AbstractNode.h" line="210"/>
        <source>Live data source for plot</source>
        <translation type="unfinished">Live Datenquelle für Plot</translation>
    </message>
    <message>
        <location filename="../../plot/AbstractDObjPCLFigure.h" line="80"/>
        <location filename="../../plot/AbstractDObjPCLFigure.h" line="82"/>
        <location filename="../../plot/AbstractDObjPCLFigure.h" line="84"/>
        <location filename="../../plot/AbstractDObjFigure.h" line="73"/>
        <source>Source data for plot</source>
        <translation type="unfinished">Quelldaten für Plot</translation>
    </message>
    <message>
        <location filename="../../plot/AbstractDObjFigure.h" line="74"/>
        <source>Actual output data of plot</source>
        <translation type="unfinished">Aktuelle Ausgabedaten für Plot</translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractNode.cpp" line="105"/>
        <source>Parameter: does not exist in updateParam</source>
        <translation type="unfinished">Parameter: Existiert nicht in &apos;updateParam&apos;</translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractNode.cpp" line="114"/>
        <source>Running update on a locked input channel, i.e. updatePending flag is not set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractNode.cpp" line="119"/>
        <source>Channel is already updating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractNode.cpp" line="237"/>
        <source>Not all parameters in list could not be found in channels, in updateChannels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractNode.cpp" line="276"/>
        <source>channel is not a sender in setUpdatePending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractNode.cpp" line="281"/>
        <source>unknown channel in setUpdatePending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractFigure.cpp" line="204"/>
        <location filename="../../plot/sources/AbstractFigure.cpp" line="250"/>
        <source>duplicate Channel, in addChannel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractFigure.cpp" line="212"/>
        <location filename="../../plot/sources/AbstractFigure.cpp" line="219"/>
        <source>parameters incompatible, while adding channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractFigure.cpp" line="224"/>
        <source>undefined channel direction, while adding channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractFigure.cpp" line="239"/>
        <source>invalid child pointer, in addChannel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractFigure.cpp" line="266"/>
        <location filename="../../plot/sources/AbstractFigure.cpp" line="293"/>
        <source>channel does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="71"/>
        <source>Tried to scale unscaleable unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="75"/>
        <source>No unit specified</source>
        <translation type="unfinished">Keine Einheiten festgelegt</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="216"/>
        <source>Pluginname undefined. No xml file loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="253"/>
        <source>ParamList not inialized properly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="498"/>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="714"/>
        <source>Can&apos;t open xml file</source>
        <translation type="unfinished">XML-Datei kann nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="786"/>
        <source>%1
Autosave parameter %2 not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="790"/>
        <source>XML-Import warnings:
Autosave parameter %1 not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="817"/>
        <source>%1
Obsolete parameter %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="821"/>
        <source>XML-Import warnings:
Obsolete parameter %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="830"/>
        <source>%1
Parameter %2 not autosave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="834"/>
        <source>XML-Import warnings:
Parameter %1 not autosave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="859"/>
        <source>%1
Parameter not loadable %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="863"/>
        <source>XML-Import warnings:
Parameter not loadable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="873"/>
        <source>%1
Type conflict for %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="877"/>
        <source>XML-Import warnings:
Type conflict for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="954"/>
        <source>Save object failed: type not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1248"/>
        <source>Save object failed: invalid object handle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1253"/>
        <source>Save object failed: object seems empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1288"/>
        <source>Save object failed: file not writeable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1383"/>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1412"/>
        <source>Load object warning: Metadata &quot; %1 &quot; for %2 missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1448"/>
        <source>Load object failed: Number of dims smaller 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1456"/>
        <source>Not enough memory to alloc sizes vector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1472"/>
        <source>Load object failed: dimension size missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1486"/>
        <source>Load object failed: dimX not specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1498"/>
        <source>Load object failed: dimY not specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1573"/>
        <source>Load object failed: type not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1582"/>
        <source>Load object failed: Error during allocating memory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1627"/>
        <source>Load object failed: file corrupted at metaData (v1.0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1634"/>
        <source>Load object warning: file has invalid metaData for v1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1652"/>
        <source>Load object warning: DoubleExportType for v1.0 invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1657"/>
        <source>Load object warning: DoubleExportType for v1.0 missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1691"/>
        <source>Load object warning: MetaData for %1 missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1724"/>
        <source>Load object warning: MetaData for dimX missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1754"/>
        <source>Load object warning: MetaData for dimY missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1784"/>
        <source>Load object warning: MetaData for values missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1813"/>
        <source>Load object warning: MetaData import for Rotation Matrix failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1848"/>
        <source>Load object failed: file corrupted at tagSpace (v1.0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1853"/>
        <source>Load object failed: tag space not at expected position. Got %1 instead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1866"/>
        <source>Load object failed: tags Space invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1904"/>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1910"/>
        <source>Load object warning: invalid tagType found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1916"/>
        <source>Load object warning: tagsSpace invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1955"/>
        <source>Load object failed: dataSpace missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1960"/>
        <source>Load object failed: dataSpace not at expected position. Got %1 instead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1970"/>
        <source>Load object warning: dataSpace and dataObject are not equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1975"/>
        <source>Load object warning: dataSpace attributes corrupted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1988"/>
        <source>Load object warning: dataSpace for a plane corrupted. Got %1 instead of %2 bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1999"/>
        <source>Load object failed: dataStream ended before finished reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2029"/>
        <source>Load object failed: invalid object handle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2056"/>
        <source>Load object failed: file not readable or does not exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2070"/>
        <source>Load object failed: file seems corrupt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2078"/>
        <source>Load object failed: wrong xml version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2086"/>
        <source>Load object failed: wrong document encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2092"/>
        <source>Load object failed: unexpected file ending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2111"/>
        <source>Load object failed: file is no itomDataObjectFile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2139"/>
        <source>Load object failed: illegal format version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2144"/>
        <source>Load object failed: object header not valied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="152"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="167"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="172"/>
        <source>Timeout while waiting for answer from camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="516"/>
        <location filename="../../common/sources/helperActuator.cpp" line="185"/>
        <location filename="../../common/sources/helperActuator.cpp" line="223"/>
        <source>Error during setPosRel: Vectors differ in size</source>
        <translation type="unfinished">Fehler bei setPosRel: Vektoren unterscheiden sich in der Größe</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="552"/>
        <source>Error during setPosAbs: Vectors differ in size</source>
        <translation type="unfinished">Fehler bei setPosAbs: Vektoren unterscheiden sich in der Größe</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="47"/>
        <source>parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="68"/>
        <source>mandatory parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="72"/>
        <source>optional parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="76"/>
        <source>output parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="106"/>
        <location filename="../../common/sources/helperCommon.cpp" line="127"/>
        <source>parameter &apos;%1&apos; cannot be found in given parameter vector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="211"/>
        <location filename="../../common/sources/helperCommon.cpp" line="336"/>
        <source>name of requested parameter is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="218"/>
        <location filename="../../common/sources/helperCommon.cpp" line="343"/>
        <source>the parameter name &apos;%1&apos; is invald</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="242"/>
        <source>array index of parameter out of bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="249"/>
        <location filename="../../common/sources/helperCommon.cpp" line="376"/>
        <source>given index of parameter name ignored since parameter is no array type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="256"/>
        <location filename="../../common/sources/helperCommon.cpp" line="383"/>
        <source>parameter not found in m_params.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="369"/>
        <source>array index out of bounds.</source>
        <translation type="unfinished">Array-Index liegt außerhalb des Bereichs.</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="424"/>
        <source>invalid parameter name</source>
        <translation type="unfinished">Ungültiger Parametername</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperGrabber.cpp" line="81"/>
        <location filename="../../common/sources/helperGrabber.cpp" line="368"/>
        <location filename="../../common/sources/helperGrabber.cpp" line="388"/>
        <location filename="../../common/sources/helperGrabber.cpp" line="408"/>
        <location filename="../../common/sources/helperActuator.cpp" line="84"/>
        <source>timeout while getting numaxis parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperGrabber.cpp" line="119"/>
        <source>Camera not correctly initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperGrabber.cpp" line="141"/>
        <location filename="../../common/sources/helperActuator.cpp" line="319"/>
        <source>Semaphore not correctly initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperGrabber.cpp" line="146"/>
        <location filename="../../common/sources/helperActuator.cpp" line="147"/>
        <location filename="../../common/sources/helperActuator.cpp" line="326"/>
        <source>Timeout while Waiting for Semaphore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperActuator.cpp" line="138"/>
        <source>Motor not correctly initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperActuator.cpp" line="154"/>
        <source>Semaphore contained error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ito::AbstractAddInConfigDialog</name>
    <message>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="82"/>
        <source>slot &apos;setParam&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParam&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="87"/>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="156"/>
        <source>pointer to plugin is invalid.</source>
        <translation type="unfinished">Zeiger des Plugins ist ungültig.</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="101"/>
        <source>Error while setting parameter &apos;%1&apos;</source>
        <translation type="unfinished">Fehler beim Parametersetzen &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="105"/>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="167"/>
        <source>Error while setting parameter</source>
        <translation type="unfinished">Fehler beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="119"/>
        <source>Warning while setting parameter &apos;%1&apos;</source>
        <translation type="unfinished">Warnung beim Parametersetzen &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="123"/>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="178"/>
        <source>Warning while setting parameter</source>
        <translation type="unfinished">Warnung beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="151"/>
        <source>slot &apos;setParamVector&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParamVector&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="202"/>
        <source>Timeout while waiting for answer from plugin instance.</source>
        <translation type="unfinished">Zeitüberschreitung beim Warten auf Antwort der Plugin-Instanz.</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="215"/>
        <source>Error while execution</source>
        <translation type="unfinished">Fehler bei der Ausführung</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInConfigDialog.cpp" line="226"/>
        <source>Warning while execution</source>
        <translation type="unfinished">Warnung bei der Ausführung</translation>
    </message>
</context>
<context>
    <name>ito::AbstractAddInDockWidget</name>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="82"/>
        <source>slot &apos;setParam&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParam&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="87"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="136"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="243"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="302"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="352"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="399"/>
        <source>pointer to plugin is invalid.</source>
        <translation type="unfinished">Zeiger des Plugins ist ungültig.</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="93"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="142"/>
        <source>Error while setting parameter</source>
        <translation type="unfinished">Fehler beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="104"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="153"/>
        <source>Warning while setting parameter</source>
        <translation type="unfinished">Warnung beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="131"/>
        <source>slot &apos;setParamVector&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParamVector&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="176"/>
        <source>Timeout while waiting for answer from plugin instance.</source>
        <translation type="unfinished">Zeitüberschreitung beim Warten auf Antwort der Plugin-Instanz.</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="189"/>
        <source>Error while execution</source>
        <translation type="unfinished">Fehler bei der Ausführung</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="200"/>
        <source>Warning while execution</source>
        <translation type="unfinished">Warnung bei der Ausführung</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="224"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="283"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="340"/>
        <source>setActuatorPosition can only be called for actuator plugins</source>
        <translation type="unfinished">Der Parameter &apos;setActuatorPosition&apos; kann nur für Motor-Plugins aufgerufen werden</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="237"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="296"/>
        <source>slot &apos;%1&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;%1&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="249"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="308"/>
        <source>Error while calling %1</source>
        <translation type="unfinished">Fehler beim Aufruf von %1</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="260"/>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="319"/>
        <source>Warning while calling %1</source>
        <translation type="unfinished">Warnung beim Aufruf von %1</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="346"/>
        <source>slot &apos;requestStatusAndPosition&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;requestStatusAndPosition&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="358"/>
        <source>Error while calling &apos;requestStatusAndPosition&apos;</source>
        <translation type="unfinished">Fehler beim Aufruf von  &apos;requestStatusAndPosition&apos;</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="369"/>
        <source>Warning while calling &apos;requestStatusAndPosition&apos;</source>
        <translation type="unfinished">Warnung beim Aufruf von  &apos;requestStatusAndPosition&apos;</translation>
    </message>
    <message>
        <location filename="../../common/abstractAddInDockWidget.cpp" line="390"/>
        <source>setActuatorInterrupt can only be called for actuator plugins</source>
        <translation type="unfinished">Der Parameter &apos;setActuatorInterrupt&apos; kann nur für Motor-Plugins aufgerufen werden</translation>
    </message>
</context>
<context>
    <name>ito::AbstractDObjFigure</name>
    <message>
        <location filename="../../plot/sources/AbstractDObjFigure.cpp" line="111"/>
        <source>Function &apos;spawnLinePlot&apos; not supported from this plot widget</source>
        <translation type="unfinished">Die Funktion  &apos;spawnLinePlot&apos; wird von diesem Plot-Widget nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../plot/sources/AbstractDObjFigure.cpp" line="232"/>
        <source>Figure does not contain an input slot for live sources</source>
        <translation type="unfinished">Der Plot besitzt keinen Slot für Live-Quellen</translation>
    </message>
</context>
<context>
    <name>ito::AbstractDObjPclFigure</name>
    <message>
        <location filename="../../plot/sources/AbstractDObjPCLFigure.cpp" line="242"/>
        <source>Function &apos;spawnLinePlot&apos; not supported from this plot widget</source>
        <translation type="unfinished">Die Funktion  &apos;spawnLinePlot&apos; wird von diesem Plot-Widget nicht unterstützt</translation>
    </message>
</context>
<context>
    <name>ito::AbstractFigure</name>
    <message>
        <location filename="../../plot/sources/AbstractFigure.cpp" line="146"/>
        <source>Properties</source>
        <translation type="unfinished">Eigenschaften</translation>
    </message>
</context>
<context>
    <name>ito::AddInAlgo</name>
    <message>
        <location filename="../../common/addInInterface.h" line="1023"/>
        <source>uninitialized vector for mandatory parameters!</source>
        <translation type="unfinished">Nicht initialisierter Vektor für Pflichtparameter!</translation>
    </message>
    <message>
        <location filename="../../common/addInInterface.h" line="1027"/>
        <source>uninitialized vector for optional parameters!</source>
        <translation type="unfinished">Nicht initialisierter Vektor für optionale Parameter!</translation>
    </message>
    <message>
        <location filename="../../common/addInInterface.h" line="1031"/>
        <source>uninitialized vector for output parameters!</source>
        <translation type="unfinished">Nicht initialisierter Vektor für Ausgabeparameter!</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="899"/>
        <source>Constructor must be overwritten</source>
        <translation type="unfinished">Der Konstruktor muss überschrieben sein</translation>
    </message>
</context>
<context>
    <name>ito::AddInBase</name>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="303"/>
        <source>function execution unused in this plugin</source>
        <translation type="unfinished">Die Funktion &apos;execution&apos; wird in diesem Plugin nicht benutzt</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="332"/>
        <source>Toolbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="498"/>
        <source>Your plugin is supposed to have a configuration dialog, but you did not implement the showConfDialog-method</source>
        <translation type="unfinished">Das Plugin hat vermutlich einen Konfigurationsdialog, aber die Methode &apos;showConfDialog&apos; wurde nicht implementiert</translation>
    </message>
</context>
<context>
    <name>ito::AddInDataIO</name>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="561"/>
        <source>listener does not have a slot </source>
        <translation type="unfinished">&apos;Listener&apos; hat keinen Slot </translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="565"/>
        <source>this object already has been registered as listener</source>
        <translation type="unfinished">Diese Objekt wurde bereits beim &apos;Listener&apos; registriert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="577"/>
        <source>timer could not be set</source>
        <translation type="unfinished">Timer kann nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="602"/>
        <source>the object could not been removed from the listener list</source>
        <translation type="unfinished">Das Objekt kann nicht aus der &apos;Listener&apos;-Liste entfernt werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="696"/>
        <location filename="../../common/sources/addInInterface.cpp" line="716"/>
        <location filename="../../common/sources/addInInterface.cpp" line="734"/>
        <location filename="../../common/sources/addInInterface.cpp" line="752"/>
        <location filename="../../common/sources/addInInterface.cpp" line="770"/>
        <location filename="../../common/sources/addInInterface.cpp" line="788"/>
        <location filename="../../common/sources/addInInterface.cpp" line="806"/>
        <source>not implemented</source>
        <translation type="unfinished">Nicht implementiert</translation>
    </message>
</context>
<context>
    <name>ito::AddInGrabber</name>
    <message>
        <location filename="../../common/sources/addInGrabber.cpp" line="82"/>
        <location filename="../../common/sources/addInGrabber.cpp" line="97"/>
        <source>slot &apos;setSource&apos; of live source node could not be invoked</source>
        <translation type="unfinished">Der Slot &apos;setSource&apos; kann nicht aufgerufen werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInGrabber.cpp" line="213"/>
        <source>Error during check data, external dataObject invalid. Object has more or less than 1 plane. It must be of right size and type or an uninitilized image.</source>
        <translation type="unfinished">Fehler beim überprüfen der Daten. Das externe Datenobjekt ist ungültig. Das Objekt hat mehr oder weniger als eine Ebene. Es muss die richtige Größe und vom richtigen Typ sein oder ein nicht initialisiertes Image.</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInGrabber.cpp" line="217"/>
        <source>Error during check data, external dataObject invalid. Object must be of right size and type or a uninitilized image.</source>
        <translation type="unfinished">Fehler beim überprüfen der Daten. Das externe Datenobjekt ist ungültig. Das Objekt muss die richtige Größe und vom richtigen Typ sein oder ein nicht initialisiertes Image.</translation>
    </message>
</context>
</TS>
