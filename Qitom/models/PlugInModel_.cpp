/* ********************************************************************
    itom software
    URL: http://www.uni-stuttgart.de/ito
    Copyright (C) 2013, Institut f�r Technische Optik (ITO),
    Universit�t Stuttgart, Germany

    This file is part of itom.
  
    itom is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public Licence as published by
    the Free Software Foundation; either version 2 of the Licence, or (at
    your option) any later version.

    itom is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library
    General Public Licence for more details.

    You should have received a copy of the GNU Library General Public License
    along with itom. If not, see <http://www.gnu.org/licenses/>.
*********************************************************************** */

#include "PlugInModel.h"
#include "../organizer/addInManager.h"
#include <qicon.h>
#include <qfileinfo.h>
#include "../../common/sharedStructuresGraphics.h"

using namespace ito;

//----------------------------------------------------------------------------------------------------------------------------------
/** constructor
*
*   contructor, creating column headers for the tree view
*/
PlugInModel::PlugInModel()
{
    m_headers << tr("Name") << tr("Type") << tr("Version") << tr("Filename") << tr("Author") << tr("min. itom Version") << tr("max. itom Version") << tr("Description");
    m_alignment << QVariant(Qt::AlignLeft) << QVariant(Qt::AlignLeft) << QVariant(Qt::AlignCenter) << QVariant(Qt::AlignLeft) << QVariant(Qt::AlignLeft) << QVariant(Qt::AlignRight) << QVariant(Qt::AlignRight) << QVariant(Qt::AlignLeft);

    m_iconActuator = QIcon(":/plugins/icons/pluginActuator.png");
    m_iconGrabber = QIcon(":/plugins/icons/pluginGrabber.png");
    m_iconADDA = QIcon(":/plugins/icons/pluginADDA.png");
    m_iconRawIO = QIcon(":/plugins/icons/pluginRawIO.png");
    m_iconFilter = QIcon(":/plugins/icons/pluginFilter.png");
    m_iconDataIO = QIcon(":/plugins/icons/plugin.png");
    m_iconAlgo = QIcon(":/plugins/icons/pluginAlgo.png");
    m_iconWidget = QIcon(":/plugins/icons/window.png");
    m_iconDesignerWidget = QIcon(":/plots/icons/itom_icons/2d.png");

    //analzye enumeration PlotFeatures from sharedStructuresGraphics
    m_designerWidgetPlotTypes[ ito::PlotLine ] = tr("line");
    m_designerWidgetPlotTypes[ ito::PlotImage ] = tr("image");
    m_designerWidgetPlotTypes[ ito::PlotISO ] = tr("isometric");
    m_designerWidgetPlotTypes[ ito::Plot3D ] = tr("3D");

    m_treeFixNodes = new int[7 + m_designerWidgetPlotTypes.size()];
    m_treeFixIndizes = new QModelIndex[7 + m_designerWidgetPlotTypes.size()];

    m_treeFixNodes[0] = typeDataIO;
    m_treeFixNodes[1] = typeActuator;
    m_treeFixNodes[2] = typeAlgo;
    m_treeFixNodes[3] = typeDataIO | typeGrabber;
    m_treeFixNodes[4] = typeDataIO | typeADDA;
    m_treeFixNodes[5] = typeDataIO | typeRawIO;
    m_treeFixNodes[6] = 1 << 32; //designerWidget base node

    m_treeFixIndizes[0] = createIndex(0, 0, &(m_treeFixNodes[0])); //level 0
    m_treeFixIndizes[1] = createIndex(1, 0, &(m_treeFixNodes[1])); //level 0
    m_treeFixIndizes[2] = createIndex(2, 0, &(m_treeFixNodes[2])); //level 0
    m_treeFixIndizes[3] = createIndex(0, 0, &(m_treeFixNodes[3])); //level 1
    m_treeFixIndizes[4] = createIndex(1, 0, &(m_treeFixNodes[4])); //level 1
    m_treeFixIndizes[5] = createIndex(2, 0, &(m_treeFixNodes[5])); //level 1
    m_treeFixIndizes[6] = createIndex(3, 0, &(m_treeFixNodes[6])); //level 0

    QMapIterator<int, QString> i(m_designerWidgetPlotTypes);
    int c = 0;
    while (i.hasNext()) 
    {
        i.next();
        m_treeFixNodes[7+c] = 1 << 32 + i.key();
        m_treeFixIndizes[7+c] = createIndex(c, 0, &(m_treeFixNodes[7+c]));
        c++;
    }
}

//----------------------------------------------------------------------------------------------------------------------------------
/** destructor - clean up, clear header and alignment list
*
*/
PlugInModel::~PlugInModel()
{
    m_headers.clear();
    m_alignment.clear();
    delete[] m_treeFixNodes;
    delete[] m_treeFixIndizes;
    return;
}

QModelIndex PlugInModel::getTypeNode(const int type) const
{
    for(unsigned int i = 0 ; i < (sizeof(m_treeFixNodes) / sizeof(m_treeFixNodes[0])) ; i++)
    {
        if(type == m_treeFixNodes[i])
        {
            return m_treeFixIndizes[i];
        }
    }
    return QModelIndex();
}

//----------------------------------------------------------------------------------------------------------------------------------
Qt::ItemFlags PlugInModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
    {
        return 0;
    }
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

//----------------------------------------------------------------------------------------------------------------------------------
/** return parent element
*   @param [in] index   the element's index for which the parent should be returned
*   @return     the parent element. 
*
*/
QModelIndex PlugInModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
    {
        return QModelIndex();
    }

    ito::AddInManager *aim = ito::AddInManager::getInstance();
    
    tItemType itemType;
    size_t itemInternalData;

    if (!getModelIndexInfo(index, itemType, itemInternalData) )
    {
        return QModelIndex();
    }
    
    switch (itemType)
    {
        case itemCatDataIO:
        case itemCatActuator:
        case itemCatAlgo:
        case itemCatDesignerWidget:
        {
            return QModelIndex();
        }
        case itemSubCategoryDataIO_Grabber:
        case itemSubCategoryDataIO_ADDA:
        case itemSubCategoryDataIO_RawIO:
        {
            return m_treeFixIndizes[0];
        }
        case itemSubCategoryDesignerWidget:
        {
            return m_treeFixIndizes[6];
        }
        case itemDesignerWidget:
        {
            return * ((QModelIndex*)itemInternalData);
        }
        case itemPlugin:
        {
            ito::AddInInterfaceBase *aiib = (ito::AddInInterfaceBase*)(itemInternalData);
            return getTypeNode(aiib->getType());
            /*for (int i = 0 ; i < sizeof(m_treeFixNodes) / sizeof(m_treeFixNodes[0]) ; i++)
            {
                if (aiib->getType() == m_treeFixNodes[i])
                {
                    return m_treeFixIndizes[i]; 
                }
            }*/
        }
        case itemInstance:
        {
            ito::AddInBase *aib = (ito::AddInBase*)(itemInternalData);
            ito::AddInInterfaceBase *aiib = aib->getBasePlugin();
        
            if (aiib->getType() & ito::typeActuator)
            {
                for (int i = 0; i < aim->getActList()->count(); i++)
                {
                    if (aim->getActList()->at(i) == (QObject*)aiib)
                    {
                        return createIndex(i, 0, (void*)aiib);
                    }
                }
                return QModelIndex();
            }
            else if (aiib->getType() & ito::typeDataIO)
            {
                int rowCounter = -1;
                ito::AddInInterfaceBase *aiib2 = NULL;
                for (int i = 0; i < aim->getDataIOList()->count(); i++)
                {
                    aiib2 = (ito::AddInInterfaceBase*)aim->getDataIOList()->at(i);
                    if (aiib2->getType() == aiib->getType())
                    {
                        rowCounter++;
                    }
                    if (aiib2 == aiib)
                    {
                        return createIndex(rowCounter, 0, (void*)aiib);
                    }
                }
                return QModelIndex();
            }
            else
            {
                return QModelIndex();
            }
        }
        case itemFilter:
        {
            ito::AddInAlgo::FilterDef* filter = (ito::AddInAlgo::FilterDef*)(itemInternalData);
            for (int i = 0; i < aim->getAlgList()->count(); i++)
            {
                if (aim->getAlgList()->at(i) == (QObject*)filter->m_pBasePlugin)
                {
                    return createIndex(i, 0, (void*)filter->m_pBasePlugin);
                }
            }
            return QModelIndex();
        }
        case itemWidget:
        {
            ito::AddInAlgo::AlgoWidgetDef* widget = (ito::AddInAlgo::AlgoWidgetDef*)(itemInternalData);
            for (int i = 0; i < aim->getAlgList()->count(); i++)
            {
                if (aim->getAlgList()->at(i) == (QObject*)widget->m_pBasePlugin)
                {
                    return createIndex(i, 0, (void*)widget->m_pBasePlugin);
                }
            }
            return QModelIndex();
        }
        default:
        {
            return QModelIndex();
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------
/** return number of rows
*   @param [in] parent parent of current item
*   @return     returns 0 for all child-child elements, the number of instances for child elements (a plugin) and the number of plugins
*               for a root element
*/
int PlugInModel::rowCount(const QModelIndex &parent) const
{
    ito::AddInManager *aim = ito::AddInManager::getInstance();
    
    if (parent.isValid() == false)
    {
        return 4;
    }
    else //parent valid
    {
        tItemType parentType;
        size_t parentInternalData;
        if (!getModelIndexInfo(parent, parentType, parentInternalData))
        {
            return 0;
        }

        switch (parentType)
        {
            case itemCatDataIO:
            {
                return 3; //three sub-categories of dataIO
            }
            case itemCatActuator:
            {
                return aim->getActList()->count();
            }
            case itemCatAlgo:
            {
                return aim->getAlgList()->count();
            }
            case itemCatDesignerWidget:
            {
                return m_designerWidgetPlotTypes.size();
            }
            case itemSubCategoryDesignerWidget:
            {

            }
            case itemSubCategoryDataIO_Grabber:
            case itemSubCategoryDataIO_ADDA:
            case itemSubCategoryDataIO_RawIO:
            {
                int counter = 0;
                const QList<QObject*> *dataIOs = aim->getDataIOList();
                ito::AddInInterfaceBase *aiib = NULL;
                for (int i = 0; i < dataIOs->count(); i++)
                {
                    aiib = qobject_cast<ito::AddInInterfaceBase*>(dataIOs->at(i));
                    if ((size_t)aiib->getType() == parentInternalData /*subtype*/)
                    {
                        counter++;
                    }
                }
                return counter;
            }
            case itemPlugin:
            {
                ito::AddInInterfaceBase *aiib = (ito::AddInInterfaceBase*)(parentInternalData);
                if (aiib->getType() & ito::typeAlgo)
                {
                    ito::AddInAlgo *aia = (ito::AddInAlgo*)(aiib->getInstList()[0]);
                    QHash<QString,ito::AddInAlgo::FilterDef*> filters;
                    QHash<QString,ito::AddInAlgo::AlgoWidgetDef*> widgets;
                    aia->getFilterList(filters);
                    aia->getAlgoWidgetList(widgets);
                    return filters.size() + widgets.size();
                }
                else
                {
                    return aiib->getInstList().count();
                }
            }
            /*case itemInstance:
            case itemFilter:
            case itemWidget:
            case itemUnknown:*/
            default:
            {
                return 0;
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------
/** return column count
*   @param [in] parent parent of current item
*   @return     2 for child elements (instances) and the header size for root elements (plugins)
*/
int PlugInModel::columnCount(const QModelIndex & /*parent*/) const
{
    return m_headers.size();
}

//----------------------------------------------------------------------------------------------------------------------------------
/** return current index element
*   @param [in] row row of current element
*   @param [in] column column of current element
*   @param [in] parent  parent of current element
*   @return QModelIndex - element at current index
*
*   This method returns the QModelIndex for the current element. As the tree structure is not cached it has to be "calculated" on
*   each call. An invalid parent means were in the top most "plane" of the tree, i.e. the plugin-plane. If the passed index is out
*   of range we return an empty element. Otherwise a new element marked as root level element (i.e. interal pointer = ROOTPOINTER) is 
*   returned. If the parent element is valid the index for an instance is requested. In that case it is first checked if the index
*   for a child child element is queried. In that case again an empty element is returned else the plugin for the selected instance
*   is searched in the plugin lists and an according index is created.
*/
QModelIndex PlugInModel::index(int row, int column, const QModelIndex &parent) const
{
    ito::AddInManager *aim = ito::AddInManager::getInstance();

    if (!hasIndex(row, column, parent))
    {
        return QModelIndex();
    }
    
    if (parent.isValid() == false)
    {
        if (row >= 0 && row <= 2)
        {
            return createIndex(row, column, (void*)&m_treeFixNodes[row]);
        }
        else if(row == 3) //itemCatDesignerWidget
        {
            return createIndex(row, column, (void*)&m_treeFixNodes[6]);
        }
        return QModelIndex();
    }
    else //parent valid
    {
        tItemType parentType;
        size_t parentInternalData;
        if (!getModelIndexInfo(parent, parentType, parentInternalData))
        {
            return QModelIndex();
        }

        switch (parentType)
        {
            case itemCatDataIO:
            {
                return createIndex(row, column, (void*)(&m_treeFixNodes[row+3]));
            }
            case itemCatActuator:
            {
                return createIndex(row, column, (void*)aim->getActList()->at(row));
            }
            case itemCatAlgo:
            {
                return createIndex(row, column, (void*)aim->getAlgList()->at(row));
            }
            case itemCatDesignerWidget:
            {
                return createIndex(row, column, NULL);
            }
            case itemSubCategoryDataIO_Grabber:
            case itemSubCategoryDataIO_ADDA:
            case itemSubCategoryDataIO_RawIO:
            {
                int counter = -1;
                const QList<QObject*> *dataIOs = aim->getDataIOList();
                ito::AddInInterfaceBase *aiib = NULL;
                for (int i = 0; i < dataIOs->count(); i++)
                {
                    aiib = qobject_cast<ito::AddInInterfaceBase*>(dataIOs->at(i));
                    if ((size_t)aiib->getType() == parentInternalData /*subtype*/)
                    {
                        counter++;
                        if (counter == row)
                        {
                            return createIndex(row, column, (void*)aiib);
                        }
                    }
                }
            }
            case itemPlugin:
            {
                ito::AddInInterfaceBase *aiib = (ito::AddInInterfaceBase*)(parentInternalData);
                if (aiib->getType() & ito::typeAlgo)
                {
                    ito::AddInAlgo *aia = (ito::AddInAlgo*)(aiib->getInstList()[0]);
                    QHash<QString,ito::AddInAlgo::FilterDef*> filters;
                    QHash<QString,ito::AddInAlgo::AlgoWidgetDef*> widgets;
                    aia->getFilterList(filters);
                    aia->getAlgoWidgetList(widgets);

                    if (filters.count() > row)
                    {
                        return createIndex(row, column, (void*)filters.values()[row]);
                    }
                    else
                    {
                        aia->getAlgoWidgetList(widgets);
                        qDebug() << "AlgoWidget: r" << row << ", c:" << column << ", p:" << (void*)widgets.values()[row - filters.count()];
                        return createIndex(row, column, (void*)widgets.values()[row - filters.count()]);
                    }

                }
                else
                {
                    return createIndex(row, column, (void*)aiib->getInstList()[row]);
                }
            }
            /*case itemInstance:
            case itemFilter:
            case itemWidget:
            case itemUnknown:*/
            default:
            {
                return QModelIndex();
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------
/** return the header / captions for the tree view model
*
*/
QVariant PlugInModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        if (section >= 0 && section < m_headers.size())
        {
            return m_headers.at(section);
        }
        return QVariant();
    }
    return QVariant();
}

//----------------------------------------------------------------------------------------------------------------------------------
bool PlugInModel::getModelIndexInfo(const QModelIndex &index, tItemType &type, size_t &internalData) const
{
    type = itemUnknown;
    internalData = 0;

    //table of type vs. internalData
    //itemUnknown           -> 0
    //itemCatDataIO         -> ito::typeDataIO
    //itemCatActuator       -> ito::typeActuator
    //itemCatAlgo           -> ito::typeAlgo
    //itemSubCategoryDataIO_... -> Content of m_treeFixNodes[3,4,5] (OR-combination of ito::typeDataIO and sub-type)
    //itemPlugin            -> Pointer to corresponding AddInInterfaceBase
    //itemInstance          -> Pointer to corresponding AddInBase
    //itemFilter            -> Pointer to corresponding FilterDef
    //itemWidget            -> Pointer to corresponding AlgoWidgetDef
    //itemCatDesignerWidget -> (1 << 32) + 0
    //itemSubCategoryDesignerWidget -> (1 << 32) + Plot-type from enumeration ito::PlotFeature
    //itemDesignerWidget    -> (1 << 32) + (1 << 1) + Plot-type from enumeration ito::PlotFeature

    //check if item is of type itemCategory or itemSubCategory
    const int *ptr1 = &m_treeFixNodes[0];
//    const int *ptr2 = &m_treeFixNodes[5];
    ito::AddInManager *aim = ito::AddInManager::getInstance();
    void *internalPtr = index.internalPointer();
    QObject *obj = NULL;
//    int rowIndex;

    if (internalPtr >= ptr1 && internalPtr <= (ptr1 + sizeof(m_treeFixNodes)))
    {
        switch (* (int*)index.internalPointer())
        {
            case typeAlgo:
            {
                type = itemCatAlgo;
                internalData = ito::typeAlgo;
                break;
            }
            case typeDataIO:
            {
                type = itemCatDataIO;
                internalData = ito::typeDataIO;
                break;
            }
            case typeActuator:
            {
                type = itemCatActuator;
                internalData = ito::typeActuator;
                break;
            }
            case typeDataIO | typeGrabber:
            {
                //sub category of dataIO
                type = itemSubCategoryDataIO_Grabber;
                internalData = * (int*)index.internalPointer();
                break;
            }
            case typeDataIO | typeADDA:
            {
                //sub category of dataIO
                type = itemSubCategoryDataIO_ADDA;
                internalData = * (int*)index.internalPointer();
                break;
            }
            case typeDataIO | typeRawIO:
            {
                //sub category of dataIO
                type = itemSubCategoryDataIO_RawIO;
                internalData = * (int*)index.internalPointer();
                break;
            }
            case 1 << 32:
            {
                type = itemCatDesignerWidget;
                internalData = *(int*)index.internalPointer();
                break;
            }
            default:
            {
                if( (*(int*)index.internalPointer()) & (2 << 32))
                {
                }
                else
                {
                }
                return false;
            }
        }
        return true;
    }
    else
    {
        //check if item is a filter
        //check type of element
        const QHash<QString, ito::AddInAlgo::FilterDef *> *filters = aim->getFilterList();
        QHash<QString, ito::AddInAlgo::FilterDef *>::const_iterator i = filters->constBegin();
        while (i != filters->constEnd()) 
        {
            //check if index corresponds to this filter
            if ((void*)i.value() == internalPtr)
            {
                type = itemFilter;
                internalData = (size_t)(i.value());
                return true;
            }
            ++i;
        }

        //check if item is a widget
        const QHash<QString, ito::AddInAlgo::AlgoWidgetDef *> *widgets = aim->getAlgoWidgetList();
        QHash<QString, ito::AddInAlgo::AlgoWidgetDef *>::const_iterator j = widgets->constBegin();
        while (j != widgets->constEnd()) 
        {
            //check if index corresponds to this widget
            if ((void*)j.value() == internalPtr)
            {
                type = itemWidget;
                internalData = (size_t)(j.value());
                return true;
            }
            ++j;
        }

        //if the element is no filter and no widget, it only can be a plugin (the DLL itself) or an instance of a plugin
        obj = (QObject*)internalPtr;

        ito::AddInInterfaceBase *aiib = qobject_cast<ito::AddInInterfaceBase*>(obj);
        if (aiib) //it is a plugin
        {
            type = itemPlugin;
            internalData = (size_t)aiib;
            return true;
        }

        if(obj->inherits("ito::AddInBase"))
        {
            ito::AddInBase *aib = reinterpret_cast<ito::AddInBase*>(obj);
            if (aib) //it is an instance
            {
                type = itemInstance;
                internalData = (size_t)aib;
                return true;
            }
        }
    }

    return false;
}

//----------------------------------------------------------------------------------------------------------------------------------
QVariant PlugInModel::getFixedNodeInfo(const QModelIndex &index, const QVariant &name, const tItemType &itemType, const int &role, const QIcon icon) const
{
    if (role == Qt::DisplayRole)
    {
        if (index.column() == 0)
        {
            return name;
        }
        else
        {
            return QVariant();
        }
    }
    else if (role == Qt::DecorationRole)
    {
        if (index.column() == 0)
        {
            return icon;
        }
        else
        {
            return QVariant();
        }
    }
    else if (role == Qt::ToolTipRole)
    {
        if (index.column() == 0)
        {
            return name;
        }
        else
        {
            return QVariant();
        }
    }
    else if (role == Qt::TextAlignmentRole)
    {
        if (index.column() == 0)
        {
            return m_alignment[index.column()];
        }
        else
        {
            return QVariant();
        }
    }
    else if (role == Qt::UserRole + 1) //returns type (OR-combination)
    {
        switch(itemType)
        {
            case itemCatDataIO:
                return ito::typeDataIO;
            case itemCatActuator:
                return ito::typeActuator;
            case itemCatAlgo:
                return ito::typeAlgo;
            case itemCatDesignerWidget:
                return itemCatDesignerWidget << 16;
            case itemSubCategoryDataIO_Grabber:
                return ito::typeDataIO | ito::typeGrabber;
            case itemSubCategoryDataIO_ADDA:
                return ito::typeDataIO | ito::typeADDA;
            case itemSubCategoryDataIO_RawIO:
                return ito::typeDataIO | ito::typeRawIO;
            default:
                return QVariant();
        }
        return itemType;
    }
    else if (role == Qt::UserRole + 2) //returns true if item is a category or sub-category, else false
    {
        return true;
    }
    else if (role == Qt::UserRole + 3) //returns tItemType
    {
        return itemType;
    }

    return QVariant();
}

//----------------------------------------------------------------------------------------------------------------------------------
QVariant PlugInModel::getPluginNodeInfo(const QModelIndex &index, const int &role) const
{
    ito::AddInInterfaceBase *aib = (ito::AddInInterfaceBase *)index.internalPointer();

    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
            case 0: //name
            {
                return aib->objectName();
            }
            case 1: //type
                switch (aib->getType())
                {
                    case typeActuator:
                    {
                        return tr("Motor");
                    }
                    case typeDataIO | typeGrabber:
                    {
                        return tr("Grabber");
                    }
                    case typeDataIO | typeADDA:
                    {
                        return tr("ADDA");
                    }
                    case typeDataIO | typeRawIO:
                    {
                        return tr("Raw IO");
                    }
                    case typeAlgo:
                    {
                        return tr("Algorithm");
                    }
                    default:
                        return QVariant();
                }
            case 2: //version
            {
                int version = aib->getVersion();
                return QString("%1.%2.%3").arg( MAJORVERSION(version) ).arg( MINORVERSION(version) ).arg( PATCHVERSION(version) );
            }
            case 3: //filename
            {
                QFileInfo filename;
                filename = QFileInfo(aib->getFilename());
                return filename.fileName();
            }
            case 4: //autor
            {
                return aib->getAuthor();
            }
            case 5: //minversion
            {
                int version = aib->getMinItomVer();
                if (version != MINVERSION)
                {
                    return QString("%1.%2.%3").arg( MAJORVERSION(version) ).arg( MINORVERSION(version) ).arg( PATCHVERSION(version) );
                }
                return QString("-");
            }
            case 6: //maxversion
            {
                int version = aib->getMaxItomVer();
                if (version != MAXVERSION)
                {
                    return QString("%1.%2.%3").arg( MAJORVERSION(version) ).arg( MINORVERSION(version) ).arg( PATCHVERSION(version) );
                }
                return QString("-");
            }
            case 7: //description
            {
                return aib->getDescription();
            }
            default:
            {
                return QVariant();
            }
        }
    }
    else if (role == Qt::DecorationRole)
    {
        if (index.column() == 0)
        {
            switch (aib->getType())
            {
                case typeActuator:
                    return m_iconActuator;
                break;
                case typeDataIO | typeGrabber:
                    return m_iconGrabber;
                break;
                case typeDataIO | typeADDA:
                    return m_iconADDA;
                break;
                case typeDataIO | typeRawIO:
                    return m_iconRawIO;
                break;
                case typeAlgo:
                    return m_iconAlgo;
                break;
                default:
                    return QVariant();
            }
        }
        else
        {
            return QVariant();
        }
    }
    else if (role == Qt::ToolTipRole)
    {
        switch(index.column())
        {
            case 0: //name
                return aib->objectName();
            case 3: //filename
                return aib->getFilename();
            case 7: //description
                return aib->getDescription();
            default:
                return QVariant();
        }
    }
    else if (role == Qt::TextAlignmentRole)
    {
        if (index.column() >= 0 && index.column() < m_alignment.size())
        {
            return m_alignment[index.column()];
        }
        else
        {
            return QVariant();
        }
    }
    else if (role == Qt::UserRole + 1) //returns type (OR-combination)
    {
        return aib->getType();
    }
    else if (role == Qt::UserRole + 2) //returns true if item is a category or sub-category, else false
    {
        return false;
    }
    else if (role == Qt::UserRole + 3) //returns tItemType
    {
        return itemPlugin;
    }

    return QVariant();
}

//----------------------------------------------------------------------------------------------------------------------------------
QVariant PlugInModel::getInstanceNodeInfo(const QModelIndex &index, const int &role) const
{
    //ito::AddInInterfaceBase *aib = (ito::AddInInterfaceBase *)index.internalPointer();
    ito::AddInBase *ai = (ito::AddInBase *)index.internalPointer();

    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
            case 0: //name
            {
                return QString("ID: %1").arg(ai->getID());
            }
            case 7: //description
            {
                //return aib->getDescription();
            }
            default:
            {
                return QVariant();
            }
        }
    }
/*    else if (role == Qt::DecorationRole)
    {
        return QVariant();
    }*/
    else if (role == Qt::ToolTipRole)
    {
        if (index.column() == 0)
        {
            return QString("ID: %1").arg(ai->getID());
        }
        else
        {
            return QVariant();
        }
    }
    else if (role == Qt::TextAlignmentRole)
    {
        if (index.column() >= 0 && index.column() < m_alignment.size())
        {
            return m_alignment[index.column()];
        }
        else
        {
            return QVariant();
        }
    }
    else if (role == Qt::UserRole + 1) //returns type (OR-combination)
    {
        return ai->getBasePlugin()->getType();
    }
    else if (role == Qt::UserRole + 2) //returns true if item is a category or sub-category, else false
    {
        return false;
    }
    else if (role == Qt::UserRole + 3) //returns tItemType
    {
        return itemInstance;
    }

    return QVariant();
}

//----------------------------------------------------------------------------------------------------------------------------------
QVariant PlugInModel::getFilterOrWidgetNodeInfo(const QModelIndex &index, const int &role, bool filterNotWidget) const
{
    ito::AddInAlgo::FilterDef *filterDef = NULL;
    ito::AddInAlgo::AlgoWidgetDef *widgetDef = NULL;
    QString *name;
    QString *description;

    if (filterNotWidget)
    {
        filterDef = (ito::AddInAlgo::FilterDef*)(index.internalPointer());
        name = &(filterDef->m_name);
        description = &(filterDef->m_description);
    }
    else
    {
        widgetDef = (ito::AddInAlgo::AlgoWidgetDef*)(index.internalPointer());
        name = &(widgetDef->m_name);
        description = &(widgetDef->m_description);
    }
    
    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
            case 0: //name
            {
                return *name;
            }
            case 1: //type
            {
                if (filterNotWidget)
                {
                    return tr("Filter");
                }
                return tr("Widget");
            }
            case 7: //description
            {
                return *description;
            }
            default:
            {
                return QVariant();
            }
        }
    }
    else if (role == Qt::DecorationRole)
    {
        if (index.column() == 0)
        {
            if (filterNotWidget)
            {
                return m_iconFilter;
            }
            else
            {
                return m_iconWidget;
            }
        }
        return QVariant();
    }
    else if (role == Qt::ToolTipRole)
    {
        switch(index.column())
        {
            case 0: //name
            {
                return *name;
            }
            case 7: //description
            {
                return *description;
            }
            default:
            {
                return QVariant();
            }
        }
    }
    else if (role == Qt::TextAlignmentRole)
    {
        if (index.column() >= 0 && index.column() < m_alignment.size())
        {
            return m_alignment[index.column()];
        }
        else
        {
            return QVariant();
        }
    }
    else if (role == Qt::UserRole + 1) //returns type (OR-combination)
    {
        return ito::typeAlgo;
    }
    else if (role == Qt::UserRole + 2) //returns true if item is a category or sub-category, else false
    {
        return false;
    }
    else if (role == Qt::UserRole + 3) //returns tItemType
    {
        if(filterNotWidget)
        {
            return itemFilter;
        }
        else
        {
            return itemWidget;
        }
    }

    return QVariant();
}

//----------------------------------------------------------------------------------------------------------------------------------
/** return data elements for a given row
*   @param [in] index   index for which the data elements should be delivered
*   @param [in] role    the current role of the model 
*   @return data of the selected element, depending on the element's row and column (passed in index.row and index.column)
*
*   This method is actually used to fill the tree view. It returns the data for the selected element, depending as well on the 
*   column of the selected element, passed in index.column. The method here is divded into two parts. The first one handels requests
*   for root elements (plugins) the second one is used for child elements (instances of plugins).
*/
QVariant PlugInModel::data(const QModelIndex &index, int role) const
{
    tItemType itemType;
    size_t itemInternalData;

    if (!getModelIndexInfo(index, itemType, itemInternalData))
    {
        return QVariant();
    }
    
    switch (itemType)
    {
        case itemCatDataIO:
        {
            return getFixedNodeInfo(index, tr("DataIO"), itemType, role, m_iconDataIO);
        }
        case itemCatActuator:
        {
            return getFixedNodeInfo(index, tr("Motor"), itemType, role, m_iconActuator);
        }
        case itemCatAlgo:
        {
            return getFixedNodeInfo(index, tr("Algorithm"), itemType, role, m_iconAlgo);
        }
        case itemCatDesignerWidget:
        {
            return getFixedNodeInfo(index, tr("Designer Widgets"), itemType, role, m_iconDesignerWidget);
        }
        case itemSubCategoryDataIO_Grabber:
        {
            return getFixedNodeInfo(index, tr("Grabber"), itemType, role, m_iconGrabber);
        }
        case itemSubCategoryDataIO_ADDA:
        {
            return getFixedNodeInfo(index, tr("ADDA"), itemType, role, m_iconADDA);
        }
        case itemSubCategoryDataIO_RawIO:
        {
            return getFixedNodeInfo(index, tr("Raw IO"), itemType, role, m_iconRawIO);
        }
        case itemPlugin:
        {
            ito::AddInInterfaceBase *aiib = (ito::AddInInterfaceBase*)(itemInternalData);
            for (int i = 0 ; i < sizeof(m_treeFixNodes) / sizeof(m_treeFixNodes[0]) ; i++)
            {
                if (aiib->getType() == m_treeFixNodes[i])
                {
                    return getPluginNodeInfo(index, role);
                }
            }
            break;
        }
        case itemInstance:
        {
            ito::AddInBase *aib = (ito::AddInBase*)(itemInternalData);
            ito::AddInInterfaceBase *aiib = aib->getBasePlugin();
            ito::AddInManager *aim = ito::AddInManager::getInstance();

            if (aiib->getType() & ito::typeActuator)
            {
                int count = aim->getActList()->count();
                for (int i = 0 ; i < count ; i++)
                {
                    if (aim->getActList()->at(i) == (QObject*)aiib)
                    {
                        return getInstanceNodeInfo(index, role);
                    }
                }
            }
            else if(aiib->getType() & ito::typeDataIO)
            {
                int count = aim->getDataIOList()->count();
                for (int i = 0; i < count; i++)
                {
                    if (aim->getDataIOList()->at(i) == (QObject*)aiib)
                    {
                        return getInstanceNodeInfo(index, role);
                    }
                }
            }
            break;
        }
        case itemFilter:
        {
            return getFilterOrWidgetNodeInfo(index, role, true);
        }
        case itemWidget:
        {
            return getFilterOrWidgetNodeInfo(index, role, false);
        }
        //default:
        //{
        //    return QVariant();
        //}
    }

    return QVariant();
}

//----------------------------------------------------------------------------------------------------------------------------------
QModelIndex PlugInModel::getIndexByAddIn(ito::AddInBase *ai) const
{
    if (ai == NULL)
    {
        return QModelIndex();
    }

    QModelIndex baseIndex = getIndexByAddInInterface(ai->getBasePlugin());
    if(baseIndex.isValid() == false)
    {
        return QModelIndex();
    }

    int rows = rowCount(baseIndex);
    QModelIndex idx;
    for (int i = 0; i < rows; i++)
    {
        idx = index(i, 0, baseIndex);
        if (idx.isValid() && idx.internalPointer() == (void*)ai)
        {
            return index(i, 0, baseIndex);
        }
    }
    return QModelIndex();
}

//----------------------------------------------------------------------------------------------------------------------------------
QModelIndex PlugInModel::getIndexByAddInInterface(AddInInterfaceBase *aib) const
{
    if (aib)
    {
        ito::AddInManager *aim = ito::AddInManager::getInstance();
        const QList<QObject*> *list = NULL;

        switch (aib->getType())
        {
            case ito::typeActuator:
                {
                    list = aim->getActList();

                    for (int i = 0; i < list->count(); i++)
                    {
                        if (qobject_cast<ito::AddInInterfaceBase*>(list->at(i)) == aib)
                        {
                            return index(i, 0, m_treeFixIndizes[1]);
                        }
                    }
                }
                break;
            case ito::typeAlgo:
                {
                    list = aim->getAlgList();

                    for (int i = 0; i < list->count(); i++)
                    {
                        if (qobject_cast<ito::AddInInterfaceBase*>(list->at(i)) == aib)
                        {
                            return index(i, 0, m_treeFixIndizes[2]);
                        }
                    }
                }
                break;
            case ito::typeDataIO | ito::typeGrabber:
            case ito::typeDataIO | ito::typeRawIO:
            case ito::typeDataIO | ito::typeADDA:
                {
                    list = aim->getDataIOList();
                    int countGrabber = 0;
                    int countRawIO = 0;
                    int countADDA = 0;
                    ito::AddInInterfaceBase *aib2 = NULL;

                    for (int i = 0; i < list->count(); i++)
                    {
                        aib2 = qobject_cast<ito::AddInInterfaceBase*>(list->at(i));
                        if (aib2 == aib)
                        {
                            if (aib->getType() & ito::typeGrabber)
                            {
                                return index(countGrabber, 0, m_treeFixIndizes[3]);
                            }
                            else if (aib->getType() & ito::typeRawIO)
                            {
                                return index(countRawIO, 0, m_treeFixIndizes[5]);
                            }
                            else if (aib->getType() & ito::typeADDA)
                            {
                                return index(countADDA, 0, m_treeFixIndizes[4]);
                            }
                            else
                            {
                                return QModelIndex();
                            }
                        
                        }

                        if (aib2->getType() & ito::typeGrabber)
                        {
                            countGrabber++;
                        }
                        if (aib2->getType() & ito::typeRawIO)
                        {
                            countRawIO++;
                        }
                        if (aib2->getType() & ito::typeADDA)
                        {
                            countADDA++;
                        }
                    }
                }
                break;
        }
    }        
        
    return QModelIndex();
}

//----------------------------------------------------------------------------------------------------------------------------------
bool PlugInModel::getIsAlgoPlugIn(size_t &internalData) const
{
    ito::AddInInterfaceBase *aiib = (ito::AddInInterfaceBase*)(internalData);
    return (aiib->getType() == m_treeFixNodes[2]);
}

//----------------------------------------------------------------------------------------------------------------------------------
bool PlugInModel::getIsGrabberInstance(size_t &internalData) const
{
    ito::AddInInterfaceBase *aiib = (ito::AddInInterfaceBase*)(internalData);
    return (aiib->getType() == typeGrabber + 1);
}

//----------------------------------------------------------------------------------------------------------------------------------
bool PlugInModel::insertInstance(ito::AddInInterfaceBase* addInInterface, bool beginOperation)
{
    if (beginOperation)
    {
        QModelIndex baseIndex = getIndexByAddInInterface(addInInterface);
        if (baseIndex.isValid())
        {
            int rows = rowCount(baseIndex);
            beginInsertRows(baseIndex, rows, rows); //append element
            return true;
        }
        return false;
    }
    else
    {
        endInsertRows();
        return true;
    }
}

//----------------------------------------------------------------------------------------------------------------------------------
bool PlugInModel::deleteInstance(ito::AddInInterfaceBase* /*addInInterface*/, ito::AddInBase *addInInstance, bool beginOperation)
{
    if (beginOperation)
    {
        QModelIndex index = getIndexByAddIn(addInInstance);
        if (index.isValid())
        {
            QModelIndex parentIdx = parent(index);
            beginRemoveRows(parentIdx, index.row(), index.row());
            return true;
        }
        return false;
    }
    else
    {
        endRemoveRows();
        return true;
    }
}

//----------------------------------------------------------------------------------------------------------------------------------
bool PlugInModel::resetModel(bool beginOperation)
{
    if (beginOperation)
    {
        beginResetModel();
    }
    else
    {
        endResetModel();
    }
    return true;
}

void PlugInModel::setDesignerPlugins( QList<DesignerPlugin> &designerPlugins )
{
    beginResetModel();
    m_designerPlugins = designerPlugins;
    endResetModel();
}