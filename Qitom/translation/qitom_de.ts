<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DialogAboutQItom</name>
    <message>
        <location filename="../ui/dialogAbout.ui" line="+73"/>
        <source>itom logo</source>
        <translation>ITOM Logo</translation>
    </message>
    <message>
        <location line="+229"/>
        <source>ito logo</source>
        <translation>ITO Logo</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Copy all version numbers to clip board</source>
        <translation>Kopiert alle Versionsnummern in die Zwischenablage</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy Versions</source>
        <translation>Versionen kopieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location line="-327"/>
        <source>About itom</source>
        <translation>Über itom</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Basic information</source>
        <translation>Informationen</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Contributors</source>
        <translation>Mitwirkende</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The following license text corresponds to the file COPYING.txt, which is provided with this installation of itom. </source>
        <translation>Die folgende Lizenz entspricht der Datei COPYING.txt, die mit dieser Installation von itom mitgeliefert wurde.</translation>
    </message>
    <message>
        <location line="-86"/>
        <location line="+53"/>
        <location line="+69"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location line="+83"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Institut für Technische Optik (&lt;/span&gt;&lt;a href=&quot;http://www.ito.uni-stuttgart.de&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;ITO&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;) &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Universität Stuttgart&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Prof. Dr. Wolfgang Osten&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Pfaffenwaldring 9, 70569 Stuttgart, Germany&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DialogEditBreakpoint</name>
    <message>
        <location filename="../ui/dialogEditBreakpoint.ui" line="+17"/>
        <source>Edit Breakpoint</source>
        <translation>Haltepunkt editieren</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Filename:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Line-Number:</source>
        <translation>Zeilennummer:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Condition:</source>
        <translation>&amp;Bedingungen:</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>&amp;Ignore Count:</source>
        <translation>&amp;Ignorierungen:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>temporary &amp;breakpoint</source>
        <translation>&amp;Temporärer Haltepunkt</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;enabled</source>
        <translation>&amp;Aktivierung</translation>
    </message>
</context>
<context>
    <name>DialogGoto</name>
    <message>
        <location filename="../ui/dialogGoto.ui" line="+14"/>
        <source>Go To</source>
        <translation>Gehe zu</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>line</source>
        <translation>Zeile</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>character</source>
        <translation>Zeichen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Line number (0 - 0, current: 0)</source>
        <translation>Zeilennummer (0 - 0, aktuell: 0)</translation>
    </message>
</context>
<context>
    <name>DialogIconBrowser</name>
    <message>
        <location filename="../ui/dialogIconBrowser.ui" line="+14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Insert and Close</source>
        <translation>Einfügen und Schließen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Copy to Clipboard</source>
        <translation>In Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>DialogLoadedPlugins</name>
    <message>
        <location filename="../ui/dialogLoadedPlugins.ui" line="+14"/>
        <source>Load Status of Plugins</source>
        <translation>Ladestatus der Plugins</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Filters</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Errors</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>Warnings</source>
        <translation>Warnungen</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>Messages</source>
        <translation>Meldungen</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>Plugin name</source>
        <translation>Plugin-Name</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Ignored</source>
        <translation>Ignoriert</translation>
    </message>
    <message>
        <location line="-232"/>
        <source>Load status of detected plugin files</source>
        <translation>Ladestatus der erkannten Plugin-Dateien</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Only display compatible plugins (debug / release)</source>
        <translation>Nur kompatible Plugins anzeigen (debug/release)</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Collapse all</source>
        <translation>Alles reduzieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Expand all</source>
        <translation>Alles erweitern</translation>
    </message>
</context>
<context>
    <name>DialogNewPluginInstance</name>
    <message>
        <location filename="../ui/dialogNewPluginInstance.ui" line="+14"/>
        <source>New Plugin Instance</source>
        <translation>Neue Plugin-Instanz</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Plugin Information</source>
        <translation>Plugin-Informationen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>[ICON]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[name]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[type]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Mandatory Parameters</source>
        <translation>Pflichtparameter</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Optional Parameters</source>
        <translation>Optionale Parameter</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Access instance with python</source>
        <translation>Zugriff auf Instanz mt Python</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>variable name in global workspace</source>
        <translation>Variablenname im ganzen Arbeitsbereich</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>variable name:</source>
        <translation>Variablenname:</translation>
    </message>
</context>
<context>
    <name>DialogOpenFileWithFilter</name>
    <message>
        <location filename="../ui/dialogOpenFileWithFilter.ui" line="+14"/>
        <source>File Import Assistant</source>
        <translation>Assistent für den Import von Dateien</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Information</source>
        <translation>Informationen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>[ICON]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Filename:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[name]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[filter]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Mandatory Parameters</source>
        <translation>Pflichtparameter</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Optional Parameters</source>
        <translation>Optionale Parameter</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&lt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>file is being loaded...</source>
        <translation>Datei wird geladen...</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save in global workspace</source>
        <translation>Als globale Variable speichern</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>variable name in global workspace</source>
        <translation>Variablenname im globalen Arbeitsbereich</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>variable name:</source>
        <translation>Variablenname:</translation>
    </message>
</context>
<context>
    <name>DialogPipManager</name>
    <message>
        <location filename="../ui/dialogPipManager.ui" line="+14"/>
        <source>Python Package Manager</source>
        <translation type="unfinished">Python Package-Manager</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Installed Packages</source>
        <translation type="unfinished">Installierte Packages</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Install...</source>
        <translation type="unfinished">Installieren...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update...</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Uninstall</source>
        <translation type="unfinished">Deinstallieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sudo Uninstall</source>
        <translation type="unfinished">Als &apos;Sudo&apos; deinstallieren</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Check for updates</source>
        <translation type="unfinished">Auf Updates prüfen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Reload</source>
        <translation type="unfinished">Neu laden</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Log</source>
        <translation type="unfinished">Protokoll</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Pip Settings</source>
        <translation type="unfinished">Pip-Einstellungen</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Version:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>No connection to pip available</source>
        <translation type="unfinished">Keine Verbindung zu Pip möglich</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Proxy:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Proxy in the form [user:passwd@]proxy.server:port</source>
        <translation type="unfinished">Proxy-Angaben in der Form [user:passwd@]proxy.server:port</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>[user:passwd]@proxy.server:port</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Timeout:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+33"/>
        <source>socket timeout (default: 15 s)</source>
        <translation type="unfinished">Verbindungs-Timeout (standard: 15 s)</translation>
    </message>
    <message>
        <location line="-27"/>
        <source> s</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Run pip in an isolated mode, ignoring environment variables and user configuration.</source>
        <translation type="unfinished">Pip im Isulationsmodus starten (Umgebungsvariabeln und Benutzerkonfiguration werden ignoriert).</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Run pip in an isolated mode</source>
        <translation type="unfinished">Pip im Isulationsmodus starten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Retries:</source>
        <translation type="unfinished">Wiederholungen:</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>OK</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DialogPipManagerInstall</name>
    <message>
        <location filename="../ui/dialogPipManagerInstall.ui" line="+14"/>
        <source>Install Package</source>
        <translation type="unfinished">Package installieren</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Install</source>
        <translation type="unfinished">Installation</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Install from Wheel archive (whl)</source>
        <translation type="unfinished">Vom Wheel-Archiv (whl) installieren</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Install from tar.gz archive</source>
        <translation type="unfinished">Vom tar.gz-Archive installieren</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Search Python package index for package name, download and install it</source>
        <translation type="unfinished">Python Package manuell herunterladen und installieren</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Package file or name:</source>
        <translation type="unfinished">:Package-Datei oder Package-Name:</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+60"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location line="-48"/>
        <source>Options (Basic pip options are also considered)</source>
        <translation type="unfinished">Optionen (enthält auch Basisoptionen für Pip)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Upgrade existing package if newer version is available</source>
        <translation type="unfinished">Aktualisierung wenn eine neuere Version verfügbar ist</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Install dependencies if required</source>
        <translation type="unfinished">Installation von abhängigen Modulen wenn nötig</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>If checked, indicate an URL or a local path that is searched for the indicated package</source>
        <translation type="unfinished">Eine URL oder ein lokales Verzeichnis, in dem nach dem Package gesucht wird</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Find links:</source>
        <translation type="unfinished">Suchpfad:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Ignore Python package index (pypi.python.org/simple). Only look at find-links URLs.</source>
        <translation type="unfinished">Online-Python Package ignorieren (es wird nur der Suchpfad berücksichtigt).</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Run install with sudo rights (linux only)</source>
        <translation type="unfinished">Installation mit sudo-Berechtigungen ausführen (nur Linux)</translation>
    </message>
</context>
<context>
    <name>DialogPluginPicker</name>
    <message>
        <location filename="../ui/dialogPluginPicker.ui" line="+14"/>
        <source>Plugin Picker</source>
        <translation>Plugin laden</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Choose plugin instance:</source>
        <translation>Plugin-Instanz auswählen:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>show plugins without active instances</source>
        <translation>Plugins ohne aktive Instanzen anzeigen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>create new instance</source>
        <translation>Neue Instanz erstellen</translation>
    </message>
</context>
<context>
    <name>DialogReloadModule</name>
    <message>
        <location filename="../ui/dialogReloadModule.ui" line="+14"/>
        <source>Python Modules</source>
        <translation>Python-Module</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>show build-in modules</source>
        <translation>BuildIn-Module anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>show modules lying in python-folder(s)</source>
        <translation>Module der Python-Ordner anzeigen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Information</source>
        <translation>Informationen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Module Name:</source>
        <translation>Modul-Name:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;click on item to see information&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Path:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reload Modules</source>
        <translation>Modul neu laden</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>DialogReplace</name>
    <message>
        <location filename="../ui/dialogReplace.ui" line="+32"/>
        <source>Find and Replace</source>
        <translation>Suchen und Ersetzen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Find what</source>
        <translation>Suchen nach</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Replace with</source>
        <translation>Ersetzen durch</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Find in</source>
        <translation>Suchen in</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Current Document</source>
        <translation>Aktuelles Dokument</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Selection</source>
        <translation>Markierung</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Find Next</source>
        <translation>Weitersuchen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F3</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Replace</source>
        <translation>Ersetzen</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Replace All</source>
        <translation>Alle ersetzen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Expand</source>
        <translation>Erweitert</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Match whole word only</source>
        <translation>Nur ganzes Wort suchen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Match case</source>
        <translation>Groß-/Kleinschreibung beachten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Wrap around</source>
        <translation>Vom Ende zum Anfang</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Regular expression</source>
        <translation>Reguläre Ausdrücke</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Direction</source>
        <translation>Suchrichtung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Up</source>
        <translation>Aufwärts</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Down</source>
        <translation>Abwärts</translation>
    </message>
</context>
<context>
    <name>DialogSaveFileWithFilter</name>
    <message>
        <location filename="../ui/dialogSaveFileWithFilter.ui" line="+14"/>
        <source>File Export Assistant</source>
        <translation>Assistent für den Export von Dateien</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Information</source>
        <translation>Informationen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>[ICON]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Filename:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[name]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[filter]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Mandatory Parameters</source>
        <translation>Pflichtparameter</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Optional Parameters</source>
        <translation>Optionale Parameter</translation>
    </message>
</context>
<context>
    <name>DialogSelectUser</name>
    <message>
        <location filename="../ui/dialogSelectUser.ui" line="+67"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Role</source>
        <translation type="obsolete">Rolle</translation>
    </message>
    <message>
        <source>Ini-File</source>
        <translation type="obsolete">INI-Datei</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>Run itom as...</source>
        <translation type="unfinished">Unter folgendem Benutzer starten...</translation>
    </message>
    <message>
        <source>User</source>
        <translation type="obsolete">Benutzer</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Permission</source>
        <translation type="unfinished">Berechtigungen</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Ini File</source>
        <translation>Ini-Datei</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location line="-77"/>
        <source>Select user</source>
        <translation type="unfinished">Benutzer auswählen</translation>
    </message>
</context>
<context>
    <name>DialogVariableDetail</name>
    <message>
        <location filename="../ui/dialogVariableDetail.ui" line="+17"/>
        <source>Variable Detail</source>
        <translation>Variablendetail</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Value:</source>
        <translation>Wert:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy name to clipboard</source>
        <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>copy</source>
        <translation>Kopieren</translation>
    </message>
</context>
<context>
    <name>FileDownloader</name>
    <message>
        <source>no network reply instance available</source>
        <translation type="vanished">Fehler beim Herunterladen: Keine Antwort erhalten</translation>
    </message>
    <message>
        <source>Requested URL forces a redirection. Maximum number of redirections exceeded.</source>
        <translation type="vanished">Angefragte URL erfordert eine Weiterleitung. Anzahl maximaler Weiterleitungen wurde jedoch überschritten.</translation>
    </message>
</context>
<context>
    <name>HelpTreeDockWidget</name>
    <message>
        <location filename="../ui/helpTreeDockWidget.ui" line="+26"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>CommandLinkButton</source>
        <translation></translation>
    </message>
    <message>
        <location line="-55"/>
        <source>&lt;&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Help database is loading...</source>
        <translation>Hilfedatenbank wird geladen...</translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <location filename="../organizer/addInManager.cpp" line="+460"/>
        <source>The file &apos;%1&apos; is not a valid Qt plugin.</source>
        <translation>Die Datei &apos;%1&apos; ist kein gültiges Qt Plugin.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../helper/paramHelper.cpp" line="+1404"/>
        <source>invalid parameter name</source>
        <translation>Ungültiger Parametername</translation>
    </message>
    <message>
        <location line="-1365"/>
        <source>Types of parameter &apos;%s&apos; is unequal to required type of interface parameter &apos;%s&apos;</source>
        <translation type="unfinished">Der Typ des Parameters &apos;%s&apos; entspricht nicht dem geforderten Typ des Schnittstellenparameters &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>In/Out flags of parameter &apos;%s&apos; are unequal to required flags of interface parameter &apos;%s&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>The parameter &apos;%s&apos; is restricted by meta information while the interface parameter &apos;%s&apos; is not.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+12"/>
        <location line="+25"/>
        <location line="+25"/>
        <location line="+25"/>
        <location line="+26"/>
        <location line="+25"/>
        <location line="+31"/>
        <location line="+25"/>
        <location line="+25"/>
        <location line="+25"/>
        <location line="+52"/>
        <location line="+40"/>
        <source>The type of the meta information of parameter &apos;%s&apos; is unequal to this of the interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-261"/>
        <source>The allowed integer range of parameter &apos;%s&apos; is smaller than the requested range from interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-50"/>
        <source>The allowed char range of parameter &apos;%s&apos; is smaller than the requested range from interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+157"/>
        <source>The allowed double range of parameter &apos;%s&apos; is smaller than the requested range from interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+68"/>
        <source>The string type of the meta information of parameter &apos;%s&apos; is unequal to this of the interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>String &apos;%s&apos;, requested by meta data of interface parameter &apos;%s&apos; could not be found in meta data of parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+70"/>
        <source>The allowed data object types of parameter &apos;%s&apos; are more restrictive than these required by the interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+12"/>
        <source>The minimum and maximum dimensions of the data object of parameter &apos;%s&apos; are more restrictive than these required by the interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-67"/>
        <source>The meta data of the interface parameter &apos;%s&apos; requires a plugin with name &apos;%s&apos;, but parameter &apos;%s&apos; does it not.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-254"/>
        <source>The allowed char range or the allowed range of numbers of elements of parameter &apos;%s&apos; is smaller than the requested range from interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+50"/>
        <source>The allowed integer range or the allowed range of numbers of elements of parameter &apos;%s&apos; is smaller than the requested range from interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>The allowed value range or the allowed interval/range of parameter &apos;%s&apos; is smaller than the requested range from interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+81"/>
        <source>The allowed double range or the allowed range of numbers of elements of parameter &apos;%s&apos; is smaller than the requested range from interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>The allowed value range or the allowed range of parameter &apos;%s&apos; is smaller than the requested range from interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Both parameter &apos;%s&apos; and interface parameter &apos;%s&apos; require different plugins.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>The minimum plugin type bit mask of parameter &apos;%s&apos; is more restrictive than this of the interface parameter &apos;%s&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+59"/>
        <source>meta data of interface parameter &apos;%s&apos; is unknown.</source>
        <translation type="unfinished">Die Meta-Daten der Parameterschnittstelle &apos;%s&apos; sind unbekannt.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>String &apos;%s&apos; does not fit to given string-constraints.</source>
        <translation type="unfinished">Der String &apos;%s&apos; entspricht nicht den geforderten String-Bedingungen.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+158"/>
        <source>AddIn must not be NULL</source>
        <translation>AddIn muss NULL sein</translation>
    </message>
    <message>
        <location line="-141"/>
        <location line="+25"/>
        <location line="+34"/>
        <location line="+31"/>
        <location line="+22"/>
        <source>value out of range [%1, %2]</source>
        <translation>Wert liegt außerhalb des Gültigkeitsbereichs [%1, %2]</translation>
    </message>
    <message>
        <location line="-106"/>
        <location line="+26"/>
        <location line="+34"/>
        <location line="+30"/>
        <location line="+22"/>
        <source>value does not fit to given step size [%1:%2:%3]</source>
        <translation>Wert entspricht nicht der angegebenen Schrittgröße [%1:%2:%3]</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>length of char array out of range [%1, %2]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>length of char array does not fit to given step size [%1:%2:%3]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+268"/>
        <source>the given meta information does not fit a an array of character values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-248"/>
        <source>length of integer array out of range [%1, %2]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>length of integer array does not fit to given step size [%1:%2:%3]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>length of integer array must be 2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+37"/>
        <location line="+168"/>
        <source>The 1st value %1 does not fit to given step size [%2:%3:%4]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+168"/>
        <source>The 2nd value %1 does not fit to given step size [%2:%3:%4]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-161"/>
        <source>The given integer array [v1,v2] is considered to be an interval but the size of the interval (v2-v1) is out of bounds [%1,%2]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The given integer array [v1,v2] is considered to be a range but the size of the range (1+v2-v1) is out of bounds [%1,%2]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+159"/>
        <source>The size of the interval (bound2-bound1) does not fit to given step size [%1:%2:%3]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-155"/>
        <source>The size of the range (1+bound2-bound1) does not fit to given step size [%1:%2:%3]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+79"/>
        <source>the given meta information does not fit a an array of integer values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>length of double array out of range [%1, %2]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>length of double array does not fit to given step size [%1:%2:%3]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>length of double array must be 2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The first value of the given double interval [%1,%2] is bigger than the second value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The given double array [v1=%1,v2=%2] is considered to be an interval but does not fit to v1=[%3,v2], v2=[v1,%4]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The given double array [v1,v2] is considered to be an interval but the size of the interval (v2-v1) is out of bounds [%1,%2]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+83"/>
        <location line="+151"/>
        <source>Index value is out of range [0, %i]</source>
        <translation>Indexwert liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>index-based parameter cannot be validated since non-index based parameter is an interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+199"/>
        <source>index is ouf of range [0, %i]</source>
        <translation>Index liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Paramater is no array type. Indexing not possible.</source>
        <translation>Der Parameter ist nicht vom Typ Array. Eine Indizierung ist nicht möglich.</translation>
    </message>
    <message>
        <location line="-818"/>
        <source>AddIn does not fit to minimum required type(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>AddIn must be of the following plugin: &apos;%s&apos;.</source>
        <translation type="unfinished">AddIn muss folgendes Plugin sein: &apos;%s&apos;.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>The given integer array [%1,%2] is considered to be an interval but the first value is bigger than the second one</source>
        <translation type="unfinished">Gegebenes Integer-Array [%1, %2] wird als Interval interpretiert, doch ist der erste Wert ist größer als der Zweite</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The given integer array [%1,%2] is considered to be an interval but does not fit to the limits [%3,%4]</source>
        <translation type="unfinished">Gegebenes Integer-Array [%1, %2] wird als Interval interpretiert, doch die Werte entsprechen nicht den Grenzwerten [%3, %4]</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The given integer array [%1,%2] is considered to be a range but the first value is bigger than the second one</source>
        <translation type="unfinished">Gegebenes Integer-Array [%1, %2] wird als Range interpretiert, doch ist der erste Wert ist größer als der Zweite</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The given integer array [%1,%2] is considered to be a range but does not fit to the limits [%3,%4]</source>
        <translation type="unfinished">Gegebenes Integer-Array [%1, %2] wird als Range interpretiert, doch die Werte entsprechen nicht den Grenzwerten [%3, %4]</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>roi[2] (width) is out of range [%1,%2]</source>
        <translation type="unfinished">roi[2] (Breits) liegt außerhalb des Gültigkeitsbereichs [%1, %2]</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>roi[2] (width) does not fit to given step size [%1:%2:%3]</source>
        <translation type="unfinished">roi[2] (Breits) hat nicht die vorgegebene Schrittgröße [%1:%2:%3]</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>roi[0] (x0) is out of range [%1,%2]</source>
        <translation type="unfinished">roi[0] (x0) liegt außerhalb des Gültigkeitsbereichs [%1, %2]</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>right side of roi exceeds the maximal limit of %1 (reduce x0 or width)</source>
        <translation type="unfinished">Die rechte Seite des RIO überschreitet den maximalen Grenzwert von %1 (x0 oder Breite reduzieren)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>roi[0] (x0) does not fit to given step size [%1:%2:%3]</source>
        <translation type="unfinished">roi[0] (x0) hat nicht die vorgegebene Schrittgröße [%1:%2:%3]</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>roi[3] (height) is out of range [%1,%2]</source>
        <translation type="unfinished">roi[3] (Höhe) liegt außerhalb des Gültigkeitsbereichs [%1, %2]</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>roi[3] (height) does not fit to given step size [%1:%2:%3]</source>
        <translation type="unfinished">roi[3] (Höhe) hat nicht die vorgegebene Schrittgröße [%1:%2:%3]</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>roi[1] (y0) is out of range [%1,%2]</source>
        <translation type="unfinished">roi[1] (y0) liegt außerhalb des Gültigkeitsbereichs [%1, %2]</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>bottom side of roi exceeds maximal limit of %1 (recude y0 or height)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>roi[1] (y0) does not fit to given step size [%1:%2:%3]</source>
        <translation type="unfinished">roi[1] (y0) hat nicht die vorgegebene Schrittgröße [%1:%2:%3]</translation>
    </message>
    <message>
        <location line="+191"/>
        <location line="+20"/>
        <source>index-based parameter cannot be validated since non-index based parameter has an unhandled type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+139"/>
        <source>Index-based parameter name requires an array-type parameter.</source>
        <translation type="unfinished">Indexbasierter Parametername erwartet einen Parameter vom Typ Array.</translation>
    </message>
    <message>
        <location line="-122"/>
        <location line="+139"/>
        <source>Parameter could not be converted to destination type.</source>
        <translation type="unfinished">Der Parameter kann nicht in den benötigten Zielparametertyp umgewandelt werden.</translation>
    </message>
    <message>
        <location line="-134"/>
        <location line="+139"/>
        <source>type of parameter does not fit to requested parameter type</source>
        <translation type="unfinished">Der Parametertyp entspricht nicht dem erwarteten Typ</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Name of given parameter is empty.</source>
        <translation type="unfinished">Name des übergebenen Parameters ist leer.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Parameter &apos;%1&apos; is read only.</source>
        <translation>Parameter &apos;%1&apos; ist schreibgeschützt.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Parameter &apos;%1&apos; not found.</source>
        <translation>Parameter &apos;%1&apos; wurde nicht gefunden.</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>param is no array</source>
        <translation>&apos;Param&apos; ist kein Array</translation>
    </message>
    <message>
        <location filename="../python/pythonCommon.cpp" line="+231"/>
        <location filename="../api/apiFunctions.cpp" line="+154"/>
        <source>Unknown parameter type</source>
        <translation>Unbekannter Parametertyp</translation>
    </message>
    <message>
        <location filename="../api/apiFunctions.cpp" line="+40"/>
        <source>Wrong number of parameters</source>
        <translation>Falsche Anzahl an Parametern</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+14"/>
        <source>Wrong parameter type</source>
        <translation>Falscher Parametertyp</translation>
    </message>
    <message>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+22"/>
        <location line="+48"/>
        <source>Filter name empty</source>
        <translation type="unfinished">Filtername ist leer</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>Filter not found</source>
        <translation type="unfinished">Der Filter wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+26"/>
        <location line="+48"/>
        <source>Filter &apos;%s&apos; not found</source>
        <translation type="unfinished">Filter &apos;%s&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="-56"/>
        <location line="+48"/>
        <source>Vectors paramsMand, paramsOpt and paramsOut must not be NULL</source>
        <translation type="unfinished">Die Vektoren paramsMand, paramsOpt und paramsOut dürfen nicht NULL sein</translation>
    </message>
    <message>
        <location line="-33"/>
        <location line="+48"/>
        <source>Filter parameters not found in hash table.</source>
        <translation type="unfinished">Der Filter-Parameter wurde nicht in der Hash-Tabelle gefunden.</translation>
    </message>
    <message>
        <location line="+19"/>
        <location line="+19"/>
        <location line="+21"/>
        <location line="+21"/>
        <source>Fatal error! Could not get addInManager instance!</source>
        <translation type="unfinished">Fataler Fehler! Keine addInManager-Instanz gefunden!</translation>
    </message>
    <message>
        <location line="-56"/>
        <source>No plugin name specified.</source>
        <translation type="unfinished">Kein Plugin-Name vergeben.</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>The data object &apos;%s&apos; must have %i dimensions (%i given)</source>
        <translation type="unfinished">Das DataObject &apos;%s&apos; muss %i Dimensionen besitzen (übergeben wurden %i)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The given data object must have %i dimensions (%i given)</source>
        <translation type="unfinished">Das übergebene DataObject &apos;%s&apos; muss %i Dimensionen besitzen (übergeben wurden %i)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The size of the %i. dimension of data object &apos;%s&apos; exceeds the given boundaries [%i, %i]</source>
        <translation type="unfinished">Die Größe der %i. Dimension des DataObjects &apos;%s&apos; übersteigt die aktuelle Begrenzung [%i, %i]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The size of the %i. dimension exceeds the given boundaries [%i, %i]</source>
        <translation type="unfinished">Die Größe der %i. Dimension übersteigt die aktuelle Begrenzung [%i, %i]</translation>
    </message>
    <message>
        <location filename="../python/pythonCommon.cpp" line="+1202"/>
        <source>Keyword autoLoadParams not of integer type</source>
        <translation type="unfinished">Schlüsselwort autoLoadParams ist nicht vom Typ Integer</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Could not delete Keyword: autoLoadParams</source>
        <translation type="unfinished">Schlüsselwort kann nicht gelöscht werden: autoLoadParams</translation>
    </message>
    <message>
        <location filename="../python/pythonPlugins.cpp" line="+540"/>
        <source>timeout while getting name parameter</source>
        <translation type="unfinished">Zeitüberschreitung beim Lesen des Parameternamens</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>you must provide at least one parameter with the name of the function</source>
        <translation type="unfinished">Es muss mindestens ein Parameter für dem Funktionsnamen unterstützt werden</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>the first function name parameter can not be interpreted as string</source>
        <translation type="unfinished">Der erste Parameter sollte der Funktionsname sein, kann jedoch nicht als String erkannt werden</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>plugin does not provide an execution of function &apos;%s&apos;</source>
        <translation type="unfinished">Das Plugin unterstützt nicht die Ausführung der Funktion &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>timeout while calling specific function in plugin.</source>
        <translation type="unfinished">Zeitüberschreitung beim Aufruf spezifischer Funktionen im Plugin.</translation>
    </message>
    <message>
        <location line="+136"/>
        <source>timeout while getting parameter</source>
        <translation type="unfinished">Zeitüberschreitung beim Lesen der Parameter</translation>
    </message>
    <message>
        <location line="+336"/>
        <source>timeout.</source>
        <translation>Zeitüberschreitung.</translation>
    </message>
    <message>
        <location line="+486"/>
        <source>timeout while calibration</source>
        <translation type="unfinished">Zeitüberschreitung bei der Kalibrierung</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>timeout while setting origin</source>
        <translation type="unfinished">Zeitüberschreitung beim Setzen auf den Nullpunkt</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>timeout while getting Status</source>
        <translation type="unfinished">Zeitüberschreitung beim Lesen des Status</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>timeout while getting position values</source>
        <translation type="unfinished">Zeitüberschreitung beim Lesen der Positionswerte</translation>
    </message>
    <message>
        <location line="+289"/>
        <source>timeout while setting absolute position</source>
        <translation type="unfinished">Zeitüberschreitung beim setzen der absoluten Position</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>timeout while setting relative position</source>
        <translation type="unfinished">Zeitüberschreitung beim Setzten der relativen Position</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Itom ActuatorPlugin type in python</source>
        <translation type="unfinished">Itom ActuatorPlugin-Typ in Python</translation>
    </message>
    <message>
        <location line="+974"/>
        <source>copyVal function only implemented for typeADDA and typeGrabber</source>
        <translation type="unfinished">Die Funktion &apos;copyVal&apos; wurde nur für ADDA-Wandler und Grabber implementiert</translation>
    </message>
    <message>
        <location line="+553"/>
        <source>Itom DataIOPlugin type in python</source>
        <translation type="unfinished">Itom DataIOPlugin-Typ in Python</translation>
    </message>
    <message>
        <location filename="../python/pythonUi.cpp" line="+3108"/>
        <source>no widget name specified</source>
        <translation type="unfinished">Kein Widget-Name vergeben</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>no addin-manager found</source>
        <translation type="unfinished">Der &apos;AddInManager&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>the first parameter must contain the widget name as string</source>
        <translation type="unfinished">Der erste Parameter muss einen String mit dem Widget-Name enthalten</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Could not find plugin widget with name &apos;%1&apos;</source>
        <translation type="unfinished">Das Plugin-Widget mit dem Namen &apos;%1&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Could not get parameters for plugin widget &apos;%1&apos;</source>
        <translation type="unfinished">Vom Plugin-Widget &apos;%1&apos; konnten keine Parameter gelesen werden</translation>
    </message>
    <message>
        <location filename="../organizer/helpSystem.cpp" line="+317"/>
        <source>file could not be opened.</source>
        <translation type="unfinished">Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Load XML file failed: file seems corrupt</source>
        <translation type="unfinished">Das Öffnen der XML-Datei schlug fehl: Datei scheint defekt zu sein</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load XML file failed:  wrong xml version</source>
        <translation type="unfinished">Das Öffnen der XML-Datei schlug fehl: Falsche XML-Version</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load XML file failed: wrong document encoding</source>
        <translation type="unfinished">Das Öffnen der XML-Datei schlug fehl: Falscher Dokumenten-Encoder</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Load XML file failed: could not intepret checksum content as uint</source>
        <translation type="unfinished">Das Öffnen der XML-Datei schlug fehl: Checksummenfehler</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>collection project file could not be opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+55"/>
        <source>error calling qcollectiongenerator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+31"/>
        <source>templates for plugin documentation not found. Directory &apos;docs/pluginDoc/template&apos; not available. Plugin documentation will not be built.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>folder &apos;build&apos; as subfolder of &apos;docs/pluginDoc&apos; could not be created. Plugin documentation will not be built.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>could not clear folder &apos;docs/pluginDoc/build&apos;. Plugin documentation will not be built.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>no plugin directory available. No plugin documentation will be built.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+73"/>
        <source>error opening index.html of template folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>error opening itomPluginDoc.qhp of template folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+47"/>
        <source>error writing index.html of template folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>error writing itomPluginDoc.qhp of template folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>could not copy folder &apos;docs/pluginDoc/template/_static&apos; to &apos;docs/pluginDoc/build/_static&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>error calling qhelpgenerator for creating the plugin documentation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../helper/versionHelper.cpp" line="+76"/>
        <source>none</source>
        <translation type="unfinished">nicht</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Warning: The version contains locally changed code!
</source>
        <translation type="unfinished">Warnung: Die Version enthält lokal geänderten Code!</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+19"/>
        <source>Build from a clean version.
</source>
        <translation type="unfinished">Das &apos;Build&apos; ist keine &apos;clean version&apos; von GIT.
</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>The version contains locally changed code! </source>
        <translation type="unfinished">Diese Version beinhaltet lokal geänderten Code! </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The version contains unversioned files (e.g. from pyCache-files)!</source>
        <translation type="unfinished">Diese Version beinhaltet nicht versionierte Dateien (z. B. von &apos;pyCache-Dateien)!</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>This version of itom is not under version control (no GIT or SVN)!
</source>
        <translation type="unfinished">Diese Version von itom ist nicht unter der Versionskontrolle (kein GIT oder SVN)!
</translation>
    </message>
    <message>
        <location filename="../python/pythonItom.cpp" line="+2464"/>
        <source>Menu element must have a valid key.</source>
        <translation type="unfinished">Menüelement muss einen gültigen Schlüssel haben.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>For menu elements of type &apos;BUTTON&apos; any type of code (String or callable method or function) must be indicated.</source>
        <translation type="unfinished">Für Menüelemente vom Typ &apos;button&apos; muss jede Art von Code (Sting oder Funktionen/Methoden) erkennbar sein.</translation>
    </message>
    <message>
        <location line="-234"/>
        <source>Button must have a valid name.</source>
        <translation type="unfinished">Der Button muss einen gültigen Namen haben.</translation>
    </message>
    <message>
        <location line="+248"/>
        <source>A menu element of type &apos;separator&apos; can not execute some code. Code argument is ignored.</source>
        <translation type="unfinished">Ein Menüelement vom Typ &apos;separator&apos; kann keinen Code ausführen. Der Code wird ignoriert.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>A menu element of type &apos;separator&apos; can not execute any function or method. Code argument is ignored.</source>
        <translation type="unfinished">Ein Menüelement vom Typ &apos;separator&apos; kann keine Funktionen oder Methoden ausführen. Der Code wird ignoriert.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>A menu element of type &apos;menu&apos; can not execute some code. Code argument is ignored.</source>
        <translation type="unfinished">Ein Menüelement vom Typ &apos;menu&apos; kann keinen Code ausführen. Der Code wird ignoriert.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>A menu element of type &apos;menu&apos; can not execute any function or method. Code argument is ignored.</source>
        <translation type="unfinished">Ein Menüelement vom Typ &apos;menu&apos; kann keine Funktionen oder Methoden ausführen. Der Code wird ignoriert.</translation>
    </message>
    <message>
        <location filename="../../plot/AbstractNode.h" line="+210"/>
        <source>Live data source for plot</source>
        <translation type="unfinished">Live Datenquelle für Anzeige</translation>
    </message>
    <message>
        <location filename="../api/apiFunctionsGraph.cpp" line="+265"/>
        <source>timeout while unregistering live image from camera.</source>
        <translation type="unfinished">Zeitüberschreitung beim Stoppen des Livebilds der Kamera.</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>timeout while loading plugin widget</source>
        <translation type="unfinished">Zeitüberschreitung beim Laden der Plugin-Widgets</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>error retrieving widget pointer</source>
        <translation type="unfinished">Fehler bei der Abfrage des Widget-Pointers</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>error closing dialog</source>
        <translation type="unfinished">Fehler beim Schließen des Dialogs</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>timeout showing dialog</source>
        <translation type="unfinished">Zeitüberschreitung beim Anzeigen des Dialogs</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>UI-Organizer is not available!</source>
        <translation type="unfinished">&apos;UI-Organizer&apos; ist nicht verfügbar!</translation>
    </message>
    <message>
        <location filename="../../plot/AbstractDObjPCLFigure.h" line="+80"/>
        <location line="+2"/>
        <location line="+2"/>
        <location filename="../../plot/AbstractDObjFigure.h" line="+73"/>
        <source>Source data for plot</source>
        <translation type="unfinished">Quelldaten für Plot</translation>
    </message>
    <message>
        <location filename="../../plot/AbstractDObjFigure.h" line="+1"/>
        <source>Actual output data of plot</source>
        <translation type="unfinished">Aktuelle Ausgabedaten des Plots</translation>
    </message>
    <message>
        <location filename="../organizer/addInManager.cpp" line="-201"/>
        <source>directory &apos;%1&apos; could not be found</source>
        <translation type="unfinished">Verzeichnis &apos;%1&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>plugins folder could not be found</source>
        <translation type="unfinished">Plugin-Ordner wurden nicht gefunden</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>filename is no AddIn-library: %1</source>
        <translation type="unfinished">Die Datei &apos;%1&apos; ist keine AddIn-Bibliothek</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>scan and load plugins (%1)</source>
        <translation type="unfinished">Plugin &apos;%1&apos; wird gescannt und geladen</translation>
    </message>
    <message>
        <source>Unable to load translation file &apos;%1&apos;.</source>
        <translation type="obsolete">Übersetzungsdatei &apos;%1&apos; konnte nicht geladen werden.</translation>
    </message>
    <message>
        <location filename="../organizer/designerWidgetOrganizer.cpp" line="+197"/>
        <location filename="../organizer/addInManager.cpp" line="+40"/>
        <source>Unable to find translation file.</source>
        <translation type="unfinished">Übersetzungsdatei wurde nicht gefunden.</translation>
    </message>
    <message>
        <location line="-11"/>
        <location filename="../organizer/addInManager.cpp" line="-12"/>
        <source>Unable to load translation file &apos;%1&apos;. Translation file is empty.</source>
        <translation type="unfinished">Die Übersetzungsdatei &apos;%1&apos; kann nicht geladen werden. Diese Datei ist leer.</translation>
    </message>
    <message>
        <location filename="../organizer/addInManager.cpp" line="+55"/>
        <source>AddIn with filename &apos;%1&apos; is unknown.</source>
        <translation type="unfinished">AddIn namens &apos;%1&apos; ist unbekannt.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>AddIn &apos;%1&apos; fits to the obsolete interface %2. The AddIn interface of this version of &apos;itom&apos; is %3.</source>
        <translation type="unfinished">AddIn &apos;%1&apos; ist neuer als die veraltete AddIn-Schnittstelle %2. Die AddIn-Schnittstelle dieser itom-Version ist %3.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>AddIn &apos;%1&apos; fits to a new addIn-interface, which is not supported by this version of itom. The AddIn interface of this version of &apos;itom&apos; is %2.</source>
        <translation type="unfinished">AddIn &apos;%1&apos; wurde für eine neuere AddIn-Schnittstelle erstellt, die aktuell nicht unterstützt wird. Die aktuelle ITOM-Version ist %2.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>AddIn &apos;%1&apos; does not fit to the general interface AddInInterfaceBase</source>
        <translation type="unfinished">AddIn &apos;%1&apos; passt nicht zur AddInInterfaceBase</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>AddIn &apos;%1&apos; is not derived from class QObject.</source>
        <translation type="unfinished">AddIn &apos;%1&apos; wurde nicht von der Klasse QObject abgeleitet.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Library &apos;%1&apos; was ignored. Message: %2</source>
        <translation type="unfinished">Bibliothek &apos;%1&apos; wurde ignoriert. Meldung: %2</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+9"/>
        <source>AddIn &apos;%1&apos; could not be loaded. Error message: %2</source>
        <translation type="unfinished">AddIn &apos;%1&apos; konnte nicht geladen werden. Fehlermeldung: %2</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>%1 (DataIO) loaded</source>
        <translation type="unfinished">%1 (DataIO) geladen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Plugin %1 (DataIO) already exists. Duplicate rejected.</source>
        <translation type="unfinished">Plugin %1 (DataIO) existiert bereits. Duplikat entfernt.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 (Actuator) loaded</source>
        <translation type="unfinished">%1 (Motor) geladen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Plugin %1 (Actuator) already exists. Duplicate rejected.</source>
        <translation type="unfinished">Plugin %1 (Motor) existiert bereits. Duplikat entfernt.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>error initializing plugin: %1</source>
        <translation type="unfinished">Fehler beim Initialisieren der Plugins: %1</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Filter &apos;%1&apos; rejected since a filter with the same name already exists in global filter list</source>
        <translation type="unfinished">Filter &apos;%1&apos; wurde abgelehnt. Es befindet sich bereits ein Filter dieses namens in der globalen Filterliste</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Filter %1 loaded</source>
        <translation type="unfinished">Filter %1 geladen</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Widget &apos;%1&apos; rejected since widget with the same name already exists in global plugin widget list</source>
        <translation type="unfinished">Widget &apos;%1&apos; wurde abgelehnt. Es befindet sich bereits ein Widget dieses namens in der globalen Plugin-Widget-Liste</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Widget %1 loaded</source>
        <translation type="unfinished">Widget %1 geladen</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>invalid plugin type. Only typeDataIO, typeActuator or typeAlgo are allowed.</source>
        <translation type="unfinished">Ungültiger Plugin-Typ. Nur &apos;typeDataIO&apos;, &apos;typeActuator&apos; und &apos;typeAlgo&apos; sind erlaubt.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Plugin &apos;%1&apos; not found in list of given type</source>
        <translation type="unfinished">Plugin &apos;%1&apos; wurde in der übergebenen Liste nicht gefunden</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>plugin not found</source>
        <translation type="unfinished">Plugin nicht gefunden</translation>
    </message>
    <message>
        <location line="+180"/>
        <location line="+143"/>
        <location line="+137"/>
        <source>Wrong plugin name</source>
        <translation type="unfinished">Falscher Plugin-Name</translation>
    </message>
    <message>
        <location line="-257"/>
        <location line="+143"/>
        <source>Base plugin or appropriate plugin type not indicated for this plugin.</source>
        <translation type="unfinished">Für dieses Plugin wurde kein Basis-Plugin oder geeigneter Plugin-Typ gefunden.</translation>
    </message>
    <message>
        <location line="-118"/>
        <source>timeout while initializing dataIO</source>
        <translation type="unfinished">Zeitüberschreitung während der Initialisierung von DataIO</translation>
    </message>
    <message>
        <location line="+43"/>
        <location line="+141"/>
        <location line="+86"/>
        <source>Parameter has own parameter management. Keyword &apos;autoLoadParams&apos; is ignored.</source>
        <translation type="unfinished">Parameter haben ein eigenes Management. Schlüsselwort &apos;autoLoadParams&apos; wurde ignoriert.</translation>
    </message>
    <message>
        <location line="-127"/>
        <source>timeout while initializing actuator</source>
        <translation type="unfinished">Zeitüberschreitung während der Initialisierung von Actuator</translation>
    </message>
    <message>
        <location line="+736"/>
        <source>no toolbox available</source>
        <translation type="unfinished">Keine Symbolleiste verfügbar</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>plugin not available</source>
        <translation type="unfinished">Plugin ist nicht verfügbar</translation>
    </message>
</context>
<context>
    <name>UserUiDialog</name>
    <message>
        <source>filename &apos;%s&apos; does not exist</source>
        <translation type="vanished">Dateiname &apos;%s&apos; existiert nicht</translation>
    </message>
    <message>
        <source>ui-file could not be correctly parsed.</source>
        <translation type="vanished">UI-Datei konnte nicht korrekt geparst werden.</translation>
    </message>
    <message>
        <source>itom</source>
        <translation type="vanished">itom</translation>
    </message>
    <message>
        <source>content-widget is empty.</source>
        <translation type="vanished">Element ist leer.</translation>
    </message>
    <message>
        <source>dialog button role is unknown</source>
        <translation type="vanished">Unbekannte Rolle des Dialog-Button</translation>
    </message>
</context>
<context>
    <name>WidgetFindWord</name>
    <message>
        <location filename="../ui/widgetFindWord.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+49"/>
        <source>find:</source>
        <translation>Suche:</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>type search text...</source>
        <translation>Suchtext...</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>up</source>
        <translation>Aufwärts</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shift+F3</source>
        <translation></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>down</source>
        <translation>Abwärts</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>F3</source>
        <translation></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>mark all</source>
        <translation>Alles markieren</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>whole word</source>
        <translation>Ganzes Wort</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>match case</source>
        <translation>Groß/Klein</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>wrap around</source>
        <translation>Vom Ende zum Anfang</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>reg. expr.</source>
        <translation>Reg. Ausdr.</translation>
    </message>
</context>
<context>
    <name>WidgetInfoBox</name>
    <message>
        <location filename="../ui/widgetInfoBox.ui" line="+14"/>
        <source>Information</source>
        <translation>Informationen</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>WidgetPropConsoleGeneral</name>
    <message>
        <location filename="../ui/widgetPropConsoleGeneral.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished">Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Paste text to console</source>
        <translation type="unfinished">Text aus der Zwischenablage in die Konsole einfügen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>If subparts of a code are copied from any script and pasted to the console, it might happen that the whole code block is already globally indentend. The code can then not be executed. Check the following option to let itom remove the global indentation level before pasting it to the console.</source>
        <translation type="unfinished">Wenn Teile eines Python-Codes in die Konsole eingefügt werden sollen kann es vorkommen, dass der einzufügende Code-Block bereits eingerückt ist und dadurch nicht ausgeführt werden kann. Um von itom den Code-Block automatisch anpassen zu lassen folgende Option auswählen.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Modify text in clipboard before pasting to console. </source>
        <translation type="unfinished">Text vor dem Einfügen in die Konsole modifizieren.</translation>
    </message>
</context>
<context>
    <name>WidgetPropConsoleLastCommand</name>
    <message>
        <location filename="../ui/widgetPropConsoleLastCommand.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>command history enabled (set disable to clear the history!)</source>
        <translation>Befehlsliste aktivieren (Deaktivieren löscht die Liste!)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>number of saving commands</source>
        <translation>Anzahl zu speichernde Befehle</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>do not add if the new command is equal to the last one</source>
        <translation>Nicht hinzufügen wenn der letzte Befehl in der Liste identisch ist</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>date color</source>
        <translation>Farbe der Datumsanzeige</translation>
    </message>
</context>
<context>
    <name>WidgetPropConsoleWrap</name>
    <message>
        <location filename="../ui/widgetPropConsoleWrap.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Line Wrap</source>
        <translation>Zeilenumbruch</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Lines are not wrapped</source>
        <translation>Zeile nicht umbrechen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Lines are wrapped at word boundaries</source>
        <translation>Wortweiser Zeilenumbruch</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Lines are wrapped at character boundaries</source>
        <translation>Zeichenweiser Zeilenumbruch</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Visual Appearance</source>
        <translation type="unfinished">Anzeige</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Flag at start of every wrapped line</source>
        <translation type="unfinished">Markierung zu Beginn jeder umgebrochenen Zeile</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+26"/>
        <source>No wrap flag</source>
        <translation type="unfinished">Keine Markierung</translation>
    </message>
    <message>
        <location line="-21"/>
        <location line="+26"/>
        <source>Wrap flag displayed by the text</source>
        <translation type="unfinished">Markierung beim Text</translation>
    </message>
    <message>
        <location line="-21"/>
        <location line="+26"/>
        <source>Wrap flag displayed by the border</source>
        <translation type="unfinished">Markierung am Rand</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Flag at end of every wrapped line</source>
        <translation type="unfinished">Markierung am Ende jeder umgebrochenen Zeile</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Number of characters a wrapped line is indented by</source>
        <translation type="unfinished">Anzahl Zeichen beim Einrücken einer umbegrochenen Zeile</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Wrap Indentation Mode</source>
        <translation type="unfinished">Einrückmodus beim Zeilenumbruch</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Wrapped sub-lines are indented by the amount indicated above</source>
        <translation type="unfinished">Umgebrochene Unterzeile mit oben angegebenem Wert einrücken</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wrapped sub-lines are indented by the same amount as the first sub-line</source>
        <translation type="unfinished">Folgende Unterzeilen wie die erste Unterzeile einrücken</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Wrapped sub-lines are indented by the same amount as the first sub-line plus one mor level of indentation</source>
        <translation type="unfinished">Jede Unterzeilen um einen Einzug mehr einrücken als die Vorherige</translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorAPI</name>
    <message>
        <location filename="../ui/widgetPropEditorAPI.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Add API</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>base path: </source>
        <translation>Stammverzeichnis: </translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorAutoCompletion</name>
    <message>
        <location filename="../ui/widgetPropEditorAutoCompletion.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>auto-completion enabled</source>
        <translation>Autovervollständigung aktivieren</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>threshold</source>
        <translation>Schwelle</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Case sensitive</source>
        <translation>Groß-/Kleinschreibung berücksichtigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use fill-up characters</source>
        <translation>Füllzeichen benutzen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>replace word right of the cursor if entry from list is selected</source>
        <translation>Ersetze Wörter hinter dem Cursor wenn der Eintrag von der Liste ausgewählt wird</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>show single</source>
        <translation>Einzeln anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sources for auto-completion</source>
        <translation>Quellen der Autovervollständigung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Use all available sources</source>
        <translation>Alle verfügbaren Quellen nutzen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use text in the current document as source</source>
        <translation>Den Text im aktuellen Dokument als Quelle nutzen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use any installed APIs as source</source>
        <translation>Jede installierte API als Quelle nutzen</translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorCalltips</name>
    <message>
        <location filename="../ui/widgetPropEditorCalltips.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>calltips enabled</source>
        <translation>Vorschläge aktivieren</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>number of calltips</source>
        <translation>Anzahl der Vorschläge</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Context display options</source>
        <translation>Optionen der Kontextanzeige</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Don&apos;t show context information</source>
        <translation>Kontextinformationen nicht anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show context information, if no prior autocompletion</source>
        <translation>Nur Kontextinformationen anzeigen wenn die Autovervollständigung nichts liefert</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Show context information</source>
        <translation>Kontextinformationen immer anzeigen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>A context is any scope (e.g. Python module) prior to the function/method name</source>
        <translation>Ein Kontext ist jeder Bereich (z. B. Python-Modul) vor dem Namen der Funktion/Methode</translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorGeneral</name>
    <message>
        <location filename="../ui/widgetPropEditorGeneral.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Indentation</source>
        <translation type="unfinished">Einzug</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Auto Indentation</source>
        <translation type="unfinished">Automatischer Einzug</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show Indentation Guides</source>
        <translation type="unfinished">Einzugsmarker anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use tabs for indentation</source>
        <translation type="unfinished">Tabs als Einzug verwenden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show Whitespace</source>
        <translation type="unfinished">Leerzeichen anzeigen</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Indentation Width</source>
        <translation type="unfinished">Einzugsgröße</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Indentation Warning</source>
        <translation type="unfinished">Warnungen beim Einrücken</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Indentation is displayed as being bad, if...</source>
        <translation type="unfinished">Warnen wenn...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>no warning</source>
        <translation type="unfinished">keine Warnung</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>indentation is made up of a different combination (tabs/spaces) compared to previous line</source>
        <translation type="unfinished">der Einzug zweier aufeinanderfolgender Zeilen unterschiedlich erstellt wurden (Tabs/Leerzeichen)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>indentation is made up of spaces followed by tabs</source>
        <translation type="unfinished">der Einzug aus einer Kombination von Leerzeichen und Tabs besteht</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>indentation contains spaces</source>
        <translation type="unfinished">der Einzug Leerzeichen enthält</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>indentation contains tabs</source>
        <translation type="unfinished">der Einzug Tabs enthält</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>End-of-line (EOL) mode</source>
        <translation type="unfinished">Zeilenende-Modus</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Windows</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unix</source>
        <translation></translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Mac</source>
        <translation></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Python Syntax-Checker (uses Python-Module &quot;frosted&quot;)</source>
        <translation type="unfinished">Python-Syntax-Checker (mit Python-Modul &quot;frosted&quot;)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Automatically include itom module for syntax checker</source>
        <translation type="unfinished">itom-Modul für den Syntax-Checker automatisch hinzufügen</translation>
    </message>
    <message>
        <source>Check intervall [sec]:</source>
        <extracomment>The timer is started when entering a new line</extracomment>
        <translation type="obsolete">Prüfintervall [Sek]:</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Check interval:</source>
        <extracomment>The timer is started when entering a new line</extracomment>
        <translation type="unfinished">Prüfintervall:</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+68"/>
        <source>[sec]</source>
        <translation></translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Class Navigator</source>
        <translation type="unfinished">Klassennavigator</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>This feature enables two comboboxes with 
 class- and method-navigation above the editor.</source>
        <translation type="unfinished">Diese Funktion blendet zwei Combo-Boxen zur 
Navigation der Klassen und Methoden über 
dem Editor ein.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Activate timer</source>
        <translation type="unfinished">Timer aktivieren</translation>
    </message>
</context>
<context>
    <name>WidgetPropEditorStyles</name>
    <message>
        <location filename="../ui/widgetPropEditorStyles.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Foreground Color</source>
        <translation>Vordergundsfarbe</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Background Color</source>
        <translation>Hintergrundsfarbe</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Font</source>
        <translation>Schriftart</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Fill to end of line</source>
        <translation>Bis Zeilenende ausfüllen</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Sample Text</source>
        <translation>Beispieltext</translation>
    </message>
    <message>
        <location line="-62"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location line="-28"/>
        <source>Overall font size:</source>
        <translation>Generelle Schriftgröße:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Reset to Default</source>
        <translation>Auf Standard zurücksetzen</translation>
    </message>
</context>
<context>
    <name>WidgetPropFigurePlugins</name>
    <message>
        <location filename="../ui/widgetPropFigurePlugins.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Installed Figure and Plot Plugins</source>
        <translation type="unfinished">Installierte Figures und Plugins</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Default Figures</source>
        <translation type="unfinished">Standard Figures</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Reset to standard</source>
        <translation type="unfinished">Zurücksetzen</translation>
    </message>
</context>
<context>
    <name>WidgetPropGeneralApplication</name>
    <message>
        <location filename="../ui/widgetPropGeneralApplication.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>show message before closing the application</source>
        <translation>Vor dem Schließen von itom fragen</translation>
    </message>
</context>
<context>
    <name>WidgetPropGeneralLanguage</name>
    <message>
        <location filename="../ui/widgetPropGeneralLanguage.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Current Language:</source>
        <translation>Aktuelle Sprache:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Affected after program restart!</source>
        <translation>Einstellung wird erst nach einem Programmneustart wirksam!</translation>
    </message>
</context>
<context>
    <name>WidgetPropHelpDock</name>
    <message>
        <location filename="../ui/widgetPropHelpDock.ui" line="+14"/>
        <source>Form</source>
        <translation>Fenster</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>List of all Modules in help-directory</source>
        <translation>Liste aller Module im Hilfeverzeichnis</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Behaviour-Options</source>
        <translation>Verhaltensoptionen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open external links</source>
        <translation>Externe Links öffnen</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>Show Modules and Packages</source>
        <translation>Module und Packages anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show Algorithms</source>
        <translation type="unfinished">Algorithmus anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show Widgets</source>
        <translation type="unfinished">Widget anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show DataIO and Actuator</source>
        <translation type="unfinished">DataIO und Actuatoren anzeigen</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Display Plaintext (html-source)</source>
        <translation type="unfinished">Klartext anzeigen (HTML-Quelle)</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Help-Update configuration</source>
        <translation type="unfinished">Konfiguration des Hilfe-Updates</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Server:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Download Timeout in sec:</source>
        <translation type="unfinished">Zeitüberschreitung für Download (Sek.):</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Refresh</source>
        <translation type="unfinished">Aktualisieren</translation>
    </message>
</context>
<context>
    <name>WidgetPropPythonGeneral</name>
    <message>
        <location filename="../ui/widgetPropPythonGeneral.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished">Fenster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Executing or debugging scripts with unsaved changes</source>
        <translation type="unfinished">Änderungen im Skript vor dem Ausführen nicht speichern</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>always ask to save scripts before execution</source>
        <translation type="unfinished">Vor dem Ausführen immer fragen ob das Skript gespeichert werden soll</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>automatically save all unsaved scripts before execution</source>
        <translation type="unfinished">Vor dem Ausführen das Skript immer speichern</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>do not save any unsaved scripts (last saved version is executed then)</source>
        <translation type="unfinished">Änderungen im Skript nicht speichern (letzte gespeicherte Version wird ausgeführt!)</translation>
    </message>
</context>
<context>
    <name>WidgetPropPythonStartup</name>
    <message>
        <location filename="../ui/widgetPropPythonStartup.ui" line="+14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Add File</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>base path: </source>
        <translation>Basis-Ordner: </translation>
    </message>
    <message>
        <location line="-45"/>
        <source>To change the order in which the items (files) are loaded, use Drag and Drop</source>
        <translation type="unfinished">Um die Reihenfolger der geladenen Einträge zu ändern, bitte Drag and Drop benutzen</translation>
    </message>
</context>
<context>
    <name>ito::AIManagerWidget</name>
    <message>
        <location filename="../widgets/AIManagerWidget.cpp" line="+77"/>
        <source>Configuration Dialog</source>
        <translation>Konfigurationsdialog</translation>
    </message>
    <message>
        <location line="+288"/>
        <source>Show Plugin Toolbox</source>
        <translation>Plugin-Toolbox anzeigen</translation>
    </message>
    <message>
        <location line="-276"/>
        <source>Close Instance</source>
        <translation>Instanz schließen</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>New Instance...</source>
        <translation>Neue Instanz...</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Send to Python...</source>
        <translation>An Python senden...</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Close all</source>
        <translation>Alles schließen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Live Image...</source>
        <translation>Livebild...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Snap Dialog...</source>
        <translation>Aufnahmedialog...</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Info...</source>
        <translation></translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Open Widget...</source>
        <translation>Fenster öffnen...</translation>
    </message>
    <message>
        <location line="+143"/>
        <source>List</source>
        <translation>Liste</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Details</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>plugins</source>
        <translation>Plugins</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>Hide Plugin Toolbox</source>
        <translation>Plugin-Toolbox ausblenden</translation>
    </message>
    <message>
        <location line="-274"/>
        <source>Show/Hide Plugin Toolbox</source>
        <translation>Plugin-Toolbox ein-/ausblenden</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Auto Grabbing</source>
        <translation>Auto-Grabbing</translation>
    </message>
    <message>
        <location line="+348"/>
        <source>The instance &apos;%1&apos; cannot be closed by GUI since it has been created by Python</source>
        <translation>Die Instanz &apos;%1&apos; kann nicht über die GUI geschlossen werden, da diese durch Python erstellt wurde</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The instance &apos;%1&apos; can temporarily not be closed since it is still in use by another element.</source>
        <translation>Die Instanz &apos;%1&apos; kann im Moment nicht geschlossen werden, da diese noch von anderen Elementen benutzt wird.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The instance &apos;%1&apos; can finally not be closed since there are still references to this instance from other componentents, e.g. python variables.</source>
        <translation>Die Instanz &apos;%1&apos; kann nicht endgültig geschlossen werden, da auf diese noch andere Komponenten (z. B. Python-Variablen) referenzieren.</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Warning while showing configuration dialog. Message: %1</source>
        <translation>Beim Anzeigen des Konfigurationsdialogs ist eine Warnung aufgetreten. Nachricht: %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning while showing configuration dialog</source>
        <translation>Beim Anzeigen des Konfigurationsdialogs ist eine Warnung aufgetreten</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error while showing configuration dialog. Message: %1</source>
        <translation>Fehler beim Anzeigen des Konfigurationsdialogs. Nachricht: %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error while showing configuration dialog</source>
        <translation>Fehler beim Anzeigen des Konfigurationsdialogs</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>error while creating new instance. 
Message: %1</source>
        <translation>Fehler beim Erzeugen einer neuen Instanz.
Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+36"/>
        <source>Error while creating new instance</source>
        <translation>Fehler beim Erzeugen einer neuen Instanz</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>warning while creating new instance. Message: %1</source>
        <translation>Warnung beim Erzeugen einer neuen Instanz. Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning while creating new instance</source>
        <translation>Warnung beim Erzeugen einer neuen Instanz</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>error while creating new instance. Message: %1</source>
        <translation>Fehler beim Erzeugen einer neuen Instanz. Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+19"/>
        <location line="+97"/>
        <source>Timeout</source>
        <translation>Zeitüberschreitung</translation>
    </message>
    <message>
        <location line="-97"/>
        <location line="+97"/>
        <source>Python did not response to the request within a certain timeout.</source>
        <translation>Python reagierte nicht innerhalb der zulässigen Zeit auf die Anfrag.</translation>
    </message>
    <message>
        <location line="-90"/>
        <location line="+97"/>
        <source>warning while sending instance to python. Message: %1</source>
        <translation>Warnung beim Senden der Instanz an Python. Meldung: %1</translation>
    </message>
    <message>
        <location line="-96"/>
        <location line="+97"/>
        <source>Warning while sending instance to python</source>
        <translation>Warnung beim Senden der Instanz an Python</translation>
    </message>
    <message>
        <location line="-93"/>
        <location line="+97"/>
        <source>error while sending instance to python. Message: %1</source>
        <translation>Fehler beim Senden der Instanz an Python. Meldung: %1</translation>
    </message>
    <message>
        <location line="-96"/>
        <location line="+97"/>
        <source>Error while sending instance to python</source>
        <translation>Fehler beim Senden der Instanz an Python</translation>
    </message>
    <message>
        <location line="-92"/>
        <location line="+98"/>
        <source>Python not available</source>
        <translation>Python nicht verfügbar</translation>
    </message>
    <message>
        <location line="-98"/>
        <location line="+98"/>
        <source>The Python engine is not available</source>
        <translation>Die Python-Engine ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-295"/>
        <location line="+4"/>
        <source>closing not possible</source>
        <translation>Schließen nicht möglich</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>final closing not possible</source>
        <translation>Endgültiges Schließen nicht möglich</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>warning while closing instance. Message: %1</source>
        <translation>Warnung beim Schließen der Instanz. Meldung: %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning while closing instance</source>
        <translation>Warnung beim Schließen der Instanz</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>error while closing instance. Message: %1</source>
        <translation>Fehler beim Schließen der Instanz. Meldung: %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error while closing instance</source>
        <translation>Fehler beim Schließen der Instanz</translation>
    </message>
    <message>
        <location line="+238"/>
        <source>Python variable name</source>
        <translation>Variablenname in Python</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Python variable name for saving this instance in global workspace</source>
        <translation>Variablenname in Python um diese Instanz in den globalen Workspace zu schreiben</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>instance</source>
        <translation>Instanz</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>Currently, you can only open user interfaces from plugins which does not have any mandatory or optional starting parameters</source>
        <translation>Derzeit können nur Benutzerschnittstellen von Plugins geöffnet werden, die keine Pflicht- oder optionale Startparameter haben</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>User interface of plugin could not be created. Returned handle is invalid.</source>
        <translation>Benutzerschnittstelle des Plugins konnte nicht erstellt werden. Gesendetes Handle ist ungültig.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>could not find instance of UiOrganizer</source>
        <translation>Die Instanz des UI-Organizers konnte nicht gefunden werden</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error while opening user interface from plugin.</source>
        <translation>Fehler beim Öffnen der Benutzerschnittstelle des Plugins.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning while opening user interface from plugin.</source>
        <translation>Warnung beim Öffnen der Benutzerschnittstelle des Plugins.</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>This instance is no grabber. Therefore no live image is available.</source>
        <translation>Diese Instanz ist kein Grabber. Daher ist kein Livebild verfügbar.</translation>
    </message>
</context>
<context>
    <name>ito::AbstractDockWidget</name>
    <message>
        <location filename="../widgets/abstractDockWidget.cpp" line="+111"/>
        <source>stay on top</source>
        <translation>Im Vordergrund anzeigen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>stay on top of all visible windows</source>
        <translation type="unfinished">Alle sichtbaren Fenster im Vordergrund anzeigen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>stay on top of main window</source>
        <translation>Hauptfenster im Vordergrund anzeigen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>stay on top of main window of itom</source>
        <translation type="unfinished">Hauptfenster im Vordergrund von itom anzeigen</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>docking toolbar</source>
        <translation>Symbolleiste Fenster andocken</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>dock widget</source>
        <translation>Fenster eindocken</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>undock widget</source>
        <translation>Fenster ausdocken</translation>
    </message>
    <message>
        <location line="+385"/>
        <source>toolbar &apos;%1&apos; is already available</source>
        <translation>Toolbar &apos;%1&apos; ist bereits vorhanden</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>toolbar &apos;%1&apos; not found</source>
        <translation>Toolbar &apos;%1&apos; wurde nicht gefunden</translation>
    </message>
</context>
<context>
    <name>ito::AbstractFilterDialog</name>
    <message>
        <location filename="../ui/abstractFilterDialog.cpp" line="+109"/>
        <location line="+10"/>
        <location line="+10"/>
        <location line="+36"/>
        <location line="+20"/>
        <source>size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>dims</source>
        <translation>Dimensionen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>type</source>
        <translation>Typ</translation>
    </message>
</context>
<context>
    <name>ito::AddInAlgo</name>
    <message>
        <location filename="../../common/addInInterface.h" line="+1023"/>
        <source>uninitialized vector for mandatory parameters!</source>
        <translation>Nicht-Inizialisierte Vektoren bei Pflichtparametern!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>uninitialized vector for optional parameters!</source>
        <translation>Nicht inizialisierte Vektoren bei optionalen Parametern!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>uninitialized vector for output parameters!</source>
        <translation>Nicht inizialisierte Vektoren bei Rückgabeparametern!</translation>
    </message>
</context>
<context>
    <name>ito::AddInManager</name>
    <message>
        <location filename="../organizer/addInManager.cpp" line="-923"/>
        <location line="+143"/>
        <location line="+136"/>
        <source>Plugin instance is invalid (NULL)</source>
        <translation>Die Plugin-Instanz ist ungültig (NULL)</translation>
    </message>
    <message>
        <location line="+585"/>
        <source>no configuration dialog available</source>
        <translation>Kein Konfigurationsdialogs vorhanden</translation>
    </message>
</context>
<context>
    <name>ito::AlgoInterfaceValidator</name>
    <message>
        <location filename="../organizer/algoInterfaceValidator.cpp" line="+84"/>
        <source>interface not found</source>
        <translation type="unfinished">Schnittstelle nicht gefunden</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>DataObject [in/out]</source>
        <translation type="unfinished">DataObjekt [ein/aus]</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+7"/>
        <location line="+7"/>
        <location line="+7"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>filename</source>
        <translation type="unfinished">Dateiname</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>PointCloud [in/out]</source>
        <translation type="unfinished">Punktewolke [ein/aus]</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>PolygonMesh [in/out]</source>
        <translation type="unfinished">Polygon [ein/aus]</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+21"/>
        <source>DataObject [in]</source>
        <translation type="unfinished">DataObjekt [ein]</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>PointCloud [in]</source>
        <translation type="unfinished">Punktewolke [ein]</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>PolygonMesh [in]</source>
        <translation type="unfinished">Polygon [ein]</translation>
    </message>
    <message>
        <location line="+202"/>
        <source>Number of mandatory parameters of given algorithm exceed the maximum value, given by algorithm interface.</source>
        <translation type="unfinished">Die Anzahl der Pflichtparameter überschreitet den Maximalwert für die angegebene Algorithmusschnittstelle.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Number of optional parameters of given algorithm exceed the maximum value, given by algorithm interface.</source>
        <translation type="unfinished">Die Anzahl der Optionalen Parameter überschreitet den Maximalwert für die angegebene Algorithmusschnittstelle.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Number of output parameters of given algorithm exceed the maximum value, given by algorithm interface.</source>
        <translation type="unfinished">Die Anzahl der Ausgabeparameter überschreitet den Maximalwert für die angegebene Algorithmusschnittstelle.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>The given algorithm interface is unknown</source>
        <translation type="unfinished">Die angegebene Algorithmusschnittstelle ist unbekannt</translation>
    </message>
    <message>
        <source>Types of parameter &apos;%s&apos; is unequal to required type of interface parameter &apos;%s&apos;</source>
        <translation type="obsolete">Der Typ des Parameters &apos;%s&apos; entspricht nicht dem geforderten Typ des Schnittstellenparameters &apos;%s&apos;</translation>
    </message>
    <message>
        <source>meta data of interface parameter &apos;%s&apos; is unknown.</source>
        <translation type="obsolete">Die Meta-Daten der Parameterschnittstelle &apos;%s&apos; sind unbekannt.</translation>
    </message>
</context>
<context>
    <name>ito::BreakPointDockWidget</name>
    <message>
        <location filename="../widgets/breakPointDockWidget.cpp" line="+93"/>
        <source>breakpoints</source>
        <translation type="unfinished">Haltepunkte</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>delete Breakpoint</source>
        <translation type="unfinished">Haltepunkt löschen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>delete all Breakpoints</source>
        <translation type="unfinished">Alle Haltepunkte löschen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>edit Breakpoints</source>
        <translation type="unfinished">Haltepunkte bearbeiten</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>En- or disable Breakpoint</source>
        <translation type="unfinished">Haltepunkt ein- und ausschalten</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>En- or disable all Breakpoints</source>
        <translation type="unfinished">Alle Haltepunkte ein- und ausschalten</translation>
    </message>
</context>
<context>
    <name>ito::BreakPointModel</name>
    <message>
        <location filename="../models/breakPointModel.cpp" line="+72"/>
        <source>line</source>
        <translation type="unfinished">Zeile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>condition</source>
        <translation type="unfinished">Anforderungen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>temporary</source>
        <translation type="unfinished">Temporär</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>enabled</source>
        <translation type="unfinished">Aktiv</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>ignore count</source>
        <translation type="unfinished">Anzahl Ignorierungen</translation>
    </message>
    <message>
        <location line="+322"/>
        <location line="+2"/>
        <source>yes</source>
        <translation type="unfinished">ja</translation>
    </message>
    <message>
        <location line="-2"/>
        <location line="+2"/>
        <source>no</source>
        <translation type="unfinished">nein</translation>
    </message>
</context>
<context>
    <name>ito::CallStackDockWidget</name>
    <message>
        <location filename="../widgets/callStackDockWidget.cpp" line="+60"/>
        <source>file</source>
        <translation type="unfinished">Datei</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>line</source>
        <translation type="unfinished">Zeile</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>method</source>
        <translation type="unfinished">Methode</translation>
    </message>
</context>
<context>
    <name>ito::ConsoleWidget</name>
    <message>
        <location filename="../widgets/consoleWidget.cpp" line="+880"/>
        <source>script execution</source>
        <translation type="unfinished">Skript wird ausgeführt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Python is not available</source>
        <translation type="unfinished">Python ist nicht verfügbar</translation>
    </message>
</context>
<context>
    <name>ito::DesignerWidgetOrganizer</name>
    <message>
        <location filename="../organizer/designerWidgetOrganizer.cpp" line="-39"/>
        <source>could not read interface &apos;ito.AbstractItomDesignerPlugin&apos;</source>
        <translation type="unfinished">Die Schnittstelle &apos;ito.AbstractItomDesignerPlugin&apos; konnte nicht gelesen werden</translation>
    </message>
    <message>
        <location line="+114"/>
        <source>DesignerWidget &apos;%1&apos; successfully loaded</source>
        <translation type="unfinished">Designer-Widget &apos;%1&apos; erfolgreich geladen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The version &apos;ito.AbstractItomDesignerPlugin&apos; in file &apos;%1&apos; does not correspond to the requested version (%2)</source>
        <translation type="unfinished">Die Version von &apos;ito.AbstractItomDesignerPlugin&apos; in der Datei &apos;%1&apos; deckt sich nicht mit der erforderlichen Version (%2)</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Plugin in file &apos;%1&apos; is no Qt DesignerWidget inherited from QDesignerCustomWidgetInterface</source>
        <translation type="unfinished">Das Plugin in der Datei &apos;%1&apos; ist kein von Qt DesignerWidget abgeleitetes QDesignerCustomWidgetInterface</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Plugin in file &apos;%1&apos; is a Qt Designer widget but no itom plot widget that inherits &apos;ito.AbtractItomDesignerPlugin&apos;</source>
        <translation type="unfinished">Das Plugin in der Datei &apos;%1&apos; ist ein Qt Designer-Widget, aber kein von &apos;ito.AbtractItomDesignerPlugin&apos; abgeleitetes itom-Plot-Widget</translation>
    </message>
    <message>
        <location line="+61"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Figure &apos;%s&apos; does not correspond to the minimum requirements</source>
        <translation type="unfinished">Figure &apos;%s&apos; entspricht nicht den Mindestvoraussetzungen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Figure &apos;%s&apos; not found</source>
        <translation type="unfinished">Figure &apos;%s&apos; nicht gefunden</translation>
    </message>
    <message>
        <location line="+76"/>
        <location line="+125"/>
        <source>The figure category &apos;%s&apos; is unknown</source>
        <translation type="unfinished">Die Figure-Kategorie &apos;%s&apos; ist unbekannt</translation>
    </message>
    <message>
        <location line="-67"/>
        <source>The figure class &apos;%1&apos; could not be found or does not support displaying the given type of data. The default class for the given data is used instead.</source>
        <translation type="unfinished">Die Klasse &apos;%1&apos; wurde nicht gefunden oder die Daten können nicht angezeigt werden. Es wird zur Anzeige der Daten die Standardklasse verwendet.</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>no plot figure plugin could be found that fits to the given category.</source>
        <translation type="unfinished">Kein Plot-Figure-Plugin gefunden, welches mit der vorgegebenen Kategorie übereinstimmt.</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>DataObject - Line</source>
        <translation type="unfinished">Datenobjekt - Linie</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>DataObject - Plane</source>
        <translation type="unfinished">Datenobjekt - Fläche</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>DataObject - Plane Stack</source>
        <translation type="unfinished">Datenobjekt - Fläschenstapel</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Point Cloud</source>
        <translation type="unfinished">Punktewolke</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>PolygonMesh</source>
        <translation type="unfinished">Polygon</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+54"/>
        <location line="+50"/>
        <location line="+34"/>
        <source>invalid type or no type defined</source>
        <translation type="unfinished">Ungültiger Typ oder keine Typendefinition</translation>
    </message>
    <message>
        <location line="-120"/>
        <source>Gray8</source>
        <translation type="unfinished">Grau8</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Gray16</source>
        <translation type="unfinished">Grau16</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Gray32</source>
        <translation type="unfinished">Grau32</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>RGB32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>ARGB32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>CMYK32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Float32</source>
        <translation type="unfinished">Fließkomma32</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Float64</source>
        <translation type="unfinished">Fließkomma64</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Complex</source>
        <translation type="unfinished">Komplex</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Static</source>
        <translation type="unfinished">Statisch</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cartesian</source>
        <translation type="unfinished">
Katesisch</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Polar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cylindrical</source>
        <translation type="unfinished">Zylindrisch</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>OpenGl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cuda</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>X3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Line Plot</source>
        <translation type="unfinished">Linien-Plot</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Image Plot</source>
        <translation type="unfinished">Bild-Plot</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Isometric Plot</source>
        <translation type="unfinished">Isometrischer Plot</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>3D Plot</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ito::DialogAboutQItom</name>
    <message>
        <location filename="../ui/dialogAbout.cpp" line="+42"/>
        <location line="+77"/>
        <location line="+14"/>
        <source>Could not load file %1. Reason: %2.</source>
        <translation type="unfinished">Die Datei &apos;%1&apos; kann nicht geladen werden. Grund: %2.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>copy</source>
        <translation type="unfinished">Kopieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The version string has been copied to the clipboard</source>
        <translation type="unfinished">Die Versionsinformationen wurde in die Zwischenablage kopiert</translation>
    </message>
</context>
<context>
    <name>ito::DialogGoto</name>
    <message>
        <location filename="../ui/dialogGoto.cpp" line="+36"/>
        <source>Line number (1 - %1, current: %2):</source>
        <translation type="unfinished">Zeilennummer (1 - %1, aktuell: %2):</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Character number (0 - %1, current: %2):</source>
        <translation type="unfinished">Zeichennummer (1 - %1, aktuell: %2):</translation>
    </message>
</context>
<context>
    <name>ito::DialogIconBrowser</name>
    <message>
        <location filename="../ui/dialogIconBrowser.cpp" line="+80"/>
        <source>Icon Browser</source>
        <translation type="unfinished">Icon-Suche</translation>
    </message>
</context>
<context>
    <name>ito::DialogOpenFileWithFilter</name>
    <message>
        <location filename="../ui/dialogOpenFileWithFilter.cpp" line="+58"/>
        <source>The name must start with a letter followed by numbers or letters [a-z] or [A-Z]</source>
        <translation type="unfinished">Der Name muss mit einem Buchstaben, gefolgt von Ziffern oder Buchstaben [a-z] oder [A-Z], beginnen</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Python variable name missing</source>
        <translation type="unfinished">Python Variablenname fehlt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>You have to give a variable name, under which the loaded item is saved in the global workspace</source>
        <translation type="unfinished">Für den globalen Bereich muss ein Variablenname vergeben werden</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>loading...</source>
        <translation type="unfinished">laden...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid parameters.</source>
        <translation type="unfinished">Ungültige Parameter.</translation>
    </message>
    <message>
        <location line="+27"/>
        <location line="+17"/>
        <source>An error occurred while loading the file.</source>
        <translation type="unfinished">Beim Laden der Datei ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location line="-15"/>
        <location line="+17"/>
        <source>Error while loading file</source>
        <translation type="unfinished">Fehler beim Laden der Datei</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Procedure still running</source>
        <translation type="unfinished">Wird noch ausgeführt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The file is still being loaded. Please wait...</source>
        <translation type="unfinished">Die Datei wird noch geladen. Bitte warten...</translation>
    </message>
</context>
<context>
    <name>ito::DialogPipManager</name>
    <message>
        <location filename="../ui/dialogPipManager.cpp" line="+193"/>
        <source>Abort</source>
        <translation type="unfinished">Abbruch</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The pip process is still running. Do you want to interrupt it?</source>
        <translation type="unfinished">Der Pip-Prozess läuft bereits. Soll dieser abgebrochen werden?</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Pip Manager</source>
        <translation type="unfinished">Pip-Manager</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning installing Numpy if itom is already running.</source>
        <translation type="unfinished">Warnung bei der Installation von Numpy im laufenden itom.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>If you try to install / upgrade Numpy if itom is already running, a file access error might occur, since itom already uses parts of Numpy. 

Click ignore if you want to try to continue the installation or click OK in order to stop the installation. 

In the latter case, the file &apos;restart_itom_with_pip_manager.txt&apos; is created in the directory &apos;%1&apos;, such that the pip manager is started one time as standalone application once you restart itom. Then, close all instances of itom or other software accessing Numpy, restart itom and try to upgrade Numpy.</source>
        <translation type="unfinished">Die Installation oder das Updaten von Numpy im laufenden itom könnte einen Fehler verursachen, wenn Teile von Numby bereits verwendet werden. 

Soll die Installation dennoch ausgeführt werden kann mit &apos;Ignorieren&apos; fortgesetzt werden oder mit Ok die Installation gestopt werden. 

Wenn die Datei &apos;restart_itom_with_pip_manager.txt&apos; im Verzeichnis &apos;%1&apos; erstellt wurde, wird der Pip-Manager beim nächsten Programmstart in einem &apos;Standalone&apos;-Modus geöffnet und es kann versucht werden Numpy zu updaten.</translation>
    </message>
    <message>
        <location line="+49"/>
        <location line="+7"/>
        <location line="+24"/>
        <location line="+7"/>
        <source>Uninstall package</source>
        <translation type="unfinished">Package-Deinstallation</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+31"/>
        <source>The package &apos;%1&apos; is used by at least one other package. Do you really want to uninstall it?</source>
        <translation type="unfinished">Das Package &apos;%1&apos; wird von mindestens einem anderen Package gebraucht. Soll es wirklich deinstalliert werden?</translation>
    </message>
    <message>
        <location line="-24"/>
        <location line="+31"/>
        <source>Do you really want to uninstall the package &apos;%1&apos;?</source>
        <translation type="unfinished">Soll das Package &apos;%1&apos; wirklich deinstalliert werden?</translation>
    </message>
</context>
<context>
    <name>ito::DialogPipManagerInstall</name>
    <message>
        <location filename="../ui/dialogPipManagerInstall.cpp" line="+48"/>
        <source>Install Package</source>
        <translation type="unfinished">Package-Installation</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update Package</source>
        <translation type="unfinished">Package-Update</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Select package archive</source>
        <translation type="unfinished">Package-Archiv auswählen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Select directory</source>
        <translation type="unfinished">Verzeichnis wählen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>choose whl archive...</source>
        <translation type="unfinished">Whl-Archiv wählen...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>choose tar.gz archive...</source>
        <translation type="unfinished">Tar.gz-Archiv wählen...</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>package-name</source>
        <translation type="unfinished">Package-Name</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Missing package</source>
        <translation type="unfinished">Fehlendes Package</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>You need to indicate a package</source>
        <translation type="unfinished">Es muss ein Package ausgewählt sein</translation>
    </message>
</context>
<context>
    <name>ito::DialogPluginPicker</name>
    <message>
        <location filename="../ui/dialogPluginPicker.cpp" line="+149"/>
        <source>error while creating new instance. 
Message: %1</source>
        <translation type="unfinished">Fehler beim Erzeugen einer neuen Instanz.
Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+36"/>
        <source>Error while creating new instance</source>
        <translation type="unfinished">Fehler beim Erzeugen einer neuen Instanz</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>warning while creating new instance. Message: %1</source>
        <translation type="unfinished">Warnung beim Erzeugen einer neuen Instanz. Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning while creating new instance</source>
        <translation type="unfinished">Warnung beim Erzeugen einer neuen Instanz</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>error while creating new instance. Message: %1</source>
        <translation type="unfinished">Fehler beim Erzeugen einer neuen Instanz. Meldung: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>choose plugin</source>
        <translation type="unfinished">Plugin auswählen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Please choose plugin you want to create a new instance from</source>
        <translation type="unfinished">Bitte Plugin zur Erzeugung einer neuen Instanz auswählen</translation>
    </message>
</context>
<context>
    <name>ito::DialogProperties</name>
    <message>
        <location filename="../ui/dialogProperties.cpp" line="+53"/>
        <source>Properties</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Editor - please choose subpage</source>
        <translation>Editor - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+6"/>
        <location line="+4"/>
        <location line="+2"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Editor - General</source>
        <translation>Editor - Allgemein</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>API</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Editor - API files</source>
        <translation>Editor - API-Dateien</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Calltips</source>
        <translation>Vorschläge</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Editor - calltips</source>
        <translation>Editor - Vorschläge</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Auto Completion</source>
        <translation type="unfinished">Autovervollständigung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Editor - auto completion</source>
        <translation type="unfinished">Editor - Autovervollständigung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Styles</source>
        <translation type="unfinished">Ansicht</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Editor - styles</source>
        <translation type="unfinished">Editor - Ansicht</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Console</source>
        <translation>Konsole</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Console - please choose subpage</source>
        <translation>Konsole - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Console - General</source>
        <translation type="unfinished">Konsole - Allgemein</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Line Wrap</source>
        <translation>Zeilenumbruch</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Console - Line Wrap</source>
        <translation>Konsole - Zeilenumbruch</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Command History</source>
        <translation type="unfinished">Befehlsliste</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Console - Command History</source>
        <translation type="unfinished">Konsole - Befehlsliste</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Python</source>
        <translation>Python</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Python - please choose subpage</source>
        <translation>Python - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Python - general</source>
        <translation type="unfinished">Python - Allgemein</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Startup</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Python - startups</source>
        <translation>Python - Autostart</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>General - please choose subpage</source>
        <translation>Allgemein - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>General - language</source>
        <translation>Allgemein - Sprache</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Application</source>
        <translation type="unfinished">Anwendung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>General - application</source>
        <translation type="unfinished">Allgemein - Anwendung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Help Viewer</source>
        <translation>Hilfeanzeige</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>General - Help Viewer</source>
        <translation>Allgemein - Hilfeanzeige</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Plots and Figures</source>
        <translation type="unfinished">Plots und Figures</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Plots and Figures - please choose subpage</source>
        <translation type="unfinished">Plots und Figures - Bitte Unterrubrik auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Default Plots</source>
        <translation type="unfinished">Standard-Plots</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Plots and Figures - Defaults</source>
        <translation type="unfinished">Plots und Figures - Standard</translation>
    </message>
</context>
<context>
    <name>ito::DialogReloadModule</name>
    <message>
        <location filename="../ui/dialogReloadModule.cpp" line="+68"/>
        <location line="+75"/>
        <source>Python Engine is invalid</source>
        <translation type="unfinished">Python-Engine ist ungültig</translation>
    </message>
    <message>
        <location line="-75"/>
        <location line="+75"/>
        <source>The Python Engine could not be found</source>
        <translation type="unfinished">Python-Engine wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="-57"/>
        <location line="+75"/>
        <source>connection problem</source>
        <translation type="unfinished">Verbindungsproblem</translation>
    </message>
    <message>
        <location line="-75"/>
        <source>No information about loaded modules could be retrieved by python.</source>
        <translation type="unfinished">Keine Informationen über geladene Module von Python verfügbar.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+4"/>
        <source>error while getting module list</source>
        <translation type="unfinished">Fehler beim Lesen der Modulliste</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+74"/>
        <source>Unknown error</source>
        <translation type="unfinished">Unbekannter Fehler</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Timeout while forcing python to reload modules.</source>
        <translation type="unfinished">Zeitüberschreitung beim erneuten Laden der Module.</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+4"/>
        <source>error while reloading modules</source>
        <translation type="unfinished">Fehler beim erneuten Laden der Module</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Module reload</source>
        <translation type="unfinished">Module erneut laden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The following modules could not be reloaded:
</source>
        <translation type="unfinished">Folgende Module konnten nicht geladen werden:</translation>
    </message>
</context>
<context>
    <name>ito::DialogReplace</name>
    <message>
        <location filename="../ui/dialogReplace.cpp" line="+285"/>
        <source>Expand</source>
        <translation type="unfinished">Erweitert</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Collaps</source>
        <translation type="unfinished">Einfach</translation>
    </message>
</context>
<context>
    <name>ito::DialogSelectUser</name>
    <message>
        <location filename="../ui/dialogSelectUser.cpp" line="+90"/>
        <source>Role</source>
        <translation type="unfinished">Rolle</translation>
    </message>
</context>
<context>
    <name>ito::DialogUserManagement</name>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>UserID is empty! Cannot create user!</source>
        <translation type="obsolete">Die Benutzer-ID ist leer! Der Benutzer kann nicht erstellt werden!</translation>
    </message>
    <message>
        <source>UserID already exists! Cannot create user!</source>
        <translation type="obsolete">Die Benutzer-ID wurde bereits vergeben! Benutzer kann nicht erstellt werden!</translation>
    </message>
    <message>
        <location filename="../widgets/userManagement.cpp" line="+172"/>
        <location line="+7"/>
        <location line="+7"/>
        <location line="+6"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>Warning</source>
        <translation type="unfinished">Warnung</translation>
    </message>
    <message>
        <source>No or invalid group entered, setting to developer!</source>
        <translation type="obsolete">Es wurde keine oder eine ungültige Gruppe ausgewählt. Als Gruppe wurde &apos;Entwickler&apos; eingestellt!</translation>
    </message>
    <message>
        <source>No user name entered, creating user with empty name!</source>
        <translation type="obsolete">Es wurde kein Benutzername eingegeben. Das Konto wurde mit einem leeren Namen erstellt!</translation>
    </message>
    <message>
        <source>Standard itom ini file not found, aborting!</source>
        <translation type="obsolete">Die Standard-Ini-Datei von itom wurde nicht gefunden! Der Vorgang wurde abgebrochen!</translation>
    </message>
    <message>
        <source>Could not copy standard itom ini file!</source>
        <translation type="obsolete">Die Standard-Ini-Datei von itom konnte nicht kopiert werden!</translation>
    </message>
    <message>
        <location line="-150"/>
        <source>Role</source>
        <translation type="unfinished">Rolle</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>User Management - Current User: </source>
        <translation type="unfinished">Benutzerverwaltung - Aktueller Benutzer: </translation>
    </message>
    <message>
        <location line="+41"/>
        <source>User ID not found, aborting!</source>
        <translation type="unfinished">Die Benutzer-ID wurde nicht gefunden! Der Vorgang wurde abgebrochen!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>User ID and ini file name mismatch, aborting!</source>
        <translation type="unfinished">Die Ini-Datei wurde nicht gefunden! Der Vorgang wurde abgebrochen!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>User name and ini file user name mismatch, aborting!</source>
        <translation type="unfinished">Der Benutzername und der Name in der Ini-Datei stimmen nicht überein! Der Vorgang wurde abgebrochen!</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot delete current user, aborting!</source>
        <translation type="unfinished">Der aktuelle Benutzer kann nicht gelöscht werden! Der Vorgang wurde abgebrochen!</translation>
    </message>
</context>
<context>
    <name>ito::DialogUserManagementEdit</name>
    <message>
        <location filename="../widgets/userManagementEdit.cpp" line="+55"/>
        <location line="+7"/>
        <location line="+6"/>
        <location line="+56"/>
        <location line="+6"/>
        <source>Error</source>
        <translation type="unfinished">Fehler</translation>
    </message>
    <message>
        <location line="-75"/>
        <source>Name is empty! Cannot create user!</source>
        <translation type="unfinished">Name ist leer! Benutzer kann nicht erstellt werden!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>UserID already exists! Cannot create user!</source>
        <translation type="unfinished">Die Benutzer-ID wurde bereits vergeben! Benutzer kann nicht erstellt werden!</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>No user name entered, aborting!</source>
        <translation type="unfinished">Kein Benutzername! Vorgang wird abgebrochen!</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>UserOrganizer not found!</source>
        <translation type="unfinished">&quot;UserOrganizer&quot; wurde nicht gefunden!</translation>
    </message>
    <message>
        <source>ItomSettings directory not found, aborting!</source>
        <translation type="obsolete">Das Verzeichnis &quot;itomSettings&quot; wurde nicht gefunden! Vorgang wird abgebrochen!</translation>
    </message>
    <message>
        <source>Could not copy standard itom ini file!</source>
        <translation type="obsolete">Die Standard-Ini-Datei von itom konnte nicht kopiert werden!</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>User Management - New User</source>
        <translation type="unfinished">Benutzerverwaltung - Neuer Benutzer erstellen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>User Management - Edit User</source>
        <translation type="unfinished">Benutzerverwaltung - Benutzer bearbeiten</translation>
    </message>
</context>
<context>
    <name>ito::FigureWidget</name>
    <message>
        <location filename="../widgets/figureWidget.cpp" line="+125"/>
        <source>subplot %1 (empty)</source>
        <translation type="unfinished">Subplot &apos;%1&apos; (leer)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Subplots</source>
        <translation type="unfinished">&amp;Subplots</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Windows</source>
        <translation type="unfinished">&amp;Fenster</translation>
    </message>
    <message>
        <location line="+57"/>
        <location line="+49"/>
        <location line="+69"/>
        <location line="+137"/>
        <source>designer widget of class &apos;%s&apos; cannot plot objects of type dataObject</source>
        <translation type="unfinished">Designer-Widget der Klasse &apos;%s&apos; kann keine Objekte vom Typ DataObject anzeigen</translation>
    </message>
    <message>
        <location line="-239"/>
        <location line="+49"/>
        <location line="+69"/>
        <location line="+19"/>
        <location line="+149"/>
        <source>designerWidgetOrganizer is not available</source>
        <translation type="unfinished">&apos;designerWidgetOrganizer&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-145"/>
        <source>camera is not available any more</source>
        <translation type="unfinished">Die Kamera ist nicht länger verfügbar</translation>
    </message>
    <message>
        <location line="+165"/>
        <source>areaRow out of range [0,%i]</source>
        <translation type="unfinished">&apos;areaRow&apos; liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>arealCol out of range [0,%i]</source>
        <translation type="unfinished">&apos;areaCol&apos; liegt außerhalb des Gültigkeitsbereichs [0, %i]</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>subplot %1</source>
        <translation type="unfinished">Subplot %1</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>could not create designer widget of class &apos;%s&apos;</source>
        <translation type="unfinished">Es kann kein Designer-Widget der Klasse &apos;%s&apos; erstellt werden</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>designerWidgetOrganizer or uiOrganizer is not available</source>
        <translation type="unfinished">&apos;designerWidgetOrganizer&apos; oder &apos;uiOrganizer&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>addInBase pointer is NULL</source>
        <translation type="unfinished">Der Pointer auf &apos;addInBase&apos; ist NULL</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>timeout while getting parameter &apos;%s&apos; from plugin</source>
        <translation type="unfinished">Zeitüberschreitung beim Lesen des Parameters &apos;%s&apos; vom Plugin</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>parameter &apos;%s&apos; is not defined in plugin</source>
        <translation type="unfinished">Parameter &apos;%s&apos; wurde im Plugin nicht definiert</translation>
    </message>
</context>
<context>
    <name>ito::FileDownloader</name>
    <message>
        <location filename="../helper/fileDownloader.cpp" line="+111"/>
        <location line="+101"/>
        <source>no network reply instance available</source>
        <translation type="unfinished">Fehler beim Herunterladen: Keine Antwort erhalten</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Requested URL forces a redirection. Maximum number of redirections exceeded.</source>
        <translation type="unfinished">Angefragte URL erfordert eine Weiterleitung. Anzahl maximaler Weiterleitungen wurde jedoch überschritten.</translation>
    </message>
</context>
<context>
    <name>ito::FileSystemDockWidget</name>
    <message>
        <location filename="../widgets/fileSystemDockWidget.cpp" line="+87"/>
        <source>last used directories</source>
        <translation>Zuletzt verwendetet Verzeichnisse</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Filter:</source>
        <translation>Filter:</translation>
    </message>
    <message>
        <location line="+142"/>
        <source>open new folder</source>
        <translation>Neues Verzeichnis öffnen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>change to parent folder</source>
        <translation>Zum übergeordneten Verzeichnis wechseln</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>copy path to clipboard</source>
        <translation>Pfad in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>get path from clipboard</source>
        <translation>Pfad aus der Zwischenablage übernehmen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>open file</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>execute file</source>
        <translation>Datei ausführen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>locate on disk</source>
        <translation>Verzeichnis anzeigen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+579"/>
        <location line="+13"/>
        <source>delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="-590"/>
        <source>cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>create new folder</source>
        <translation>Neuen Ordner erstellen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>create new python file</source>
        <translation>Neue Python-Datei erstellen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>List</source>
        <translation>Liste</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Details</source>
        <translation></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>file system</source>
        <translation>Dateisystem</translation>
    </message>
    <message>
        <location line="+139"/>
        <source>Itom Files</source>
        <translation>ITOM-Dateien</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Directory &apos;%1&apos; does not existing!</source>
        <translation>Verzeichnis &apos;%1&apos; existiert nicht!</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>select base directory</source>
        <translation>Aktuelles Stammverzeichnis auswählen</translation>
    </message>
    <message>
        <location line="+228"/>
        <source>the selected items</source>
        <translation type="unfinished">der ausgewählte Eintrag</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Do you really want to delete %1?</source>
        <translation>Soll &apos;%1&apos; wirklich gelöscht werden?</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Error while deleting &apos;%1&apos;!</source>
        <translation>Fehler beim Löschen von &apos;%1&apos;!</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>Error pasting items</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The selected items could not be pasted from the clipboard. Maybe their URLs already exist</source>
        <translation>Die markierten Objekte konnten nicht aus der Zwischenablage eingefügt werden. Vielleicht existieren die Objekte bereits</translation>
    </message>
    <message>
        <location line="+25"/>
        <location line="+24"/>
        <source>New Folder</source>
        <translation>Neuer Ordner</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Failed to create a new directory</source>
        <translation>Fehler beim Erstellen des neuen Ordners</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+28"/>
        <source>New Script</source>
        <translation>Neues Skript</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Failed to create a new script</source>
        <translation>Fehler beim Erstellen einer neuen Python-Datei</translation>
    </message>
</context>
<context>
    <name>ito::HelpDockWidget</name>
    <message>
        <location filename="../widgets/helpDockWidget.cpp" line="+69"/>
        <source>backwards</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>forwards</source>
        <translation>Vorwärts</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>expand tree</source>
        <translation>Baum erweitern</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>collapse tree</source>
        <translation>Baum reduzieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>reload database</source>
        <translation>Datenbank neu laden</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Windows</source>
        <translation type="unfinished">&amp;Fenster</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>type text to filter the keywords in the tree</source>
        <translation>Text für die Filterung von Schlüsselwörtern im Baum eingeben</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>navigation</source>
        <translation>Navigation</translation>
    </message>
</context>
<context>
    <name>ito::HelpTreeDockWidget</name>
    <message>
        <location filename="../ui/helpTreeDockWidget.cpp" line="+374"/>
        <source>Template Error: Parameters section is only defined by either the start or end tag.</source>
        <translation type="unfinished">Vorlagenfehler: Die Parameter-Sektion ist nur definiert um entweder ein Start- oder Endzeiger zu enthalten.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Template Error: End tag of parameters section comes before start tag.</source>
        <translation type="unfinished">Vorlagenfehler: Der Endzeiger der Parameter-Sektion liegt vor dem Startzeiger.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Parameters</source>
        <translation type="unfinished">Parameter</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+26"/>
        <source>Template Error: Returns section is only defined by either the start or end tag.</source>
        <translation type="unfinished">Vorlagenfehler: Die Rückgabe-Sektion ist nur definiert um entweder ein Start- oder Endzeiger zu enthalten.</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+26"/>
        <source>Template Error: End tag of returns section comes before start tag.</source>
        <translation type="unfinished">Vorlagenfehler: Der Endzeiger der Rückgabe-Sektion liegt vor dem Startzeiger.</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>Returns</source>
        <translation type="unfinished">Rückgabe</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Example</source>
        <translation type="unfinished">Beispiel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy example to clipboard</source>
        <translation type="unfinished">Beispiel in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+52"/>
        <location line="+132"/>
        <source>optional</source>
        <translation></translation>
    </message>
    <message>
        <location line="-156"/>
        <source>Unknown filter name &apos;%1&apos;</source>
        <translation type="unfinished">Unbekannter Filtername &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>Unknown algorithm plugin with name &apos;%1&apos;</source>
        <translation type="unfinished">Unbekanntes Algorithmus-Plugin namens &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>unknown type</source>
        <translation type="unfinished">Unbekannter Typ</translation>
    </message>
    <message>
        <location line="+156"/>
        <source>Template Error: %s section is only defined by either the start or end tag.</source>
        <translation type="unfinished">Vorlagenfehler: Die &quot;%s&quot;-Sektion ist nur definiert um entweder ein Start- oder Endzeiger zu enthalten.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Template Error: End tag of %s section comes before start tag.</source>
        <translation type="unfinished">Vorlagenfehler: Der Endzeiger der &quot;%s&quot;-Sektion liegt vor dem Startzeiger.</translation>
    </message>
    <message>
        <location line="+46"/>
        <location line="+21"/>
        <location line="+21"/>
        <source>Range: [%1,%2], Default: %3</source>
        <translation type="unfinished">Bereich: [%1, %2], Voreinstellung: %3</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+21"/>
        <location line="+21"/>
        <source>Range: [%1:%2:%3], Default: %4</source>
        <translation type="unfinished">Bereich: [%1:%2:%3], Voreinstellung: %4</translation>
    </message>
    <message>
        <location line="-37"/>
        <location line="+21"/>
        <location line="+21"/>
        <source>Default: %1</source>
        <translation type="unfinished">Voreinstellung: %1</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>RegExp: &apos;%1&apos;</source>
        <translation type="unfinished">RegAusdr.: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>RegExp: [%1]</source>
        <translation type="unfinished">RegAusdr.: [%1]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>RegExp: &lt;no pattern given&gt;</source>
        <translation type="unfinished">RegAusdr.: &lt;keine Vorlage&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Match: &apos;%1&apos;</source>
        <translation type="unfinished">Treffer: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Match: [%1]</source>
        <translation type="unfinished">Treffer: [%1]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Match: &lt;no pattern given&gt;</source>
        <translation type="unfinished">Treffer: &lt;keine Vorlage&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Wildcard: &apos;%1&apos;</source>
        <translation type="unfinished">Platzhalter: &apos;%1&apos;</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Wildcard: [%1]</source>
        <translation type="unfinished">Platzhalter: [%1]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Wildcard: &lt;no pattern given&gt;</source>
        <translation type="unfinished">Platzhalter: &lt;keine Vorlage&gt;</translation>
    </message>
    <message>
        <location line="+455"/>
        <source>Database %s could not be opened</source>
        <translation type="unfinished">Datenbank &apos;%s&apos; konnte nicht geöffnet werden</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Database %s could not be found</source>
        <translation type="unfinished">Datenbank &apos;%s&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Help database is loading...</source>
        <translation type="unfinished">Hilfedatenbank wird geladen...</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>No help database available! 
 go to Properties File -&gt; General -&gt; Helpviewer and check the selection</source>
        <translation type="unfinished">Keine Hilfe-Datenbank erreichbar!
Bitte unter Optionen -&gt; Allgemein -&gt; Hilfeanzeige die Einstellungen prüfen</translation>
    </message>
    <message>
        <location line="+507"/>
        <source>The protocol of the link is unknown. </source>
        <translation type="unfinished">Das Protokoll des Links ist unbekannt. </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do you want to try with the external browser?</source>
        <translation type="unfinished">Soll versucht werden den Link mit dem Browser zu öffnen?</translation>
    </message>
</context>
<context>
    <name>ito::IOHelper</name>
    <message>
        <location filename="../helper/IOHelper.cpp" line="+112"/>
        <location line="+720"/>
        <source>Multiple plugins</source>
        <translation type="unfinished">Multiple Plugins</translation>
    </message>
    <message>
        <location line="-720"/>
        <source>Multiple plugins provide methods to load the file of type &apos;%1&apos;. Please choose one.</source>
        <translation type="unfinished">Multiple Plugins unterstüzen Methoden um Dateien des Typs &apos;%1&apos; zu laden. Bitte einen auswählen.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>File &apos;%1&apos; could not be opened with registered external application</source>
        <translation type="unfinished">Die Datei &apos;%1&apos; konnte nicht mit der verknüpften externen Anwendung geöffnet werden</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Save selected variables as...</source>
        <translation type="unfinished">Speichern der markierten Variablen als...</translation>
    </message>
    <message>
        <location line="+26"/>
        <location line="+61"/>
        <location line="+115"/>
        <source>python engine not available</source>
        <translation type="unfinished">Python-Engine ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>variables cannot be imported since python is busy right now</source>
        <translation type="unfinished">Variablen können nicht importiert werden während Python läuft</translation>
    </message>
    <message>
        <location line="-166"/>
        <source>timeout while getting value from workspace</source>
        <translation type="unfinished">Zeitüberschreitung beim Lesen der Werte aus dem Arbeitsbereich</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>the number of values returned from workspace does not correspond to requested number</source>
        <translation type="unfinished">Die Anzahl der zurückgegebenen Werte aus dem Arbeitsbereich entspricht nicht der angeforderten Anzahl</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+115"/>
        <source>file cannot be opened</source>
        <translation type="unfinished">Datei kann nicht geöffnet werden</translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+61"/>
        <source>variables cannot be exported since python is busy right now</source>
        <translation type="unfinished">Variablen können nicht exportiert werden während Python ausgeführt wird</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>timeout while pickling variables</source>
        <translation type="unfinished">Zeitüberschreitung beim &quot;Pickeln&quot; von Variablen</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>timeout while saving variables to matlab file</source>
        <translation type="unfinished">Zeitüberschreitung beim Speichern von Variablen in eine Matlab-Datei</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+114"/>
        <source>suffix must be *.idc or *.mat</source>
        <translation type="unfinished">Dateiendung muss *.idc oder *.mat sein</translation>
    </message>
    <message>
        <location line="-89"/>
        <source>Import data</source>
        <translation type="unfinished">Daten importieren</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>file not found</source>
        <translation type="unfinished">Datei nicht gefunden</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>timeout while unpickling variables</source>
        <translation type="unfinished">Zeitüberschreitung beim &quot;Unpickeln&quot; von Variablen</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>timeout while loading matlab variables</source>
        <translation type="unfinished">Zeitüberschreitung beim Laden von Matlab-Variablen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>open python script</source>
        <translation type="unfinished">Python-Skript öffnen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>python (*.py)</source>
        <translation type="unfinished">Python (*.py)</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>timeout while opening script</source>
        <translation type="unfinished">Zeitüberschreitung beim Öffnen eines Skripts</translation>
    </message>
    <message>
        <location line="+228"/>
        <source>PolygonMesh and PointCloud not available since support of PointCloudLibrary is disabled in this version.</source>
        <translation type="unfinished">PolygonMesh und PointCloud sind in dieser Version von PointCloudLibrary nicht verfügbar.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The algorithm interface is not supported</source>
        <translation type="unfinished">Die Algorithmusschnittstelle wird nicht unterstützt</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Timeout while sending values to python</source>
        <translation type="unfinished">Zeitüberschreitung beim Versuch Werte an Pyhton zu senden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>A timeout occurred while content of loaded file has been sent to python workspace</source>
        <translation type="unfinished">Beim Senden des Dateiinhalts der geladenen Datei an Python ist eine Zeitüberschreitung aufgetreten</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>An error occured while importing the loaded file into the python workspace.</source>
        <translation type="unfinished">Beim Import der geladenen Datei in den Python-Workspace ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>A warning occured while importing the loaded file into the python workspace.</source>
        <translation type="unfinished">Beim Import der geladenen Datei in den Python-Workspace ist eine Warnung aufgetreten.</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Error while sending values to python</source>
        <translation type="unfinished">Fehler beim Senden von Werten an Python</translation>
    </message>
    <message>
        <location line="-700"/>
        <source>file %1 does not exist</source>
        <translation type="unfinished">Datei &apos;%1&apos; existiert nicht</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>file %1 can not be opened with this application</source>
        <translation type="unfinished">Datei &apos;%1&apos; kann mit dieser Anwendung nicht geöffnet werden</translation>
    </message>
    <message>
        <location line="+621"/>
        <source>Warning while sending values to python</source>
        <translation type="unfinished">Warnung beim Senden von Werten an Python</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>AlgoInterfaceValidator not available.</source>
        <translation type="unfinished">&apos;AlgoInterfaceValidator&apos; ist nicht verfügbar.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>AddInManager or PythonEngine not available</source>
        <translation type="unfinished">&apos;AddInManager&apos; oder &apos;PythonEngine&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>There is no plugin interface able to save the requested file type</source>
        <translation type="unfinished">Es gibt keine Plugin-Schnittstelle um diesen Dateityp zu speichern</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Multiple plugins provide methods to save the file of type &apos;%1&apos;. Please choose one.</source>
        <translation type="unfinished">Diverse Plugins unterstützen Methoden um Dateien des Typs &apos;%1&apos; zu speichern. Bitte eines wählen.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>algorithm interface not supported</source>
        <translation type="unfinished">Algorithmus-Schnittstelle wird nicht unterstützt</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>error while getting mand and out parameters from algorithm interface</source>
        <translation type="unfinished">Fehler beim Lesen von Ausgabeparametern der Algorithmus-Schnittstelle</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>AlgoInterfaceValidator not available</source>
        <translation type="unfinished">&apos;AlgoInterfaceValidato&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>AddInManager not available</source>
        <translation type="unfinished">AddInManager nicht verfügbar</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Python Scripts (*.py)</source>
        <translation type="unfinished">Python-Skripts (*.py)</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+4"/>
        <source>Itom Data Collection (*.idc)</source>
        <translation type="unfinished">Itom Datensammlung (*.idc)</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+4"/>
        <source>Matlab Matrix (*.mat)</source>
        <translation type="unfinished">Matlab-Matrizen (*.mat)</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>User Interfaces (*.ui)</source>
        <translation type="unfinished">Eigene Fenster (*.ui)</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Itom Files (%1)</source>
        <translation type="unfinished">Itom Dateien (%1)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>All Files (*.*)</source>
        <translation type="unfinished">Alle Dateien (*.*)</translation>
    </message>
</context>
<context>
    <name>ito::LastCommandDockWidget</name>
    <message>
        <location filename="../widgets/lastCommandDockWidget.cpp" line="+190"/>
        <source>clear list</source>
        <translation>Liste löschen</translation>
    </message>
</context>
<context>
    <name>ito::MainApplication</name>
    <message>
        <location filename="../mainApplication.cpp" line="+166"/>
        <location line="+4"/>
        <source>Version %1
%2</source>
        <translation></translation>
    </message>
    <message>
        <location line="-4"/>
        <source>64 bit (x64)</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>32 bit (x86)</source>
        <translation></translation>
    </message>
    <message>
        <location line="+94"/>
        <source>load translations...</source>
        <translation>Übersetzungen werden geladen...</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>load style...</source>
        <translation type="unfinished">Style wird geladen...</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>load process organizer...</source>
        <translation type="unfinished">&apos;Process Organizer&apos; wird geladen...</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>scan and load plugins...</source>
        <translation type="unfinished">Plugins werden gescannt und geladen...</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>start python...</source>
        <translation type="unfinished">Python wird gestartet...</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>load main window...</source>
        <translation type="unfinished">&apos;Main Window&apos; wird geladen...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>load ui organizer...</source>
        <translation type="unfinished">&apos;UI Organizer&apos; wird geladen...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>scan and load designer widgets...</source>
        <translation type="unfinished">&apos;Designer Widgets&apos; werden gescannt und geladen...</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>load script editor organizer...</source>
        <translation type="unfinished">&apos;Script Editor Organizer&apos; wird geladen...</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>execute startup scripts...</source>
        <translation type="unfinished">Startskripts werden ausgeführt...</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>scan and run scripts in autostart folder...</source>
        <translation type="unfinished">Skripts im Autostartordner werden gescannt und ausgeführt...</translation>
    </message>
    <message>
        <location line="+138"/>
        <source>Do you really want to exit the application?</source>
        <translation type="unfinished">Soll itom wirklich beendet werden?</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Python is still running. Please close it first before shutting down this application</source>
        <translation type="unfinished">Python läuft bereits. Bitte zuerst die laufende Anwendung beenden</translation>
    </message>
</context>
<context>
    <name>ito::MainWindow</name>
    <message>
        <location filename="../widgets/mainWindow.cpp" line="+112"/>
        <source>itom</source>
        <translation>itom</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Breakpoints</source>
        <translation type="unfinished">Haltepunkte</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>File System</source>
        <translation type="unfinished">Dateisystem</translation>
    </message>
    <message>
        <location line="-40"/>
        <source>itom (x64)</source>
        <translation></translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Call Stack</source>
        <translation type="unfinished">Aufrufliste</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Global Variables</source>
        <translation type="unfinished">Globale Variablen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Local Variables</source>
        <translation type="unfinished">Lokale Variablen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Plugins</source>
        <translation></translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Python could not be started. itom cannot be used in the desired way.</source>
        <translation type="unfinished">Python konnte nicht gestartet werden. itom kann so nicht benutzt werden.</translation>
    </message>
    <message>
        <location line="+344"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>New Script...</source>
        <translation>Neues Skript...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open File...</source>
        <translation>Datei öffnen...</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Properties...</source>
        <translation>Optionen...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>User Management...</source>
        <translation>Benutzerverwaltung...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>About Qt...</source>
        <translation>Über QT...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>About itom...</source>
        <translation>Über itom...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Loaded plugins...</source>
        <translation>Geladene Plugins...</translation>
    </message>
    <message>
        <location line="-449"/>
        <source>Command History</source>
        <translation type="unfinished">Befehlsliste</translation>
    </message>
    <message>
        <location line="+455"/>
        <source>Help...</source>
        <translation>Hilfe...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Script Reference</source>
        <translation type="unfinished">Skriptreferenz</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>UI Designer</source>
        <translation type="unfinished">UI-Designer</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Run python code in debug mode</source>
        <translation type="unfinished">Python-Code im Debug-Modus ausführen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>set whether internal python code should be executed in debug mode</source>
        <translation type="unfinished">Interner Pyhton-Code wird im Debug-Modus ausgeführt</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Shift+F10</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>continue</source>
        <translation type="unfinished">Fortsetzen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>F6</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>step</source>
        <translation type="unfinished">Einzelschritt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>F11</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>step over</source>
        <translation type="unfinished">Prozedurschritt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>F10</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>step out</source>
        <translation type="unfinished">Ausführen bis Rücksprung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Shift+F11</source>
        <translation></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Reload modules...</source>
        <translation type="unfinished">Geladene Module...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>autoreload modules</source>
        <translation type="unfinished">Automatisch ladende Module</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>autoreload before script execution</source>
        <translation type="unfinished">Automatisch laden vor der Skriptausführung</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>autoreload before single command</source>
        <translation type="unfinished">Automatisch laden vor den Einzelbefehlen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>autoreload before events and function calls</source>
        <translation type="unfinished">Automatisch laden vor den Ereignis- und Funktionsaufrufen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Package Manager...</source>
        <translation type="unfinished">Package-Manager...</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Application</source>
        <translation type="unfinished">Anwendung</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Tools</source>
        <translation type="unfinished">Tools</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>About</source>
        <translation type="unfinished">Über</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Python</source>
        <translation type="unfinished">Python</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>File</source>
        <translation type="unfinished">Datei</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Recently used files</source>
        <translation type="unfinished">Zuletzt verwendete Dateien</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>View</source>
        <translation type="unfinished">Ansicht</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Toolboxes</source>
        <translation type="unfinished">Werkzeuge</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Script</source>
        <translation type="unfinished">Skript</translation>
    </message>
    <message>
        <location line="+601"/>
        <source>key must not be empty.</source>
        <translation type="unfinished">Der Schlüssel darf nicht leer sein.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>invalid menu item type.</source>
        <translation type="unfinished">Ungültiger Typ von &quot;menu item&quot;.</translation>
    </message>
    <message>
        <location line="+356"/>
        <source>there is no python code associated with this action.</source>
        <translation type="unfinished">Dieser Komponente wurde kein Python-Code hinterlegt.</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>The UI designer (Qt designer) could not be started (%1).</source>
        <translation type="unfinished">Der UI-Designer (QT-Designer) konnte nicht geöffnet werden (%1).</translation>
    </message>
    <message>
        <location line="-1629"/>
        <location line="+609"/>
        <source>Help</source>
        <translation type="unfinished">Hilfe</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Reload modules</source>
        <translation type="unfinished">Geladene Module</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>no entries</source>
        <translation type="unfinished">kein Eintrag</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Ready</source>
        <translation type="unfinished">Fertig</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+15"/>
        <location line="+27"/>
        <source>python is being executed</source>
        <translation type="unfinished">Python wird ausgeführt</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>open file</source>
        <translation type="unfinished">Datei öffnen</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Error when preparing help or showing assistant.</source>
        <translation type="unfinished">Fehler bei der Erstellung oder beim Anzeigen des Hilfeassistenten.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error while showing assistant.</source>
        <translation type="unfinished">Fehler beim Anzeigen des Hilfeassistenten.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning when preparing help or showing assistant.</source>
        <translation type="unfinished">Warnung bei der Erstellung oder beim Anzeigen des Hilfeassistenten.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning while showing assistant.</source>
        <translation type="unfinished">Warnung beim Anzeigen des Hilfeassistenten.</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>The help assistant could not be started.</source>
        <translation type="unfinished">Der Hilfeassistent konnte nicht gestartet werden.</translation>
    </message>
    <message>
        <source>The toolbar &apos;</source>
        <translation type="obsolete">Die Symbolleiste &apos;</translation>
    </message>
    <message>
        <source>&apos; could not be found</source>
        <translation type="obsolete">&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+227"/>
        <source>one single menu element must be of type MENU [2]</source>
        <translation type="unfinished">Das Menüelement muss vom Typ MENU sein [2]</translation>
    </message>
    <message>
        <source>no menu element is indicated</source>
        <translation type="obsolete">Es wurde kein Menüelement erkannt</translation>
    </message>
    <message>
        <source>The menu item &apos;%s&apos; does already exist but is no menu type</source>
        <translation type="obsolete">Das Menüelement &apos;%s&apos;  existiert bereits, ist jedoch nicht Typ von MENU</translation>
    </message>
    <message>
        <source>menu item already exists.</source>
        <translation type="obsolete">Das Menüelement existiert bereits.</translation>
    </message>
    <message>
        <source>Invalid typeID.</source>
        <translation type="obsolete">Ungültige Typ-ID.</translation>
    </message>
    <message>
        <location line="+103"/>
        <location line="+4"/>
        <source>Add menu element</source>
        <translation type="unfinished">Menüpunkt hinzufügen</translation>
    </message>
    <message>
        <location line="+89"/>
        <location line="+64"/>
        <source>Remove menu element</source>
        <translation type="unfinished">Menüelement löschen</translation>
    </message>
    <message>
        <source>A user-defined menu with the key sequence &apos;%1&apos; could not be found</source>
        <translation type="obsolete">Das Benutzerdefiniertes Menü mit dem Schlüssel &apos;%1&apos; konnte nicht gefunden werden</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Current Directory: %1</source>
        <translation type="unfinished">Aktuelles Verzeichnis: %1</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Python is not available. This action cannot be executed.</source>
        <translation type="unfinished">Python ist nicht verfügbar. Diese Anwendung kann nicht ausgeführt werden.</translation>
    </message>
    <message>
        <source>there is no python code associated to this action.</source>
        <translation type="obsolete">Zu dieser &apos;Action&apos; existiert kein Python-Code.</translation>
    </message>
    <message>
        <source>The UI designer (Qt designer) could not be started.</source>
        <translation type="obsolete">Der UI-Designer (Qt designer) konnte nicht gestartet werden.</translation>
    </message>
</context>
<context>
    <name>ito::PaletteOrganizer</name>
    <message>
        <location filename="../organizer/paletteOrganizer.cpp" line="+615"/>
        <source>Palette %1 has a restricted access.</source>
        <translation type="unfinished">Die Palette &apos;%1&apos; hat eine Zugangsbeschränkung.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Palette %1 has a write protection.</source>
        <translation type="unfinished">Die Palette &apos;%1&apos; ist schreibgeschützt.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Palette %1 not found within palette list</source>
        <translation type="unfinished">Die Palette &apos;%1&apos; wurde in der Palettenliste nicht gefunden</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Destination vector not initialized</source>
        <translation type="unfinished">Der Zielvektor wurde nicht initialisiert</translation>
    </message>
</context>
<context>
    <name>ito::ParamInputParser</name>
    <message>
        <location filename="../ui/paramInputParser.cpp" line="+59"/>
        <location line="+113"/>
        <location line="+72"/>
        <source>Canvas widget does not exist any more</source>
        <translation type="unfinished">Container existiert nicht länger</translation>
    </message>
    <message>
        <location line="-155"/>
        <source>[no description]</source>
        <translation type="unfinished">[keine Beschreibung]</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>[Integer]</source>
        <translation type="unfinished">Ganzzahl</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[Char]</source>
        <translation type="unfinished">[Zeichen]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[Double]</source>
        <translation type="unfinished">Kommazahl</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[String]</source>
        <translation type="unfinished">Text</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[HW-Instance]</source>
        <translation type="unfinished">[HW-Instanz]</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[unknown]</source>
        <translation type="unfinished">[unbekannt]</translation>
    </message>
    <message>
        <location line="+48"/>
        <location line="+72"/>
        <source>QT error: Grid layout could not be identified</source>
        <translation type="unfinished">QT-Fehler: Grid Layout kann nicht identifiziert werden</translation>
    </message>
    <message>
        <location line="-38"/>
        <source>The parameter &apos;%1&apos; is invalid.</source>
        <translation type="unfinished">Der Parameter &apos;%1&apos; ist ungültig.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid input</source>
        <translation type="unfinished">Ungültiger Input</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>checked: 1, unchecked: 0</source>
        <translation type="unfinished">ausgewählt: 1, nicht ausgewählt: 0</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>%1 [Wildcard]</source>
        <translation type="unfinished">%1 [Maskenzeichen]</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 [Regular Expression]</source>
        <translation type="unfinished">%1 [Regulärer Ausdruck]</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>[None]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+37"/>
        <location line="+23"/>
        <source>QT error: Spin box widget could not be found</source>
        <translation type="unfinished">QT-Fehler: QSpinBox wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>QT error: Double spin box widget could not be found</source>
        <translation type="unfinished">QT-Fehler: QDoubleSpinBox wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>QT error: String input widget could not be found</source>
        <translation type="unfinished">QT-Fehler: String Input Widget wurde nicht gefunden</translation>
    </message>
</context>
<context>
    <name>ito::PipManager</name>
    <message>
        <location filename="../models/pipManager.cpp" line="+47"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Location</source>
        <translation type="unfinished">Speicherort</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Requires</source>
        <translation type="unfinished">Erforderlich</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Updates</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Summary</source>
        <translation type="unfinished">Hinweis</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Homepage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>License</source>
        <translation type="unfinished">Lizenz</translation>
    </message>
    <message>
        <location line="+155"/>
        <source>up to date</source>
        <translation type="unfinished">aktuell</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>new version %1 available</source>
        <translation type="unfinished">neue Version %1 verfügbar</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>unknown</source>
        <translation type="unfinished">unbekannt</translation>
    </message>
    <message>
        <location line="+285"/>
        <source>Could not start python pip
</source>
        <translation type="unfinished">Python-Pip kann nicht gestartet werden</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>An error occurred when attempting to read from the process.
</source>
        <translation type="unfinished">Beim Versuch auf den Prozess zuzugreifen ist ein Fehler aufgetreten.
</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>other error</source>
        <translation type="unfinished">Es ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Python pip crashed during execution
</source>
        <translation type="unfinished">Python-Pip ist bei der Ausführung abgestürzt</translation>
    </message>
</context>
<context>
    <name>ito::PlugInModel</name>
    <message>
        <location filename="../models/PlugInModel.cpp" line="+38"/>
        <source>Name</source>
        <translation type="unfinished">Name</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation type="unfinished">Typ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Filename</source>
        <translation type="unfinished">Dateiname</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Author</source>
        <translation type="unfinished">Autor</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>min. itom Version</source>
        <translation type="unfinished">min. ITOM-Version</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>max. itom Version</source>
        <translation type="unfinished">max. ITOM-Version</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Description</source>
        <translation type="unfinished">Beschreibung</translation>
    </message>
    <message>
        <location line="+706"/>
        <location line="+425"/>
        <source>Actuator</source>
        <translation type="unfinished">Motor</translation>
    </message>
    <message>
        <location line="-421"/>
        <location line="+429"/>
        <source>Grabber</source>
        <translation></translation>
    </message>
    <message>
        <location line="-425"/>
        <location line="+429"/>
        <source>ADDA</source>
        <translation type="unfinished">ADDA Wandler</translation>
    </message>
    <message>
        <location line="-425"/>
        <location line="+429"/>
        <source>Raw IO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-425"/>
        <location line="+413"/>
        <source>Algorithm</source>
        <translation type="unfinished">Algorithmus</translation>
    </message>
    <message>
        <location line="-157"/>
        <source>Filter</source>
        <translation type="unfinished">Filter</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Widget</source>
        <translation></translation>
    </message>
    <message>
        <location line="+147"/>
        <source>DataIO</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ito::PythonEngine</name>
    <message>
        <location filename="../python/pythonEngine.cpp" line="+340"/>
        <source>error importing sys in start python engine
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>error importing itom in start python engine
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>error redirecting stdout in start python engine
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>error redirecting stderr in start python engine
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+237"/>
        <source>the module itoFunctions could not be loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+11"/>
        <source>the module itoDebugger could not be loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>the module &apos;autoreload&apos; could not be loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+41"/>
        <source>deadlock in python setup.</source>
        <translation type="unfinished">Deadlock in Python.</translation>
    </message>
    <message>
        <location line="+158"/>
        <source>Python not initialized</source>
        <translation type="unfinished">Python ist nicht inizialisiert</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>method name not found in builtin itom</source>
        <translation type="unfinished">Die Methode wurde im ITOM-Builtin nicht gefunden</translation>
    </message>
    <message>
        <location line="+116"/>
        <source>Qt text encoding not compatible to python. Python encoding is set to latin 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Qt text encoding %1 not compatible to python. Python encoding is set to latin 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+173"/>
        <source>main dictionary is empty</source>
        <translation type="unfinished">Hauptwörterbuch ist leer</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+538"/>
        <source>syntax error</source>
        <translation type="unfinished">Syntaxfehler</translation>
    </message>
    <message>
        <location line="-496"/>
        <source>exiting desired.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>error while evaluating python string.</source>
        <translation type="unfinished">Fehler beim Evaluieren eines Python-Strings.</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>file does not exist</source>
        <translation type="unfinished">Datei existiert nicht</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>file could not be opened in readonly-mode</source>
        <translation type="unfinished">Datei konnte nicht im &quot;ReadOnly&quot;-Modus geöffnet werden</translation>
    </message>
    <message>
        <location line="+134"/>
        <location line="+119"/>
        <location line="+121"/>
        <source>Error while clearing all breakpoints in itoDebugger.</source>
        <translation type="unfinished">Fehler beim Löschen aller Haltepunkte im ITO-Debugger.</translation>
    </message>
    <message>
        <location line="+212"/>
        <source>Error while transmitting breakpoints to itoDebugger.</source>
        <translation type="unfinished">Fehler beim Übermitteln der Haltepunkte zum ITO-Debugger.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Error while editing breakpoint in itoDebugger.</source>
        <translation type="unfinished">Fehler beim Editiern eines Haltepunktes im ITO-Debugger.</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Error while clearing breakpoint in itoDebugger.</source>
        <translation type="unfinished">Fehler beim Löschen eines Haltepunktes im ITO-Debugger.</translation>
    </message>
    <message>
        <location line="+1680"/>
        <source>The number of names and values must be equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>It is not allowed to put variables in modes pyStateRunning, pyStateDebugging or pyStateDebuggingWaitingButBusy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+134"/>
        <source>it is not allowed to load variables in modes pyStateRunning, pyStateDebugging or pyStateDebuggingWaitingButBusy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-109"/>
        <source>values cannot be saved since workspace dictionary not available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Function &apos;%s&apos; in this workspace can not be overwritten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Method &apos;%s&apos; in this workspace can not be overwritten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Type or class &apos;%s&apos; in this workspace can not be overwritten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Module &apos;%s&apos; in this workspace can not be overwritten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>error while transforming value &apos;%s&apos; to PyObject*.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+64"/>
        <source>The number of names and types must be equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>values cannot be obtained since workspace dictionary not available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>item &apos;%1&apos; does not exist in workspace.</source>
        <translation type="unfinished">Das Objekt &apos;%1&apos; existiert nicht im Workspace.</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>It is not allowed to register an AddIn-instance in modes pyStateRunning, pyStateDebugging or pyStateDebuggingWaitingButBusy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+54"/>
        <source>Dictionary is not available</source>
        <translation type="unfinished">Wörterbuch ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>variable name &apos;%1&apos; already exists in dictionary</source>
        <translation type="unfinished">Der Variablenname &apos;%1&apos; existiert bereis im Dictionary</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No instance of python class dataIO could be created</source>
        <translation type="unfinished">Es konnte keine Instanz der Klasse &apos;DataID&apos; erstellt werden</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>No instance of python class actuator could be created</source>
        <translation type="unfinished">Es konnte keine Instanz der Klasse &apos;Motor&apos; erstellt werden</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>AddIn must be of type dataIO or actuator</source>
        <translation type="unfinished">AddIn muss vom Typ &apos;DataIO&apos; oder &apos;Motor&apos; sein</translation>
    </message>
    <message>
        <location line="+81"/>
        <location line="+72"/>
        <source>it is not allowed to get modules if python is currently executed</source>
        <translation type="unfinished">Es ist nicht möglich Python-Module anzufordern während Python ausgeführt wird</translation>
    </message>
    <message>
        <location line="-56"/>
        <location line="+72"/>
        <source>the script itomFunctions.py is not available</source>
        <translation type="unfinished">Das Skript &apos;itomFunctions.py&apos; ist nicht verfügbar</translation>
    </message>
    <message>
        <location line="-64"/>
        <source>error while loading the modules</source>
        <translation type="unfinished">Fehler beim Laden der Module</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>error while reloading the modules</source>
        <translation type="unfinished">Fehler beim erneuten Laden der Module</translation>
    </message>
</context>
<context>
    <name>ito::QsciApiManager</name>
    <message>
        <location filename="../organizer/qsciApiManager.cpp" line="+303"/>
        <source>The python syntax documents have changed. The API has been updated.</source>
        <translation type="unfinished">Die Python Syntaxliste hat sich geändert. Die API wurde aktuallisiert.</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>The generation of the python syntax API has been cancelled.</source>
        <translation type="unfinished">Die Erstellung der Python-Syntax-API wurde abgebrochen.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The python syntax documents have changed. The API is being updated...</source>
        <translation type="unfinished">Die Python Syntaxliste hat sich geändert. Die API wird aktuallisiert...</translation>
    </message>
</context>
<context>
    <name>ito::ScriptDockWidget</name>
    <message>
        <location filename="../widgets/scriptDockWidget.cpp" line="+540"/>
        <source>file open</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>file not found</source>
        <translation>Datei nicht gefunden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>the file %1 could not be found</source>
        <translation>Die Datei &apos;%1&apos; konnte nicht gefunden werden</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>invalid file format</source>
        <translation>Unzulässiges Dateiformat</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>the file %1 is no python macro</source>
        <translation>Die Datei &apos;%1&apos;  ist kein Python-Makro</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>The following files have been changed and should be safed:</source>
        <translation>Folgende Dateien wurden geändert und sollten gespeichert werden:</translation>
    </message>
    <message>
        <location line="+551"/>
        <source>move left</source>
        <translation>Nach Links verschieben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>move right</source>
        <translation>Nach rechts verschieben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>move first</source>
        <translation>An den Anfang verschieben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>move last</source>
        <translation>Ans Ende verschieben</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>close others</source>
        <translation>Alle anderen schließen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>close all</source>
        <translation>Alles schließen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>dock</source>
        <translation>Andocken</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>undock</source>
        <translation>Lösen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>new</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>save as...</source>
        <translation>Speichern unter...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>save all</source>
        <translation>Alles speichern</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>undo</source>
        <translation>Rückgängig</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>redo</source>
        <translation>Wiederholen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>comment</source>
        <translation>Kommentieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ctrl+R</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>uncomment</source>
        <translation>Auskommentieren</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ctrl+Shift+R</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>indent</source>
        <translation>Einrücken</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>unindent</source>
        <translation>Ausrücken</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>run</source>
        <translation>Start</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>F5</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>run selection</source>
        <translation>Auswahl starten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+6"/>
        <source>F6</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="-3"/>
        <source>stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Shift+F10</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>continue</source>
        <translation>Fortsetzen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>step</source>
        <translation>Einzelschritt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>F11</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>step over</source>
        <translation>Prozedurschritt</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>F10</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>step out</source>
        <translation>Ausführen bis Rücksprung</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Shift+F11</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>quick search...</source>
        <translation>Schnellsuche...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>icon &amp;browser...</source>
        <translation>Icon &amp;suchen...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;toggle bookmark</source>
        <translation>Lesezeichen ein-/aus&amp;schalten</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;next bookmark</source>
        <translation>&amp;Nächstes Lesezeichen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;previous bookmark</source>
        <translation>&amp;Vorheriges Lesezeichen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;clear all bookmarks</source>
        <translation>Alle Lesezeichen &amp;löschen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;insert codec...</source>
        <translation>&amp;Kodierung einfügen...</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>no entries</source>
        <translation>kein Eintrag</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>Recently used files</source>
        <translation>Zuletzt verwendete Dateien</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>bookmark</source>
        <translation>Lesezeichen</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>file toolbar</source>
        <translation>Symbolleiste Datei</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>edit toolbar</source>
        <translation>Symbolleiste Bearbeiten</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>script toolbar</source>
        <translation>Symbolleiste Skript</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>bookmark toolbar</source>
        <translation>Symbolleiste Lesezeichen</translation>
    </message>
    <message>
        <location line="+614"/>
        <location line="+87"/>
        <source>find and replace</source>
        <translation>Suchen und Ersetzen</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>&apos;%1&apos; was not found</source>
        <translation>&apos;%1&apos; wurde nicht gefunden</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>%1 occurrence(s) was replaced</source>
        <translation>%1 Vorkommen ersetzt</translation>
    </message>
    <message>
        <location line="-944"/>
        <source>Ctrl+H</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="-58"/>
        <source>print...</source>
        <translation>Drucken...</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>find and replace...</source>
        <translation>Suchen und Ersetzen...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ctrl+B</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>goto...</source>
        <translation>Gehe zu...</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ctrl+G</source>
        <comment>QShortcut</comment>
        <translation></translation>
    </message>
    <message>
        <location line="+109"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&amp;Script</source>
        <translation>&amp;Skript</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Windows</source>
        <translation>&amp;Fenster</translation>
    </message>
</context>
<context>
    <name>ito::ScriptEditorOrganizer</name>
    <message>
        <location filename="../organizer/scriptEditorOrganizer.cpp" line="+291"/>
        <source>Script Editor</source>
        <translation>Skript-Editor</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>The following files have been changed and should be safed:</source>
        <translation>Folgende Dateien wurden geändert und sollten gespeichert werden:</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>remember selection for the next time (can be reverted in property dialog)</source>
        <translation type="unfinished">Auswahl für das nächste Mal merken (Einstellung kann bei den Optionen geändert werden)</translation>
    </message>
</context>
<context>
    <name>ito::ScriptEditorWidget</name>
    <message>
        <location filename="../widgets/scriptEditorWidget.cpp" line="+271"/>
        <source>&amp;toggle bookmark</source>
        <translation type="unfinished">Lesezeichen ein-/aus&amp;schalten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>next bookmark</source>
        <translation type="unfinished">Nächstes Lesezeichen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>previous bookmark</source>
        <translation type="unfinished">Vorheriges Lesezeichen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>clear all bookmarks</source>
        <translation type="unfinished">Alle Lesezeichen löschen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;toggle breakpoint</source>
        <translation type="unfinished">Haltepunkt ein-/aus&amp;schalten</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+207"/>
        <source>&amp;disable breakpoint</source>
        <translation type="unfinished">Haltepunkt &amp;deaktivieren</translation>
    </message>
    <message>
        <location line="-206"/>
        <source>&amp;edit condition</source>
        <translation type="unfinished">Bedingungen &amp;bearbeiten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;next breakpoint</source>
        <translation type="unfinished">&amp;Nächste Haltepunkt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;previous breakpoint</source>
        <translation type="unfinished">&amp;Vorheriger Haltepunkt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;delete all breakpoints</source>
        <translation type="unfinished">Alle Haltepunkte &amp;löschen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;cut</source>
        <translation type="unfinished">&amp;Ausschneiden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>cop&amp;y</source>
        <translation type="unfinished">&amp;Kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;paste</source>
        <translation type="unfinished">&amp;Einfügen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;indent</source>
        <translation type="unfinished">Zeileneinzug ver&amp;größern</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;unindent</source>
        <translation type="unfinished">Zeileneinzug ver&amp;kleinern</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;comment</source>
        <translation type="unfinished">&amp;Kommentieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unc&amp;omment</source>
        <translation type="unfinished">Kommentierung &amp;aufheben</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;run script</source>
        <translation type="unfinished">Skript &amp;starten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>run &amp;selection</source>
        <translation type="unfinished">Auswahl &amp;starten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;debug script</source>
        <translation type="unfinished">Skript im &amp;Debug-Modus starten</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>sto&amp;p script</source>
        <translation type="unfinished">Skript sto&amp;ppen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;insert codec...</source>
        <translation type="unfinished">&amp;Kodierung einfügen...</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>&amp;enable breakpoint</source>
        <translation type="unfinished">Haltepunkte d&amp;eaktivieren</translation>
    </message>
    <message>
        <location line="+483"/>
        <source>insert codec</source>
        <translation type="unfinished">Kodierung einfügen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Choose an encoding of the file which is added to the first line of the script</source>
        <translation type="unfinished">Ein Kodierung für die erste Zeile im Skript auswählen</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+83"/>
        <location line="+48"/>
        <source>unsaved changes</source>
        <translation type="unfinished">Ungespeicherte Änderungen</translation>
    </message>
    <message>
        <location line="-131"/>
        <location line="+131"/>
        <source>there are unsaved changes in the current document. Do you want to save it first?</source>
        <translation type="unfinished">Es gibt noch ungespeicherte Änderungen im aktuellen Dokument. Sollen diese zuerst gespeichert werden?</translation>
    </message>
    <message>
        <location line="-114"/>
        <source>error while opening file</source>
        <translation type="unfinished">Fehler beim Öffnen der Datei</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>file %1 could not be loaded</source>
        <translation type="unfinished">Datei %1 konnte nicht geladen werden</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>there are unsaved changes in the document &apos;%1&apos;. Do you want to save it first?</source>
        <translation type="unfinished">Im Dokument &apos;%1&apos; gibt es noch ungespeicherte Änderungen. Sollen diese zuerst gespeichert werden?</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+59"/>
        <source>error while accessing file</source>
        <translation type="unfinished">Fehler beim Zugriff auf die Datei</translation>
    </message>
    <message>
        <location line="-59"/>
        <location line="+59"/>
        <source>file %1 could not be accessed</source>
        <translation type="unfinished">Auf die Datei %1 konnte nicht zugegriffen werden</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>save as...</source>
        <translation type="unfinished">Speichern unter...</translation>
    </message>
    <message>
        <location line="+698"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>There is nothing to print</source>
        <translation type="unfinished">Es gibt nichts zu drucken</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>unnamed</source>
        <translation>unbenannt</translation>
    </message>
    <message>
        <location line="+240"/>
        <source>The file &apos;%1&apos; does not exist any more.</source>
        <translation type="unfinished">Die Datei &apos;%1&apos; existiert nicht mehr.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Keep this file in editor?</source>
        <translation type="unfinished">Diese Datei im Editor belassen?</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>The file &apos;%1&apos; has been modified by another programm.</source>
        <translation type="unfinished">Die Datei &apos;%1&apos; wurde von einem anderen Programm geändert.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do you want to reload it?</source>
        <translation type="unfinished">Soll diese neu geladen werden?</translation>
    </message>
    <message>
        <location line="+161"/>
        <source>{Global Scope}</source>
        <translation>{Globaler Gültigkeitsbereich}</translation>
    </message>
    <message>
        <location filename="../widgets/scriptEditorWidget.h" line="+88"/>
        <source>Untitled%1</source>
        <translation type="unfinished">Unbenannt%1</translation>
    </message>
</context>
<context>
    <name>ito::UiOrganizer</name>
    <message>
        <location filename="../organizer/uiOrganizer.cpp" line="+288"/>
        <source>the plugin did not return a valid widget pointer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+46"/>
        <source>widget is NULL</source>
        <translation type="unfinished">Widget ist &apos;NULL&apos;</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>widgets of type QDockWidget are not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>plugin with name &apos;%1&apos; could be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+105"/>
        <source>figHandle %i is no handle for a figure window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>No internal dialog or window with name &apos;%1&apos; could be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+54"/>
        <source>ui-file &apos;%1&apos; could not be correctly parsed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>filename &apos;%1&apos; does not exist</source>
        <translation type="unfinished">Dateiname &apos;%1&apos; existiert nicht</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>dialog could not be created</source>
        <translation type="unfinished">Der Dialog kann nicht erstellt werden</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>A widget inherited from QDialog cannot be docked into the main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Main window not available for docking the user interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+131"/>
        <source>designer plugin widget (&apos;%1&apos;) could not be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>No designer plugin with className &apos;%s&apos; could be found. Please make sure that this plugin is compiled and the corresponding DLL and header files are in the designer folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+92"/>
        <location line="+25"/>
        <location line="+39"/>
        <location line="+47"/>
        <location line="+25"/>
        <location line="+25"/>
        <source>dialog handle does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-102"/>
        <source>dialog cannot be docked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+47"/>
        <source>dialog cannot be docked or undocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+213"/>
        <source>defaultButton must be within enum QMessageBox::StandardButton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>buttons must be within enum QMessageBox::StandardButtons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>defaultButton must appear in buttons, too.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+177"/>
        <location line="+93"/>
        <location line="+102"/>
        <location line="+146"/>
        <location line="+30"/>
        <location line="+51"/>
        <location line="+51"/>
        <location line="+129"/>
        <location line="+40"/>
        <location line="+55"/>
        <location line="+61"/>
        <source>object name is not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-686"/>
        <location line="+56"/>
        <source>property &apos;%1&apos; does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-49"/>
        <source>property &apos;%1&apos; could not be read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+73"/>
        <location line="+29"/>
        <source>property &apos;%1&apos; could not be written</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+38"/>
        <location line="+31"/>
        <source>The attribute number is out of range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+31"/>
        <location line="+25"/>
        <location line="+24"/>
        <location line="+1499"/>
        <location line="+37"/>
        <location line="+36"/>
        <location line="+26"/>
        <source>the objectID cannot be cast to a widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1358"/>
        <source>The object ID is invalid.</source>
        <translation type="unfinished">Die ID des Datenobjekts ist ungültig.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The given object ID is unknown.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+238"/>
        <source>object ID is not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-408"/>
        <source>could not get reference to main dialog or window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>uiHandle is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+38"/>
        <location line="+51"/>
        <source>no object name given.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-46"/>
        <location line="+51"/>
        <source>The object ID of the parent widget is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-46"/>
        <location line="+51"/>
        <source>The object ID of the parent widget is unknown.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+105"/>
        <source>parameter type %1 is unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+37"/>
        <source>signal does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>signal could not be connected to slot throwing a python keyboard interrupt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+55"/>
        <source>slot could not be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+411"/>
        <source>There exists no object with the given id.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+351"/>
        <source>unsupported data type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+64"/>
        <location line="+185"/>
        <source>figHandle %i is not handle for a figure window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2667"/>
        <location line="+2423"/>
        <location line="+64"/>
        <location line="+151"/>
        <location line="+34"/>
        <location line="+36"/>
        <source>figHandle %i not available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-169"/>
        <source>figure window is not available any more</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>handle &apos;%i&apos; is no figure.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>handle &apos;0&apos; cannot be assigned.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Figure</source>
        <translation>Abbildung</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>subplot at indexed position %i is not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+70"/>
        <source>figHandle %i is not a handle for a figure window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+43"/>
        <location line="+37"/>
        <location line="+37"/>
        <source>The desired widget has no signals/slots defined that enable the pick points interaction</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ito::UserModel</name>
    <message>
        <location filename="../models/UserModel.cpp" line="+36"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>role</source>
        <translation>Rolle</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>iniFile</source>
        <translation>INI-Datei</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>features</source>
        <translation type="unfinished">Eigenschaften</translation>
    </message>
    <message>
        <location line="+196"/>
        <location line="+14"/>
        <source>Developer</source>
        <translation type="unfinished">Entwickler</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Administrator</source>
        <translation></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User</source>
        <translation type="unfinished">Benutzer</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>File System</source>
        <translation type="unfinished">Dateisystem</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User Management</source>
        <translation type="unfinished">Benutzerverwaltung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Addin Manager (Plugins)</source>
        <translation type="unfinished">Addin-Manager (Plugins)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Console</source>
        <translation type="unfinished">Konsole</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Console (Read Only)</source>
        <translation type="unfinished">Kondole (nur lesend)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Properties</source>
        <translation type="unfinished">Optionen</translation>
    </message>
</context>
<context>
    <name>ito::UserOrganizer</name>
    <message>
        <source>Developer</source>
        <translation type="obsolete">Entwickler</translation>
    </message>
    <message>
        <source>File System</source>
        <translation type="obsolete">Dateisystem</translation>
    </message>
    <message>
        <source>User Management</source>
        <translation type="obsolete">Benutzerverwaltung</translation>
    </message>
    <message>
        <source>Addin Manager (Plugins)</source>
        <translation type="obsolete">Addin-Manager (Plugins)</translation>
    </message>
    <message>
        <source>Console</source>
        <translation type="obsolete">Konsole</translation>
    </message>
    <message>
        <source>Console (Read Only)</source>
        <translation type="obsolete">Kondole (nur lesend)</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="obsolete">Optionen</translation>
    </message>
    <message>
        <source>Role</source>
        <translation type="obsolete">Rolle</translation>
    </message>
    <message>
        <source>User</source>
        <translation type="obsolete">Benutzer</translation>
    </message>
    <message>
        <location filename="../organizer/userOrganizer.cpp" line="+57"/>
        <source>Standard User</source>
        <translation type="unfinished">Standardbenutzer</translation>
    </message>
    <message>
        <location line="+267"/>
        <source>itomSettings directory not found, aborting!</source>
        <translation type="unfinished">Das Verzeichnis &quot;itomSettings&quot; wurde nicht gefunden! Vorgang abgebrochen!</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Could not copy standard itom ini file!</source>
        <translation type="unfinished">Die Standard-Ini-Datei von itom konnte nicht kopiert werden!</translation>
    </message>
</context>
<context>
    <name>ito::UserUiDialog</name>
    <message>
        <location filename="../widgets/userUiDialog.cpp" line="+93"/>
        <source>filename &apos;%s&apos; does not exist</source>
        <translation type="unfinished">Dateiname &apos;%s&apos; existiert nicht</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>ui-file could not be correctly parsed.</source>
        <translation type="unfinished">UI-Datei konnte nicht korrekt geparst werden.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>itom</source>
        <translation type="unfinished">itom</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>content-widget is empty.</source>
        <translation type="unfinished">Element ist leer.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>dialog button role is unknown</source>
        <translation type="unfinished">Unbekannte Rolle des Dialog-Button</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropEditorAPI</name>
    <message>
        <location filename="../ui/widgetPropEditorAPI.cpp" line="+49"/>
        <source>base path for relative pathes: </source>
        <translation type="unfinished">Basispfad der relativen Pfade: </translation>
    </message>
    <message>
        <location line="+4"/>
        <source>[does not exist]</source>
        <translation type="unfinished">[existiert nicht]</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>load python api file</source>
        <translation type="unfinished">Pyhton API-Dateien laden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>python api file (*.api)</source>
        <translation type="unfinished">Pyhton API-Dateien (*.api)</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropEditorStyles</name>
    <message>
        <location filename="../ui/widgetPropEditorStyles.cpp" line="+137"/>
        <source>choose background color</source>
        <translation type="unfinished">Hintergrundfarbe auswählen</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>choose foreground color</source>
        <translation type="unfinished">Vordergrundsfarbe auswählen</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropFigurePlugins</name>
    <message>
        <location filename="../ui/widgetPropFigurePlugins.cpp" line="+73"/>
        <source>class name</source>
        <translation type="unfinished">Klassenname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>data types</source>
        <translation type="unfinished">Datentyp</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>data formats</source>
        <translation type="unfinished">Datenformat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>features</source>
        <translation type="unfinished">Eigenschaften</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>plot type</source>
        <translation type="unfinished">Anzeigetyp</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>plugin file</source>
        <translation type="unfinished">Plugin-Datei</translation>
    </message>
    <message>
        <source>DataObject - Line</source>
        <translation type="obsolete">Datenobjekt - Linie</translation>
    </message>
    <message>
        <source>DataObject - Plane</source>
        <translation type="obsolete">Datenobjekt - Fläche</translation>
    </message>
    <message>
        <source>DataObject - Plane Stack</source>
        <translation type="obsolete">Datenobjekt - Fläschenstapel</translation>
    </message>
    <message>
        <source>Point Cloud</source>
        <translation type="obsolete">Punktewolke</translation>
    </message>
    <message>
        <source>PolygonMesh</source>
        <translation type="obsolete">Polygon</translation>
    </message>
    <message>
        <source>Gray8</source>
        <translation type="obsolete">Grau8</translation>
    </message>
    <message>
        <source>Gray16</source>
        <translation type="obsolete">Grau16</translation>
    </message>
    <message>
        <source>Gray32</source>
        <translation type="obsolete">Grau32</translation>
    </message>
    <message>
        <source>Float32</source>
        <translation type="obsolete">Fließkomma32</translation>
    </message>
    <message>
        <source>Float64</source>
        <translation type="obsolete">Fließkomma64</translation>
    </message>
    <message>
        <source>Complex</source>
        <translation type="obsolete">Komplex</translation>
    </message>
    <message>
        <source>Static</source>
        <translation type="obsolete">Statisch</translation>
    </message>
    <message>
        <source>Cartesian</source>
        <translation type="obsolete">
Katesisch</translation>
    </message>
    <message>
        <source>Cylindrical</source>
        <translation type="obsolete">Zylindrisch</translation>
    </message>
    <message>
        <source>Line Plot</source>
        <translation type="obsolete">Linien-Plot</translation>
    </message>
    <message>
        <source>Image Plot</source>
        <translation type="obsolete">Bild-Plot</translation>
    </message>
    <message>
        <source>Isometric Plot</source>
        <translation type="obsolete">Isometrischer Plot</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>category</source>
        <translation type="unfinished">Kategorie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>description</source>
        <translation type="unfinished">Beschreibung</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>default figure plot</source>
        <translation type="unfinished">Standardgrafik-Plot</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropGeneralLanguage</name>
    <message>
        <location filename="../ui/widgetPropGeneralLanguage.cpp" line="+115"/>
        <location line="+20"/>
        <source>Current Language: </source>
        <translation>Aktuelle Sprache: </translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropHelpDock</name>
    <message>
        <location filename="../ui/widgetPropHelpDock.cpp" line="+62"/>
        <source>Database</source>
        <translation>Datenbank</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Updates and Downloads</source>
        <translation>Updates und Downloads</translation>
    </message>
    <message>
        <location line="+217"/>
        <location line="+459"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-390"/>
        <source>Invalid type attribute of xml file</source>
        <translation>Ungültiges Typen-Attribut der XML-Datei</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Type attribute node &apos;database&apos; of xml file is missing.</source>
        <translation>Typen-Attribut des Knotens &apos;database&apos; der XML-Dateien vermisst.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>xml parsing error: %1</source>
        <translation>XML-Parserfehler: %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>xml error: node &apos;database&apos; is missing.</source>
        <translation>XML-Fehler: Knoten &apos;database&apos; nicht gefunden.</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Up to date</source>
        <translation>Aktuell</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Update to version: %1 (%2)</source>
        <translation>Update auf Version: %1 (%2)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Download version: %1 (%2)</source>
        <translation>Download-Version: %1 (%2)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>wrong Scheme: %1 (your scheme %2)</source>
        <translation>Falsches Schema: %1 (aktuelles Schema %2)</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+143"/>
        <source>download error</source>
        <translation>Download-Fehler</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;update</source>
        <translation>&amp;Update</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>locate on disk</source>
        <translation>Verzeichnis anzeigen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>remove from disk</source>
        <translation>Auf der Festplatte löschen</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Remote database update...</source>
        <translation type="unfinished">Remote Datenbank-Update...</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Timeout: Server is not responding in time</source>
        <translation type="unfinished">Zeitüberschreitung: Server antwortet nicht</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Could not delete old local version of Database</source>
        <translation type="unfinished">Die alte Datenbankversion konnte nicht gelöscht werden</translation>
    </message>
</context>
<context>
    <name>ito::WidgetPropPythonStartup</name>
    <message>
        <location filename="../ui/widgetPropPythonStartup.cpp" line="+42"/>
        <source>base path for relative pathes: </source>
        <translation type="unfinished">Basispfad der relativen Pfade: </translation>
    </message>
    <message>
        <location line="+53"/>
        <source>load python script</source>
        <translation type="unfinished">Pyhton-Skript laden</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>python script (*.py)</source>
        <translation type="unfinished">Python-Skript (*.py)</translation>
    </message>
</context>
<context>
    <name>ito::WorkspaceDockWidget</name>
    <message>
        <location filename="../widgets/workspaceDockWidget.cpp" line="+133"/>
        <source>delete item(s)</source>
        <translation>Objekt(e) löschen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>export item(s)</source>
        <translation>Objekt(e) exportieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>import item(s)</source>
        <translation>Objekt(e) importieren</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>rename item</source>
        <translation>Objekt umbenennen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Workspace</source>
        <translation>Arbeitsbereich</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Do you really want to delete the selected variables?</source>
        <translation>Sollen die markierten Variablen wirklich gelöscht werden?</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Export data</source>
        <translation>Datenexport</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Error while exporting variables:
%1</source>
        <translation>Fehler beim exportieren der Variablen:
%1</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Import data</source>
        <translation>Datenimport</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Error while importing variables:
%1</source>
        <translation>Fehler beim importieren der Variablen:
%1</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>renaming variable</source>
        <translation>Variable umbenennen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>timeout while renaming variables</source>
        <translation>Zeitüberschreitung beim umbenennen der Variablen</translation>
    </message>
</context>
<context>
    <name>ito::WorkspaceWidget</name>
    <message>
        <location filename="../widgets/workspaceWidget.cpp" line="+63"/>
        <source>Globals</source>
        <translation>Global</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+4"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+4"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Locals</source>
        <translation>Lokal</translation>
    </message>
    <message>
        <location line="+345"/>
        <source>timeout while asking python for detailed information</source>
        <translation>Zeitüberschreitung bei der Anfrage an Python für detailierte Informationen</translation>
    </message>
</context>
<context>
    <name>uiDebugViewer</name>
    <message>
        <location filename="../widgets/uiDebugViewer.ui" line="+17"/>
        <source>Debug-Viewer</source>
        <translation>Debug-Übersicht</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>global</source>
        <translation>Global</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>global workspace variables</source>
        <translation>Variablen im globalen Arbeitsbereich</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>local</source>
        <translation>Lokal</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>PushButton</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>uiMainWindow</name>
    <message>
        <location filename="../widgets/uiMainWindow.ui" line="+14"/>
        <source>itom</source>
        <translation>itom</translation>
    </message>
</context>
<context>
    <name>userManagement</name>
    <message>
        <source>Enable All</source>
        <translation type="obsolete">Alle aktivieren</translation>
    </message>
    <message>
        <source>Disable All</source>
        <translation type="obsolete">Alle deaktivieren</translation>
    </message>
    <message>
        <source>Main Application Features</source>
        <translation type="obsolete">Hauptanwendung</translation>
    </message>
    <message>
        <source>Developer Tools
(Workspace, Breakpoints,
 Ui-Designer, Call Stack)</source>
        <translation type="obsolete">Entwickler-Tools
(Workspace, Haltepunkte,
 Ui-Designer, Call Stack)</translation>
    </message>
    <message>
        <source>Edit Properties</source>
        <translation type="obsolete">Editor-Eigentschaften</translation>
    </message>
    <message>
        <source>File System Widget</source>
        <translation type="obsolete">Dateisystem</translation>
    </message>
    <message>
        <source>Addin Manager Widget</source>
        <translation type="obsolete">AddIn-Manager</translation>
    </message>
    <message>
        <source>Console</source>
        <translation type="obsolete">Konsole</translation>
    </message>
    <message>
        <source>read only</source>
        <translation type="obsolete">nur lesen</translation>
    </message>
    <message>
        <source>off</source>
        <translation type="obsolete">aus</translation>
    </message>
    <message>
        <source>User / Group</source>
        <translation type="obsolete">Benutzer / Gruppen</translation>
    </message>
    <message>
        <location filename="../widgets/userManagement.ui" line="+67"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Group</source>
        <translation type="obsolete">Gruppe</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Ini File</source>
        <translation>Ini-Datei</translation>
    </message>
    <message>
        <source>Reset All In Group</source>
        <translation type="obsolete">Gruppe zurücksetzen</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>New User</source>
        <translation type="unfinished">Neuer Benutzer erstellen</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Delete User</source>
        <translation>Benutzer löschen</translation>
    </message>
    <message>
        <source>developer</source>
        <translation type="obsolete">Entwickler</translation>
    </message>
    <message>
        <source>admin</source>
        <translation type="obsolete">Administrator</translation>
    </message>
    <message>
        <source>user</source>
        <translation type="obsolete">Benutzer</translation>
    </message>
    <message>
        <location line="-126"/>
        <source>User Management</source>
        <translation>Benutzerverwaltung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>User</source>
        <translation type="unfinished">Benutzer</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Permission</source>
        <translation type="unfinished">Berechtigungen</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Edit User</source>
        <translation type="unfinished">Benutzer bearbeiten</translation>
    </message>
</context>
<context>
    <name>userManagementEdit</name>
    <message>
        <location filename="../widgets/userManagementEdit.ui" line="+14"/>
        <source>User Management Edit / New</source>
        <translation></translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+142"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Main Application Permission</source>
        <translation type="unfinished">Berechtigungen in der Hauptanwendung</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Developer Tools (Workspace, Breakpoints, Ui-Designer, Call Stack)</source>
        <translation type="unfinished">Entwickler-Tools (Workspace, Haltepunkte,  Ui-Designer, Call Stack)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit Properties</source>
        <translation type="unfinished">Optionen ändern</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Addin Manager Widget (Plugins)</source>
        <translation type="unfinished">Addin-Manager (Plugins)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>File System Widget</source>
        <translation type="unfinished">Dateisystem</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>User Management</source>
        <translation type="unfinished">Benutzerverwaltung</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Console</source>
        <translation>Konsole</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>normal</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>read only</source>
        <translation>nur lesen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>off</source>
        <translation>aus</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Role</source>
        <translation>Rolle</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Developer</source>
        <translation></translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Administrator</source>
        <translation></translation>
    </message>
</context>
</TS>
