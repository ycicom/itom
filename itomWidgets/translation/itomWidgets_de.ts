<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DoubleRangeWidget</name>
    <message>
        <source>SliderSpinBoxWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MenuComboBox</name>
    <message>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
</context>
<context>
    <name>MenuComboBoxPrivate</name>
    <message>
        <source>Search</source>
        <translation>Suchen</translation>
    </message>
</context>
<context>
    <name>PathLineEdit</name>
    <message>
        <source>Select a file to save </source>
        <translation>Dateiname angeben</translation>
    </message>
</context>
<context>
    <name>PathLineEditPrivate</name>
    <message>
        <source>Open a dialog</source>
        <translation>Dialog öffnen</translation>
    </message>
</context>
<context>
    <name>RangeWidget</name>
    <message>
        <source>SliderSpinBoxWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SearchBoxPrivate</name>
    <message>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
</context>
<context>
    <name>SliderWidget</name>
    <message>
        <source>SliderWidget</source>
        <translation></translation>
    </message>
</context>
</TS>
