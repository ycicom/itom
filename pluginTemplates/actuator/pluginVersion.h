/* ********************************************************************
    Template for an actuator plugin for the software itom
    
    You can use this template, use it in your plugins, modify it,
    copy it and distribute it without any license restrictions.
*********************************************************************** */

#ifndef PLUGINVERSION_H
#define PLUGINVERSION_H

#define PLUGIN_VERSION_MAJOR 0
#define PLUGIN_VERSION_MINOR 0
#define PLUGIN_VERSION_PATCH 1
#define PLUGIN_VERSION_REVISION 0
#define PLUGIN_VERSION_STRING "0.0.1"
#define PLUGIN_COMPANY        "Your Company Name"
#define PLUGIN_COPYRIGHT      "Your Copyright"
#define PLUGIN_NAME           "MyActuator"

//----------------------------------------------------------------------------------------------------------------------------------

#endif // PLUGINVERSION_H
